require('./map/map-helper')();
require('./chart/chart-helper')();
require('./metrics/metrics-helper')();
require('./events/events-helper')();
require('./spinner')();

module.exports = function() {
    this.isElectron = function() {
        return process.argv.includes('--is-electron');
    };
};