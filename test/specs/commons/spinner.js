var Spinner = (function() {
    function Spinner() {}
    Spinner.Wait = function() {
        var spinner = true;
        spinnerExists = browser.isExisting("[data-test-id='spinner']");
        spinnerVisible = browser.isVisible("[data-test-id='spinner']");
        if (spinnerExists == true && spinnerVisible == true) {
            while (spinner == true) {
                //TODO: This is CPU intensive. Add 500ms delay before checking
                spinner = browser.isVisible("[data-test-id='spinner']");
            }
        } else
        if (spinnerExists == false || spinnerVisible == false) {
            spinner = false;
        }
    };
    return Spinner;
}());
module.exports = function() {
    this.spinner = new Spinner();
}