let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });
    it('To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025', function(){
        console.log("Test started - To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025");
        //1. template 1 - 5GNR DL IP Data Stall
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        baseMethods.closeAllViews();
        //2. Template 2 - Device Benchmarking
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName2);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify 'Select Comparison' dailog opened and click Ok
        var scBox = browser.element(parameter.selectComparison).isVisible();
        if (scBox){
            browser.element(parameter.deviceScoring).click();
            browser.element(parameter.selectComparisonOKBtn).click();
        }
        //verify Benchmarking and Time Series views are opened
        assert.equal(browser.element(parameter.viewBenchmarking).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        baseMethods.closeAllViews();        
        //Template 3 - Parameter Comparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTestMO);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName3);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        var mboxOpened1 = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened1){
            //click 2 parameters to compare
            for (var i=1; i<3; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //verify Parameter comparison view are opened
        assert.equal(browser.element(parameter.viewParameterComparison).isVisible(), true);
        baseMethods.closeAllViews();
        //Template 4 - SCG Failure
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName4);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        console.log("Test completed - To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025");
    });    
});