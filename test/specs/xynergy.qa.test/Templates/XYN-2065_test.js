let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });
    it('Verify all template information and icon - XYN-2065', function(){
        console.log("Test started - Verify all template information and icon - XYN-2065");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        //verify list of available templates under 'Template'
        var templateList = browser.elements(parameter.templatesList).value.length;
        if (templateList >=1)
        {
            var templateListvalue = browser.elements(parameter.templatesList).getText();
            expect(templateListvalue).to.include(config.templateName1);
            expect(templateListvalue).to.include(config.templateName2);
            expect(templateListvalue).to.include(config.templateName3);
            
        }
        //Verif tooltip is available
        browser.element(parameter.templatesTooltip).click();
        assert.equal(browser.element(parameter.templatesTooltip).isVisible(), true);
        console.log("Test completed - Verify all template information and icon - XYN-2065");
    });    
});