let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });
    it('To check Device Benchmarking Template - XYN-3087', function(){
        console.log("Test started - To check Device Benchmarking Template - XYN-3087");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName2);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify 'Select Comparison' dailog opened and click Ok
        var scBox = browser.element(parameter.selectComparison).isVisible();
        if (scBox){
            browser.element(parameter.deviceScoring).click();
            browser.element(parameter.selectComparisonOKBtn).click();
        }
        //verify Benchmarking and Time Series views are opened
        assert.equal(browser.element(parameter.viewBenchmarking).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //Verify default scoring method as Keysight Scoring Method            
        expect(browser.element(parameter.selectScoringDropdown).getText()).to.include(config.scoringMethod);
        //Verify device name in Devices is same as devices in Scope
        browser.element(parameter.deivcesTab).click();
        var deviceName = browser.element(parameter.deviceDetails).getValue();               
        genericObject.clickOnDTTab(parameter.scope);
        isScopeDevice = templatesObject.compareScopeDevices(deviceName); 
        assert.equal(isScopeDevice, true);
        console.log("Test completed - To check Device Benchmarking Template - XYN-3087");
    });    
});