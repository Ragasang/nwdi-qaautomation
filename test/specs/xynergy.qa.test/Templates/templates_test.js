let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            //baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            //baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            baseMethods.closeAllViews();
        }       
    });
    //Keep XYN-1975 on top always
    it('Verify templates shouldnot open when no data scope is defined - XYN-1975', function(){
        console.log("Test started - Verify templates shouldn't open when no data scope is defined - XYN-1975");
        //After Login, click on AV and then DT for Views
        browser.element(parameter.AVMenu).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        //select any template
        baseMethods.selectTemplate(config.templateName1);
        //catch the error dialog
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            assert.equal(browser.element(parameter.wrngContent).getText(), config.noScopeisSet); 
            //click ok
            browser.element(parameter.sameDatasetDialogWindowClose).click();                       
        }
        browser.pause(300);
        console.log("Test completed - Verify templates shouldn't open when no data scope is defined - XYN-1975");
    });
    it('To check Device Benchmarking Template - XYN-3087', function(){
        console.log("Test started - To check Device Benchmarking Template - XYN-3087");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName2);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify 'Select Comparison' dailog opened and click Ok
        var scBox = browser.element(parameter.selectComparison).isVisible();
        if (scBox){
            browser.element(parameter.deviceScoring).click();
            browser.element(parameter.selectComparisonOKBtn).click();
        }
        //verify Benchmarking and Time Series views are opened
        assert.equal(browser.element(parameter.viewBenchmarking).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //Verify default scoring method as Keysight Scoring Method            
        expect(browser.element(parameter.selectScoringDropdown).getText()).to.include(config.scoringMethod);
        //Verify device name in Devices is same as devices in Scope
        browser.element(parameter.deivcesTab).click();
        var deviceName = browser.element(parameter.deviceDetails).getValue();               
        genericObject.clickOnDTTab(parameter.scope);
        isScopeDevice = templatesObject.compareScopeDevices(deviceName); 
        assert.equal(isScopeDevice, true);
        console.log("Test completed - To check Device Benchmarking Template - XYN-3087");
    });
    it('Verifying Adding/Deleting a layer to views for templates - XYN-4137', function(){
        console.log("Test started - Verifying Adding/Deleting a layer to views for templates - XYN-4137");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName2);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify 'Select Comparison' dailog opened and click Ok
        var scBox = browser.element(parameter.selectComparison).isVisible();
        if (scBox){
            browser.element(parameter.deviceScoring).click();
            browser.element(parameter.selectComparisonOKBtn).click();
        }
        //verify Benchmarking and Time Series views are opened
        assert.equal(browser.element(parameter.viewBenchmarking).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //plot event and verify it from data layers
        baseMethods.plotEvent(config.eventName3, parameter.nrSCGFailure, parameter.PlotAcrossWidgets);
        browser.pause(300);
        //verify event plotted
        timeseries.clickOnTimeSeriesLayerBtn();
        var eventName = baseMethods.verifyTimeSeriesEventLayerData(config.eventName3);
        assert.equal(eventName, true);
        //Now delete layer from Datalayers
        timeseries.deleteAllLayers();
        var eventName = baseMethods.verifyTimeSeriesEventLayerData(config.eventName3);
        assert.equal(eventName, false);
        baseMethods.closeTimeSeriesDataLayer();
        console.log("Test completed - Verifying Adding/Deleting a layer to views for templates - XYN-4137");
    });
    it('To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025', function(){
        console.log("Test started - To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025");
        //1. template 1 - 5GNR DL IP Data Stall
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        baseMethods.closeAllViews();
        //2. Template 2 - Device Benchmarking
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName2);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify 'Select Comparison' dailog opened and click Ok
        var scBox = browser.element(parameter.selectComparison).isVisible();
        if (scBox){
            browser.element(parameter.deviceScoring).click();
            browser.element(parameter.selectComparisonOKBtn).click();
        }
        //verify Benchmarking and Time Series views are opened
        assert.equal(browser.element(parameter.viewBenchmarking).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        baseMethods.closeAllViews();        
        //Template 3 - Parameter Comparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTestMO);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName3);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        var mboxOpened1 = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened1){
            //click 2 parameters to compare
            for (var i=1; i<3; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //verify Parameter comparison view are opened
        assert.equal(browser.element(parameter.viewParameterComparison).isVisible(), true);
        baseMethods.closeAllViews();
        //Template 4 - SCG Failure
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName4);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(1000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        console.log("Test completed - To verify 5G NR IP Data Stall, Device benchmarking, Parameter comparison and SCG Failure Templates - XYN-3025");
    });
    it('Verify all template information and icon - XYN-2065', function(){
        console.log("Test started - Verify all template information and icon - XYN-2065");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        //verify list of available templates under 'Template'
        var templateList = browser.elements(parameter.templatesList).value.length;
        if (templateList >=1)
        {
            var templateListvalue = browser.elements(parameter.templatesList).getText();
            expect(templateListvalue).to.include(config.templateName1);
            expect(templateListvalue).to.include(config.templateName2);
            expect(templateListvalue).to.include(config.templateName3);
            
        }
        //Verif tooltip is available
        browser.element(parameter.templatesTooltip).click();
        assert.equal(browser.element(parameter.templatesTooltip).isVisible(), true);
        console.log("Test completed - Verify all template information and icon - XYN-2065");
    });
    it('To verify Templates implementation - XYN-2888', function(){
        console.log("Test started - To verify Templates implementation - XYN-2888");
        //This test step comes after XYN-3025
        //1. template 1 - 5GNR DL IP Data Stall - Re-align after closing messages view
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //view confirmation
        assert.equal(browser.element(config.mvviewStyle1).isVisible(), true);
        //cloes messages view
        browser.element(parameter.msgViewCloseBtn).click();
        //after close
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), false);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //style
        assert.equal(browser.element(config.mvviewStyle1).isVisible(), false);
        console.log("Test completed - To verify Templates implementation - XYN-2888");
    });
    //Below test case has been commented due to bug existed in both desktop and web apps, it will uncommented once the bug get resolved  
    /*  
    it('To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980', function(){
        console.log("Test started - To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //Fileter Messages view for not available data
        messageObject.enterMessageFilterValue(config.findMessageFilterValueInvalid);
        browser.pause(2000);
        var noDatamsg = browser.element(parameter.noData).isVisible();
        assert.equal(noDatamsg, true);
        //Add one more dataset to the existing view        
        templatesObject.repeatforScopeTest(config.searchDatasetForRegularTestMO, parameter.dataScopeAdd);
        //replace the existing view with other dataset                 
        templatesObject.repeatforScopeTest(config.searchDatasetForPComparison5GTest, parameter.dataScopeReplaceBtn);
        console.log("Test completed - To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980");
    });*/
});