let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify whether search results shows up when user types in already existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925', function(){
        console.log("Test started - Verify whether search results shows up when user types in existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        searchdataObject.clickSaveasbutton();
        //var saveresultas = browser.element("//button[@class='action-button']").click();
        var saveresult = browser.element(parameter.saveSearchResult).click();
        var FileInCAPS = baseMethods.saveFileName(config.searchTermName);
        searchdataObject.enterSaveSearchResultNameValue(FileInCAPS.toUpperCase());
        //browser.element("[data-test-id='SavedNameinput']").click();
        var SaveDataset = browser.element(parameter.saveSearchResultSaveBtn).click();
        var closeBtn = browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clickSaveasbutton();
        //var saveresultas = browser.element("//button[@class='action-button']").click();
        var saveresult = browser.element(parameter.saveSearchResult).click();
        searchdataObject.enterSaveSearchResultNameValue(FileInCAPS.toUpperCase());
        browser.pause(5000);
        listValue = browser.element(parameter.saveasDropDown).getText();
        assert.equal(FileInCAPS.toUpperCase(), listValue);
        browser.element(parameter.saveSearchResultName).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        console.log("Test completed - Verify whether search results shows up when user types in existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925");
    }); 

});