let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if the selected record gets filtered. - XYN-1484', function(){
        console.log("Test Started - Verify if the selected record gets filtered. - XYN-1484");   
        baseMethods.checkForWorkspaceWidget(); 
        baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
        browser.pause(500); var datasetItem = browser.element(parameter.listItem);
        datasetItem.click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var FilteredDataset = browser.element(parameter.filteredItem);  
        var visibility = FilteredDataset.isVisible();
        assert(visibility,true);
        var Datasetname = FilteredDataset.getText();  
        var SelectedDataset = browser.element(parameter.listItem).getText();
        expect(Datasetname.toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
        expect(SelectedDataset.toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
        console.log("Test Completed - Verify if the selected record gets filtered. - XYN-1484");   


    });

});