let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify Save Workspace from Add/replace window - XYN-2155', function(){
        console.log("Test started - Verify save workspace from Add/Replace window - XYN-2155");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.filteredItem).click();
        browser.pause(200);
        searchdataObject.clearsearchkeyword();
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Messages']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab[0]).to.include("Messages");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - Verify save workspace from Add/Replace window - XYN-2155");
    });

});