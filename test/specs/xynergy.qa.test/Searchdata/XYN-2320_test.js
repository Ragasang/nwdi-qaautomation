let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify search filter with disabled files - XYN-2320', function(){
        console.log("Test started - Verify search filter with disabled files - XYN-2320");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.listItem1).click();
        browser.pause(200);
        var sText=browser.element(parameter.errorMessage).getText();
        console.log(sText+" text is displayed");
        assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);
        browser.element(parameter.errorMessageOK).click();
        console.log("Test completed - Verify search filter with disabled files - XYN-2320");
    }); 

});