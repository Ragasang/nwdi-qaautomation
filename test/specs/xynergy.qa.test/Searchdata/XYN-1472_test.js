let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify the fields on search data page - XYN-1472', function(){
        console.log("Test Started - Verify the fields on search data page XYN-1472");
        baseMethods.SelectTabs("SEARCHDATA");
        var NewSearch = browser.element(parameter.newSearch);
        var visibility = NewSearch.isVisible();         
        assert(visibility,true);
        var EnterQuery = browser.element(parameter.searchTextArea);
        var visibility = EnterQuery.isVisible();         
        assert(visibility,true);    
        var FilterButton = browser.element(parameter.filterButton);
        var visibility = FilterButton.isVisible();         
        assert(visibility,true);
        var SearchButton = browser.element(parameter.searchButton);
        var visibility = SearchButton.isVisible();         
        assert(visibility,true);
        console.log("Test Completed - Verify the fields on search data page XYN-1472");
    }); 

});