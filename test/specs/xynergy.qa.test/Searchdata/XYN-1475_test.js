let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if user is able to search by device name - XYN-1475', function(){
        console.log("Test started - Verify if user is able to search by device name - XYN-1475");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemDeviceName,config.searchCondition,config.searchTextForDeviceName);
        //browser.pause(500);
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Devicename results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j]).to.include(config.searchTextForDeviceName);
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        console.log("Test completed - Verify if user is able to search by device name - XYN-1475");
    });

});