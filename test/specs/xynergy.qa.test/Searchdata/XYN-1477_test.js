let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('XYN-1477_Verify if user gets a message when invalid search criteria is provided ', function(){
        console.log("Test started - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
        baseMethods.SelectTabs("SEARCHDATA");
        var newQueryTab = browser.element(parameter.searchTextArea);
        newQueryTab.click();
        newQueryTab.keys(config.searchTextForFIleNameForError);
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        browser.pause(500);
        var bStatus = browser.element(parameter.incorrectQuery).isExisting();
        console.log(bStatus);
        assert(bStatus, true);
        var errortitle=browser.element(parameter.errorMessageTitle).getText();
        assert(errortitle, config.searcherrorMessageTitle);
        var errorMessage=browser.element(parameter.incorrectQuery).getText();
        assert(errorMessage, config.searcherrorMessageText);
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test completed - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
    });  

});