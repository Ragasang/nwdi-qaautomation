let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify Search Data validation prompts and Miscellaneous - XYN-3592', function(){
        console.log("Test started - Verify Search Data validation prompts and Miscellaneous - XYN-3592");
        baseMethods.SelectTabs("SEARCHDATA");        
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,'');        
        var errorTitle = browser.element(parameter.errorMessageTitle);
        var visibility = errorTitle.isVisible();         
        assert(visibility,true);
        var errorCloseBtn = browser.element(parameter.incorrectQueryClose);
        errorCloseBtn.click();
        searchdataObject.clearsearchkeyword();
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleNameForError);        
        var sText=browser.element(parameter.errorMessage).getText();        
        console.log(sText+" text is displayed");        
        assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);        
        browser.element(parameter.errorMessageOK).click();       
        var sText1=browser.element(parameter.searchTextArea).getText();
        console.log(sText1+" text is displayed");
        //assert.equal(sText1,config.searchTextForFIleNameForError,sText+" actual text is not same as expected"+config.searchTextForFIleNameForError);
        console.log("Test completed - Verify Search Data validation prompts and Miscellaneous - XYN-3592");
    });

});