let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('SearchData - Verify whether user can able to append files to the already existing dataset, XYN-2927', function(){
        console.log("Test started - To Verify whether user can able to append files to the already existing dataset, XYN-2927");
        baseMethods.checkForWorkspaceWidget();
        baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
        browser.pause(500);
        var datasetItem = browser.element(parameter.listItem);
        datasetItem.click();
        baseMethods.verifySpinnerVisibility();
        var selectdataset = browser.element(parameter.filteredItem);
        var selecteddataset = selectdataset.getText();
        console.log(selecteddataset);
        var sText = selecteddataset.split(" ");
        browser.pause(500);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        var datasetname = baseMethods.saveFileName(config.datasetName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(100);
        browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveasDropDown).click();
        var WarningText = browser.element(parameter.datasetDuplicateWarning);
        WarningText.isVisible();
        var SaveDatasetName = browser.element(parameter.saveSearchResultSaveBtn);
        SaveDatasetName.click();
        browser.pause(500);
        browser.element(parameter.openSavedResult).click();
        browser.pause(200);
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        var scope = browser.element(parameter.scope);
        if(!(scope.getAttribute("class")).includes('enabled')){
            scope.click();
        }
        browser.pause(1000);
        var verifyDataset = browser.element("[data-test-id='"+sText[1]+"']");
        browser.pause(300);
        var visibility = verifyDataset.isVisible();
        console.log(sText[1]);
        assert(visibility,true);
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - To Verify whether user can able to append files to the already existing dataset, XYN-2927");
    }); 

});