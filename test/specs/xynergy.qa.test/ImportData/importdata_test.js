let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('drive-test page', function () {
    var importScreen=browser.elements('import-data-files');
    var dataGrid=importScreen.elements('[data-test-id="data-grid-container"]');

    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });
    it('Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781', function(){
        console.log("Test Started - Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.searchDatasetForRegularTest);
        browser.pause(600);
        var importButtonEnabled=importDataObject.isImportButtonEnabled();
        console.log(importButtonEnabled);
        assert.equal(importButtonEnabled,false);   
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);    
        //baseMethods.uploadFile(fileToUpload);
        browser.pause(500);
        var importButtonEnabled=importDataObject.isImportButtonEnabled();
        assert.equal(importButtonEnabled,true);
        console.log("Test completed - Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781");
    });
    it('Verification of import data append message, XYN-1493', function () {
        console.log("Test started- import data append message XYN-1493");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.enterDataSetName);
        browser.pause(600);
        var appendMessage=baseMethods.getAppendMessage();
        console.log(appendMessage);
        expect(appendMessage).to.include(config.appendMessageValue);   
        console.log("Test Completed- import data append message XYN-1493");    
    });
    it('Verification of import data append, XYN-1495', function () {
        console.log("Test started- import data append message XYN-1495");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);
        browser.pause(1000);
        var selectedFile=browser.elements(parameter.gridRows).value.length;
        var count=5;
        console.log(selectedFile);
        if(selectedFile>0){
            importDataObject.clickDeleteSelectedFile();
            browser.pause(300);
            count=browser.elements(parameter.gridRows).value.length;
        }
        assert.equal(count,0);
        console.log("Test completed- import data append message XYN-1495");
    });
    

});