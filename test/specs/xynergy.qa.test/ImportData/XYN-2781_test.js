let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    it('Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781', function(){
        console.log("Test Started - Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.searchDatasetForRegularTest);
        browser.pause(600);
        var importButtonEnabled=importDataObject.isImportButtonEnabled();
        console.log(importButtonEnabled);
        assert.equal(importButtonEnabled,false);   
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);    
        //baseMethods.uploadFile(fileToUpload);
        browser.pause(500);
        importButtonEnabled=importDataObject.isImportButtonEnabled();
        assert.equal(importButtonEnabled,true);
        importDataObject.clickHistoryMenu();
        browser.pause(500);
        importDataObject.clickImportAddDataMenu();
        browser.pause(400);
        importButtonEnabled=importDataObject.isImportButtonEnabled();
        assert.equal(importButtonEnabled,true);
        console.log("Test completed - Verify Import button enabled only after dataset name and files are added of Open a dataset- XYN-2781");
    });

});