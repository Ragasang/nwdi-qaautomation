let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Import data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    it('Remembering previous state on navigating tabs XYN-2759', function(){
        console.log("Test Started - remembering previous state on navigating tabs XYN-2759");
        browser.pause(300); 
        var dataSetName=config.addDatasetName;
        console.log(dataSetName);
        importDataObject.enterDataSetName(dataSetName);
        browser.pause(600);   
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);
        browser.pause(400);
        importDataObject.clickHistoryMenu();
        browser.pause(200);
        importDataObject.enterValueInHistoryFilter(config.searchALFDataset);
        browser.pause(200);
        importDataObject.clickImportAddDataMenu();
        browser.pause(1000);
        var datasetValue=importDataObject.verifySelectedFile(config.fileName);
        assert.equal(datasetValue,true);
        console.log("Test completed - remembering previous state on navigating tabs XYN-2759");
    });

});