let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    it('Verify upload .zip file types- XYN-1098', function(){
        console.log("Test Started - Verify upload .zip file types- XYN-1098");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.datasetForDifferentFilteTypes);
        browser.pause(600);   
        var absolutePathZIPType = path.resolve(__dirname, config.fileNameWithzipType);      
        importDataObject.browseFile(absolutePathZIPType);
        browser.pause(500);
        importDataObject.clickImportBtn();
        browser.pause(400);
        importDataObject.waitForProgressPage();
        importDataObject.getProgressDataSet(config.datasetForDifferentFilteTypes);
        browser.pause(9000);
        browser.waitForExist(parameter.historyOpenBtn);
        browser.pause(400);
        importDataObject.enterValueInHistoryFilter(dataSetName);
        browser.pause(400);
        var getUploadedDataset=importDataObject.getDataSetCount(parameter.historyDataGrid,parameter.gridRows,dataSetName);
        console.log(getUploadedDataset);
        browser.pause(300);           
        expect(getUploadedDataset).to.be.equal(config.compareValueWithOne);
        console.log("Test completed - Verify upload .zip file types- XYN-1098");
    });
});