let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it.skip("Verify that dataset with multiple device is displaying multiple tab in sequence diagram - XYN-1430", function() {
        console.log("Test started - To verify that dataset with multiple device is displaying multiple tab in sequence diagram");
        //genericObject.clickOnDTTab(parameter.Scope);
        browser.pause(500);
        var device1 = SequenceDiagramObject.isDevicePresent(config.sprintMODeviceName);
        var device2 = SequenceDiagramObject.isDevicePresent(config.sprintMTDeviceName);
        console.log(device1);
        console.log(device2);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        var tabDevice1 = SequenceDiagramObject.isTabPresent(config.sprintMODeviceName);
        var tabDevice2 = SequenceDiagramObject.isTabPresent(config.sprintMTDeviceName);
        var tabDeviceCount = SequenceDiagramObject.getSequenceTabCount(config.DeviceType);
        console.log(tabDeviceCount);
        //console.log(tabDeviceCount);
        assert.equal(device1, tabDevice1);
        assert.equal(device2, tabDevice2);
        var result = false;
        if(tabDeviceCount>1){
            result=true;
        }
        assert.equal(result,true);
        console.log("Test completed for tab verification in sequence diagram - XYN-1430");
    });

});