let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it("Verify that selected messages to calculate latency is showing up under data layers - XYN-1467", function () {
        console.log("Selected message to calculate latency is showing under data layers");
        var result = [];
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        var result = baseMethods.SelectedDataToCalculateLatency();
        // var value = SelectedOne.value;
        SequenceDiagramObject.clickOnCalculateBtn();
        baseMethods.clickOnColumnsbutton();
        baseMethods.clickOnSequenceData(config.dataLayersTab);
        var datalayer = browser.element(parameter.seqLayerData);
        var text = datalayer.getText();
        browser.pause(200);
        console.log(result[0], result[1]);
        expect(text).to.include(result[0]);
        expect(text).to.include(result[1]);
        console.log("Test completed for - Selected message to calculate latency is showing under data layers-XYN-1467");
    });

});