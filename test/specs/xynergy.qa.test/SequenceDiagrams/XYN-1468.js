let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('To Verify the different selected Latencies color, XYN-1468', function () {
        console.log("To Verify the different selected Latencies color, XYN-1468");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor1 = browser.element(parameter.firstLatencyColor).getAttribute("style");
        console.log(latencyColor1);
        SequenceDiagramObject.selectDataToCalculateLatency2();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor2 = browser.element(parameter.secondlatencyColor).getAttribute("style");
        console.log(latencyColor2);
        assert.notEqual(latencyColor1, latencyColor2);
        console.log("Test completed - To Verify the different selected Latencies color, XYN-1468");
    }); 

});