let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it("Verify that dataset with single device is displaying single tab in sequence diagram - XYN-1429", function() {
        console.log("Test started - To verify that dataset with single device is displaying single tab in sequence diagram");
        genericObject.clickOnDTTab(parameter.scope);
        var device = SequenceDiagramObject.isDevicePresent(config.sprintMODeviceName);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        var tabDevice = browser.elements(parameter.seqTabs).value.length;
        assert.equal(device, tabDevice);
        assert.equal(config.compareValueWithOne, tabDevice);
        console.log("Test completed for tab verification in sequence diagram - XYN-1429");

    });

});