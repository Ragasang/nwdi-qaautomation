let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('Verify whether RRC messages are present on Sequence diagram, XYN-2777', function () {
        console.log("Test started to verify RRC connection message");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        // var sequenceBlock = browser.elements("//xyn-sequence-item");
        var rowMessage = browser.elements(parameter.rowMessage);
        var Message = rowMessage.value;
        var value = false;
        Message.forEach(element => {
            var messageValue = element.getText();
            if (messageValue == config.rrcConnection) {
                value = true;
            }
        })
        assert.equal(value, true);
        console.log("Test completed to verify RRC connection message-XYN-2777");
    });

});