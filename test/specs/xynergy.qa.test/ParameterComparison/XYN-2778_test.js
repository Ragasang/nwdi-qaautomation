let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Parameter Comparison - Filter enhancements- XYN-2778', function () {
        console.log("Test started - Parameter Comparison - Filter enhancements - searchable words with space should work as AND operator - XYN-2778");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //verifying for proper values from loaded data
        var searchItem = config.filterWithSpaceValues[0]+" "+config.filterWithSpaceValues[1]+" "+config.filterWithSpaceValues[2];
        parameterComparisonObject.enterFilterValue(searchItem);                        
        var findVal = parameterComparisonObject.headerItems(parameter.filteredValues + config.queryNo1);
        console.log(findVal);        
        expect(findVal).to.include(config.filterWithSpaceValues[2]);        
        var findVal = parameterComparisonObject.headerItems(parameter.filteredValues + config.queryNo3);
        console.log(findVal);
        expect(findVal).to.include(config.filterWithSpaceValues[0]);
        expect(findVal).to.include(config.filterWithSpaceValues[1]);
        parameterComparisonObject.clearFilter(); 
        //verifying for dummy values
        parameterComparisonObject.enterFilterValue(config.filterVal9);
        var noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();        
        console.log("Test completed - Parameter Comparison - Filter enhancements - searchable words with space should work as AND operator - XYN-2778");
    });
});