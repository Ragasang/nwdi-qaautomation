let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('To verify Parameter Comparison select cells message - XYN-2895', function () {
        console.log("Test started - To verify Parameter Comparison select cells message - XYN-2895");
        //First test >6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();                        
        assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), true);
        assert.equal(browser.element(parameter.selectCellsoKBtn).getText(), config.textOK);
        assert.equal(browser.element(parameter.cancelButton_1).getText(), config.textCancel);
        var totHItemsNo = browser.elements(parameter.headerItem).value.length;
        var totGetVerify = config.selectCells.length;
        var isFound = false;
        if ((totHItemsNo >=1) && (totGetVerify >=1)){
            //print list
            var getHItemsList = browser.elements(parameter.headerItem).getText();
            var getVerifyList = config.selectCells;
            for (var i=0; i<=totGetVerify-1; i++){
                isFound = getHItemsList.includes(getVerifyList[i]);
            }
        }
        assert.equal(isFound, true);
        assert.equal(browser.element(parameter.selectAll).isVisible(), true);
        assert.equal(browser.element(parameter.selectCellsInfoSection).getText(), config.selectCellsInfoMsg);
        browser.element(parameter.cancelButton_1).click();
        browser.pause(300);
        var dfcText = browser.elements(parameter.defaultContainer).getText();
        expect(dfcText).to.include(config.pComparisonDefContText_1);
        expect(dfcText).to.include(config.pComparisonDefContText_2);
        expect(dfcText).to.include(config.pComparisonDefContText_3);
        console.log("Test completed - To verify Parameter Comparison select cells message - XYN-2895");
    });
});