let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            //baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            //baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }
        
    });
    afterEach(function () {
        if(isElectron()){
        baseMethods.closeAllViews();
        }      
    });
    it('Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2930', function () {
        console.log("Test started - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2930");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        firstParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(firstParameterVal);
        //second test <6 dataset which also will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparisonLTETest);
        baseMethods.openInCanvasValidation() ;      
        baseMethods.verifySpinnerVisibility();
        secondParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(secondParameterVal);   
        //Parameter values should not equal since two different datasets are loaded
        assert.notEqual(firstParameterVal, secondParameterVal); 
        console.log("Test completed - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2930");
    });
    it('Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931', function () {
        console.log("Test started - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation() ;     
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        firstParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(firstParameterVal);
        //second test >6 dataset which also will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        secondParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(secondParameterVal);   
        //Parameter values should not equal since two different datasets are loaded
        assert.notEqual(firstParameterVal, secondParameterVal); 
        console.log("Test completed - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931");
    });
    it('Verify if multiple parameters are separated by | in comparison view - XYN-2935', function () {
        console.log("Test started - Verify if multiple parameters are separated by | in comparison view - XYN-2935");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //Get text from each parameter's column
        var j = 1;
        var findPipe = false;
        while( j > 0){
            console.log(j);
            if (findPipe == false){
                findPipe = parameterComparisonObject.getFValues(j, config.pipeSymb);
            } 
            if (findPipe == false){
                findPipe = parameterComparisonObject.getSValues(j, config.pipeSymb);
            } 
            if (findPipe == false){
                findPipe = parameterComparisonObject.getTValues(j, config.pipeSymb);
            }
            if (findPipe == false){
                findPipe = parameterComparisonObject.getFtValues(j, config.pipeSymb);
            }           
            if (findPipe == true){
                j = 0;
            }
            else{
                j++; 
            }                 
        }
        assert.equal(findPipe, true);        
        console.log("Test completed - Verify if multiple parameters are separated by | in comparison view - XYN-2935");
    });
    it('Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2923', function () {
        console.log("Test started - Verify if the data restores after filtering for an invalid value and clearing it - XYN-2923");        
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        parameterComparisonObject.enterFilterValue(config.filterVal);
        var findVal = false;
        if (findVal == false){
            findVal = parameterComparisonObject.getFilterValues(config.filterVal);
        }
        assert.equal(findVal, true);
        parameterComparisonObject.clearFilter();
        noDataPage = browser.element(parameter.noData).isVisible();
        //data should get displayed after clearing the filter
        //1. No data Available page should not get displayed
        assert.equal(noDataPage, false);
        //2. At least some values get present
        tdValue = browser.element(parameter.pcTableData).getText();
        if(tdValue == ""){
            assert.fail();
        }
        console.log("Test completed - Verify if the data restores after filtering for an invalid value and clearing it - XYN-2923");
    });
    
    it('Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905', function () {
        console.log("Test started - Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        parameterComparisonObject.enterFilterValue(config.filterVal1);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal2);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal3);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal4);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal5);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal6);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal7);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal8);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal9);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        console.log("Test completed - Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905");
    });
    it('Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786', function () {
        console.log("Test started - Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786");
        //First test >6 and LTE&5G technolgies which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GLTETest);
        baseMethods.openInCanvasValidation();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);      
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectAllCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened,
        var i=1;
        var ftechLTE=0;
        var ftech5G=0;
        var result = config.selectCells1;
        if (mboxOpened){
            //1. verify all columns headers are as expected.
            var j=2;
            while(j<=6){
                var hCell = parameterComparisonObject.headerItems(parameter.headerItem + "[" + j + "]");
                console.log(config.selectCells[j-2])
                assert.equal(hCell, config.selectCells[j-2]);
                j++;
            }
            //2. verify technologies under system column
            do{ //loop should be less than 6 cells dataset only
                var techVal = parameterComparisonObject.technologyType(i);
                console.log(techVal);
                if (ftechLTE == 0){
                    var newtechType1 = config.findMessageFilterValue3.toUpperCase();
                    if(techVal == newtechType1){
                        ftechLTE = 1; //found LTE tech from list
                    }
                }else
                if (ftech5G == 0){
                    var newtechType2 = config.techType2.toUpperCase();
                    if(techVal == newtechType2){
                        ftech5G = 1; //found LTE tech from list
                    }
                }
                if ((ftechLTE == 1) && (ftech5G == 1)){
                    i=0;
                }
                else{
                    i++;
                }         
            }
            while(i!=0);
            assert.equal(ftechLTE, 1);
            assert.equal(ftech5G, 1);
            parameterComparisonObject.clickOKBtn();                                                        
        }                
        console.log("Test completed - Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786");
    });
    it('Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896', function () {
        console.log("Test started - Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var Option=browser.elements("//span[contains(text(),'"+config.viewOption+"')]").value.length;
        assert.equal(Option,config.compareValueWithOne);
        console.log("Test completed - Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896");
    });
    it('Parameter Comparison - Filter enhancements- XYN-2778', function () {
        console.log("Test started - Parameter Comparison - Filter enhancements - searchable words with space should work as AND operator - XYN-2778");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //verifying for proper values from loaded data
        var searchItem = config.filterWithSpaceValues[0]+" "+config.filterWithSpaceValues[1]+" "+config.filterWithSpaceValues[2];
        parameterComparisonObject.enterFilterValue(searchItem);                        
        var findVal = parameterComparisonObject.headerItems(parameter.filteredValues + config.queryNo1);
        console.log(findVal);        
        expect(findVal).to.include(config.filterWithSpaceValues[2]);        
        var findVal = parameterComparisonObject.headerItems(parameter.filteredValues + config.queryNo3);
        console.log(findVal);
        expect(findVal).to.include(config.filterWithSpaceValues[0]);
        expect(findVal).to.include(config.filterWithSpaceValues[1]);
        parameterComparisonObject.clearFilter(); 
        //verifying for dummy values
        parameterComparisonObject.enterFilterValue(config.filterVal9);
        var noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();        
        console.log("Test completed - Parameter Comparison - Filter enhancements - searchable words with space should work as AND operator - XYN-2778");
    });
    it('Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817', function () {
        console.log("Test started - Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();        
        //1. By default, when you select a cell then it will be the only baseline so  dialog will not pop-up for baseline select
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();  
        if (mboxOpened){
            for (var i=1; i<4; i++){
                parameterComparisonObject.clickCheckBox(i);
            }            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        } 
        //verify default 'No baseline' has been selected
        parameterComparisonObject.clickSelectBaselineTab();
        var getTotCells = browser.elements(parameter.totRowsCount).value.length;
        console.log(getTotCells);
        assert(getTotCells > 1);
        var getCellName = parameterComparisonObject.getBaselineCellName(1);
        console.log(getCellName);
        assert.equal(config.noBaseline, getCellName);
        //2. select a different baseline, by default 1st radio button has been checked(No baseline), so select next one
        parameterComparisonObject.selectRdBtnBaseline(2);        
        //get the above baseline cell name to uncheck it from the select cells
        getCellName = parameterComparisonObject.getBaselineCellName(2);
        console.log(getCellName);
        parameterComparisonObject.clickOKBtnBaseline();        
        //3. now unselect cell from select cells tab and expect the baseline tab to get open
        parameterComparisonObject.clickSelectCellsTab();
        getTotCells = browser.elements(parameter.totRowsCount).value.length;
        console.log(getTotCells);
        if(getTotCells > 1){
            var j = 1;
            while(j <= getTotCells){
                var rowVal = parameterComparisonObject.getRowVal(j);
                if (expect(rowVal).to.include(getCellName)){                    
                    //also unselect the cell and click ok
                    parameterComparisonObject.unSelectCheckbox(j);
                    parameterComparisonObject.clickOKBtn();
                    baseMethods.verifySpinnerVisibility();
                    j = getTotCells+1;
                }
            }
        }        
        browser.pause(500);
        assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), true);
        parameterComparisonObject.clickOKBtnBaseline();
        //4. Now open select cell and check one more check box, no Baseline dialog should pop-up
        parameterComparisonObject.clickSelectCellsTab();
        var newI = i+1;
        parameterComparisonObject.clickCheckBox(newI);                    
        parameterComparisonObject.clickOKBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), false);
        console.log("Test completed - Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817");
    });
    it('To verify Parameter Comparison select cells message - XYN-2895', function () {
        console.log("Test started - To verify Parameter Comparison select cells message - XYN-2895");
        //First test >6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();                        
        assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), true);
        assert.equal(browser.element(parameter.selectCellsoKBtn).getText(), config.textOK);
        assert.equal(browser.element(parameter.cancelButton_1).getText(), config.textCancel);
        var totHItemsNo = browser.elements(parameter.headerItem).value.length;
        var totGetVerify = config.selectCells.length;
        var isFound = false;
        if ((totHItemsNo >=1) && (totGetVerify >=1)){
            //print list
            var getHItemsList = browser.elements(parameter.headerItem).getText();
            var getVerifyList = config.selectCells;
            for (var i=0; i<=totGetVerify-1; i++){
                isFound = getHItemsList.includes(getVerifyList[i]);
            }
        }
        assert.equal(isFound, true);
        assert.equal(browser.element(parameter.selectAll).isVisible(), true);
        assert.equal(browser.element(parameter.selectCellsInfoSection).getText(), config.selectCellsInfoMsg);
        browser.element(parameter.cancelButton_1).click();
        browser.pause(300);
        var dfcText = browser.elements(parameter.defaultContainer).getText();
        expect(dfcText).to.include(config.pComparisonDefContText_1);
        expect(dfcText).to.include(config.pComparisonDefContText_2);
        expect(dfcText).to.include(config.pComparisonDefContText_3);
        console.log("Test completed - To verify Parameter Comparison select cells message - XYN-2895");
    });
    it('Parameter Comparison - Verify Initial data loading process - XYN-2465', function () {
        console.log("Test started - Verify Initial data loading process (Part-1: 'Viewing Options -> 'Select Differences' & Select Cells -> Uncheck/hide cell Id) - XYN-2465");
        //First test >6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();                        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        var cellName = [];
        var file = [];
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
                //get cellname, filename in list
                cellName.push(parameterComparisonObject.getCellName(i));
                file.push(parameterComparisonObject.getFile(i));
            }            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        baseMethods.verifySpinnerVisibility();
        //verify header items from the view
        var dfcText = browser.elements(parameter.pCompMainGridHeader).getText();
        dfcText = dfcText.toLowerCase();
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[0].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[1].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[2].toLowerCase());
        for (j=0; j<i-1; j++){
            expect(dfcText).to.include(cellName[j].toLowerCase());
            expect(dfcText).to.include(file[j].toLowerCase());
        }
        parameterComparisonObject.enterFilterValue(config.filterWithSpaceValues[0]);
        baseMethods.verifySpinnerVisibility();
        var totRows = browser.elements(parameter.mainViewTableRows).value.length;
        //get each row text and find for the filter value
        if (totRows >= 1){
            for (var n=1; n<=totRows; n++){
                var findVal = parameterComparisonObject.getMainbodyFilterValues(n);
                expect(findVal.toLowerCase()).to.include(config.filterWithSpaceValues[0].toLowerCase());
            }            
        } 
        //click on Viewing options to verify 'Highlight Differences' is selected default
        parameterComparisonObject.clickViewingOptionsMenu();
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.highlightDifferences.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }
        //hide a cell column(uncheck a column) reload data to verify unchecked column is not visible
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            parameterComparisonObject.unSelectCheckbox(1);
            var unCheckedCell = parameterComparisonObject.getFile(1)
            parameterComparisonObject.clickOKBtn();
        }
        console.log(unCheckedCell);
        baseMethods.verifySpinnerVisibility();
        var noDataPage = browser.element(parameter.noData).isVisible();
        if (!noDataPage){
            var ndfcText = browser.elements(parameter.pCompMainGridHeader).getText();
            expect(ndfcText.toLowerCase()).to.not.include(unCheckedCell.toLowerCase());
        }
        console.log("Test completed - Verify Initial data loading process (Part-1: 'Viewing Options -> 'Select Differences' & Select Cells -> Uncheck/hide cell Id) - XYN-2465");
    });
    it('Parameter Comparison - Verify Initial data loading process - XYN-2465', function () {
        console.log("Test started - Verify Initial data loading process (Part-2: 'Viewing Options -> 'Shoe Differences Only' & Select Cells -> Uncheck/hide cell Id) - XYN-2465");
        //First test >6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();                        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        var cellName = [];
        var file = [];
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
                //get cellname, filename in list
                cellName.push(parameterComparisonObject.getCellName(i));
                file.push(parameterComparisonObject.getFile(i));
            }            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        baseMethods.verifySpinnerVisibility();
        //verify header items from the view
        var dfcText = browser.elements(parameter.pCompMainGridHeader).getText();
        dfcText = dfcText.toLowerCase();
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[0].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[1].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[2].toLowerCase());
        for (j=0; j<i-1; j++){
            expect(dfcText).to.include(cellName[j].toLowerCase());
            expect(dfcText).to.include(file[j].toLowerCase());
        }
        parameterComparisonObject.enterFilterValue(config.filterWithSpaceValues[0]);
        baseMethods.verifySpinnerVisibility();
        var totRows = browser.elements(parameter.mainViewTableRows).value.length;
        //get each row text and find for the filter value
        if (totRows >= 1){
            for (var n=1; n<=totRows; n++){
                var findVal = parameterComparisonObject.getMainbodyFilterValues(n);
                expect(findVal.toLowerCase()).to.include(config.filterWithSpaceValues[0].toLowerCase());
            }            
        } 
        //click on Viewing options to click 'Shoe Differences Only' is selected default
        parameterComparisonObject.clickViewingOptionsMenu();
        //select the 'Show Differences Only' option from drop down
        browser.element(parameter.showDifferencesOnly).click();
        //re-open Viewing Options Menu
        parameterComparisonObject.clickViewingOptionsMenu();            
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.showDifferencesOnly.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }
        //hide a cell column(uncheck a column) reload data to verify unchecked column is not visible
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            parameterComparisonObject.unSelectCheckbox(1);
            var unCheckedCell = parameterComparisonObject.getFile(1)
            parameterComparisonObject.clickOKBtn();
        }
        console.log(unCheckedCell);
        baseMethods.verifySpinnerVisibility();
        var noDataPage = browser.element(parameter.noData).isVisible();
        if (!noDataPage){
            var ndfcText = browser.elements(parameter.pCompMainGridHeader).getText();
            expect(ndfcText.toLowerCase()).to.not.include(unCheckedCell.toLowerCase());
        }
        console.log("Test completed - Verify Initial data loading process (Part-2: 'Viewing Options -> 'Select Differences' & Select Cells -> Uncheck/hide cell Id) - XYN-2465");
    });
    it('Parameter Comparison - Verify data loading process against Baseline Cell ID - XYN-2772', function () {
        console.log("Test started - Verify data loading process against Baseline Cell ID - XYN-2772");
        //First test <6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparisonLTETest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();       
        parameterComparisonObject.clickSelectCellsTab();                 
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        var cellName = [];
        var file = [];
        //get the no of cell id's in the data by checking the select cells window
        mBoxTotRows = browser.element(parameter.selectCellsTotRows).value.length;
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<=mBoxTotRows; i++){
                //parameterComparisonObject.clickCheckBox(i);
                //get cellname, filename in list
                cellName.push(parameterComparisonObject.getCellName(i));
                file.push(parameterComparisonObject.getFile(i));
            }            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        baseMethods.verifySpinnerVisibility();
        //verify header items from the view
        var dfcText = browser.elements(parameter.pCompMainGridHeader).getText();
        dfcText = dfcText.toLowerCase();
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[0].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[1].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[2].toLowerCase());
        for (j=0; j<i-1; j++){
            expect(dfcText).to.include(cellName[j].toLowerCase());
            expect(dfcText).to.include(file[j].toLowerCase());
        }         
        //By default highlight Differences has been selected
        //click on Viewing options to verify 'Highlight Differences' is selected default
        parameterComparisonObject.clickViewingOptionsMenu();
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.highlightDifferences.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }        
        //select the 'Show Differences Only' option from drop down
        browser.element(parameter.showDifferencesOnly).click();
        //re-open Viewing Options Menu
        parameterComparisonObject.clickViewingOptionsMenu();            
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.showDifferencesOnly.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }
        //close the dropdown
        parameterComparisonObject.clickViewingOptionsMenu();
        //hide a cell column(uncheck a column) reload data to verify unchecked column is not visible        
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            parameterComparisonObject.unSelectCheckbox(1);
            var unCheckedCell = parameterComparisonObject.getFile(1)
            parameterComparisonObject.clickOKBtn();
        }
        console.log(unCheckedCell);
        var noDataPage = browser.element(parameter.noData).isVisible();
        if (!noDataPage){
            baseMethods.verifySpinnerVisibility();
            var ndfcText = browser.elements(parameter.pCompMainGridHeader).getText();
            expect(ndfcText).to.not.include(unCheckedCell);
            //verify for fileter value
            parameterComparisonObject.enterFilterValue(config.filterWithSpaceValues[0]);
            browser.pause(300);
            var totRows = browser.elements(parameter.mainViewTableRows).value.length;
            //get each row text and find for the filter value
            if (totRows >= 1){
                for (var n=1; n<=totRows; n++){
                    var findVal = parameterComparisonObject.getMainbodyFilterValues(n);
                    expect(findVal.toLowerCase()).to.include(config.filterWithSpaceValues[0].toLowerCase());
                }            
            }
        }
        console.log("Test completed - Verify data loading process against Baseline Cell ID - XYN-2772");
    });
});