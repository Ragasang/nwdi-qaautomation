let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2923', function () {
        console.log("Test started - Verify if the data restores after filtering for an invalid value and clearing it - XYN-2923");        
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        parameterComparisonObject.enterFilterValue(config.filterVal);
        var findVal = false;
        if (findVal == false){
            findVal = parameterComparisonObject.getFilterValues(config.filterVal);
        }
        assert.equal(findVal, true);
        parameterComparisonObject.clearFilter();
        noDataPage = browser.element(parameter.noData).isVisible();
        //data should get displayed after clearing the filter
        //1. No data Available page should not get displayed
        assert.equal(noDataPage, false);
        //2. At least some values get present
        tdValue = browser.element(parameter.pcTableData).getText();
        if(tdValue == ""){
            assert.fail();
        }
        console.log("Test completed - Verify if the data restores after filtering for an invalid value and clearing it - XYN-2923");
    });
});