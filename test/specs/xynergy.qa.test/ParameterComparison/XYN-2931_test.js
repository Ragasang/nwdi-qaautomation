let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931', function () {
        console.log("Test started - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation() ;     
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        firstParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(firstParameterVal);
        //second test >6 dataset which also will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        secondParameterVal = browser.element(parameter.parameterSpanTitle).getText();
        console.log(secondParameterVal);   
        //Parameter values should not equal since two different datasets are loaded
        assert.notEqual(firstParameterVal, secondParameterVal); 
        console.log("Test completed - Verify if replacing an existing dataset with another set will refresh Parameter comparison view - XYN-2931");
    });
});