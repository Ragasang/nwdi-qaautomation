let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905', function () {
        console.log("Test started - Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        parameterComparisonObject.enterFilterValue(config.filterVal1);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal2);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal3);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal4);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal5);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal6);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal7);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal8);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        parameterComparisonObject.enterFilterValue(config.filterVal9);
        noDataPage = browser.element(parameter.noData).isVisible();
        assert.equal(noDataPage, true);
        parameterComparisonObject.clearFilter();
        browser.pause(300);

        console.log("Test completed - Verify that message names should be not be displayed on the grid - XYN-3033 and Parameter needs to be hidden - 2905");
    });
});