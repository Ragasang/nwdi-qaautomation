let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Verify if multiple parameters are separated by | in comparison view - XYN-2935', function () {
        console.log("Test started - Verify if multiple parameters are separated by | in comparison view - XYN-2935");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<5; i++){
                parameterComparisonObject.clickCheckBox(i);
            }
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        //Get text from each parameter's column
        var j = 1;
        var findPipe = false;
        while( j > 0){
            console.log(j);
            if (findPipe == false){
                findPipe = parameterComparisonObject.getFValues(j, config.pipeSymb);
            } 
            if (findPipe == false){
                findPipe = parameterComparisonObject.getSValues(j, config.pipeSymb);
            } 
            if (findPipe == false){
                findPipe = parameterComparisonObject.getTValues(j, config.pipeSymb);
            }
            if (findPipe == false){
                findPipe = parameterComparisonObject.getFtValues(j, config.pipeSymb);
            }           
            if (findPipe == true){
                j = 0;
            }
            else{
                j++; 
            }                 
        }
        assert.equal(findPipe, true);        
        console.log("Test completed - Verify if multiple parameters are separated by | in comparison view - XYN-2935");
    });
});