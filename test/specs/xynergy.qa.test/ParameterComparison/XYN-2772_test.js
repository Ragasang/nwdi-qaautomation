let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Parameter Comparison - Verify data loading process against Baseline Cell ID - XYN-2772', function () {
        console.log("Test started - Verify data loading process against Baseline Cell ID - XYN-2772");
        //First test <6 dataset which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparisonLTETest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();       
        parameterComparisonObject.clickSelectCellsTab();                 
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        var cellName = [];
        var file = [];
        //get the no of cell id's in the data by checking the select cells window
        mBoxTotRows = browser.element(parameter.selectCellsTotRows).value.length;
        if (mboxOpened){
            //click 4 parameters to compare
            for (var i=1; i<=mBoxTotRows; i++){
                //parameterComparisonObject.clickCheckBox(i);
                //get cellname, filename in list
                cellName.push(parameterComparisonObject.getCellName(i));
                file.push(parameterComparisonObject.getFile(i));
            }            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        baseMethods.verifySpinnerVisibility();
        //verify header items from the view
        var dfcText = browser.elements(parameter.pCompMainGridHeader).getText();
        dfcText = dfcText.toLowerCase();
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[0].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[1].toLowerCase());
        expect(dfcText).to.include(config.pComparisonMainHeaderItems[2].toLowerCase());
        for (j=0; j<i-1; j++){
            expect(dfcText).to.include(cellName[j].toLowerCase());
            expect(dfcText).to.include(file[j].toLowerCase());
        }         
        //By default highlight Differences has been selected
        //click on Viewing options to verify 'Highlight Differences' is selected default
        parameterComparisonObject.clickViewingOptionsMenu();
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.highlightDifferences.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }        
        //select the 'Show Differences Only' option from drop down
        browser.element(parameter.showDifferencesOnly).click();
        //re-open Viewing Options Menu
        parameterComparisonObject.clickViewingOptionsMenu();            
        var totItems = browser.elements(parameter.viewingOptionMenuItems);        
        if (totItems.value.length > 1){
            var getVal = totItems.getText();
            for (var t=0; t<totItems.value.length; t++){                
                if(getVal[t].toLowerCase() == config.showDifferencesOnly.toLowerCase()){
                    console.log(getVal[t]);
                    var styleVisible = browser.elements(parameter.viewingOptionsStyle).getAttribute(config.style);
                    console.log(styleVisible[t]);
                    expect(styleVisible[t].toLowerCase()).to.include(config.styleVisble.toLowerCase());
                }                
            }            
        }
        //close the dropdown
        parameterComparisonObject.clickViewingOptionsMenu();
        //hide a cell column(uncheck a column) reload data to verify unchecked column is not visible        
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            parameterComparisonObject.unSelectCheckbox(1);
            var unCheckedCell = parameterComparisonObject.getFile(1)
            parameterComparisonObject.clickOKBtn();
        }
        console.log(unCheckedCell);
        var noDataPage = browser.element(parameter.noData).isVisible();
        if (!noDataPage){
            baseMethods.verifySpinnerVisibility();
            var ndfcText = browser.elements(parameter.pCompMainGridHeader).getText();
            expect(ndfcText).to.not.include(unCheckedCell);
            //verify for fileter value
            parameterComparisonObject.enterFilterValue(config.filterWithSpaceValues[0]);
            browser.pause(300);
            var totRows = browser.elements(parameter.mainViewTableRows).value.length;
            //get each row text and find for the filter value
            if (totRows >= 1){
                for (var n=1; n<=totRows; n++){
                    var findVal = parameterComparisonObject.getMainbodyFilterValues(n);
                    expect(findVal.toLowerCase()).to.include(config.filterWithSpaceValues[0].toLowerCase());
                }            
            }
        }
        console.log("Test completed - Verify data loading process against Baseline Cell ID - XYN-2772");
    });
});