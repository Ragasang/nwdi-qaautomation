let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        
    });
    it('Verify Diagnostics View tab is displayed on canvas split window XYN-1694', function() {
        console.log("Test Started - to verify Diagnostics view is displayed on canvas split window");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        //console.log(browser.element(parameter.MessagesDraggable).value.length);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        //console.log(browser.element(parameter.DiagnostaticsDraggable).value.length);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.WidgetRightBox);
        browser.pause(500);
        var tabNAme=browser.element("[data-test-id='Diagnostics']").getText();
        console.log(tabNAme);
        expect(tabNAme).to.include("Diagnostics");
        var splitterWindowData=browser.elements("//li[contains(@data-test-id,'Description')]");
        var descColumnValue=browser.element("//app-data-grid[@name='diagnosis-grid']//datatable-body-row[contains(@class,'active')]//datatable-body-cell[5]");
        console.log(splitterWindowData.getText());
        console.log(descColumnValue.getText());
        expect(splitterWindowData.getText()).to.include(descColumnValue.getText());
        console.log("Test Completed - to verify Diagnostics view is displayed on canvas split window- 1694");
    });
});