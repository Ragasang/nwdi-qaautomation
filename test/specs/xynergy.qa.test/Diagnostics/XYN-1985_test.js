let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Diagnosis', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        
    });
    it.skip('verify synchronization of data on timesiries and sequence diagram widgets XYN-1985', function () {
        console.log("verify synchronization of data on timesiries and sequence diagram widgets XYN-1985 -Started");
        if(!isElectron){
        browser.windowHandleFullscreen();
        }
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048";               
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        diagnosticsObject.enterFilterTextInDiagnostics(".241");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetBottomBox);
        var diagnosticsGrid=browser.elements("//app-data-grid[@name='diagnosis-grid']");
        var rowCount = diagnosticsGrid.elements("//datatable-row-wrapper").value.length;
        var rowToSelect=browser.element("//datatable-row-wrapper");
        rowToSelect.click();
        console.log(rowToSelect.getText());
        // var timeColumn=rowToSelect.element("//datatable-body-cell[contains(@class,'active')][5]");//div[contains(@title,'.671')]");
        // console.getText(timeColumn.getText());
        browser.pause(2000);
        var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
        sequenceRowSelected.click();
        browser.pause(100);
        var sequenceToolTip=browser.element("//div[@class='tooltip-container']");
        var sequenceToolTipContent=sequenceToolTip.getText();
        console.log(sequenceToolTipContent);
        expect(rowToSelect.getText()).to.include(sequenceToolTipContent);
        // var messagesGrid=browser.elements("//app-data-grid[@name='messages-grid']");
        // var messageRowSelected=messagesGrid.elements("//datatable-body-row[contains(@class,'active')]");
        // //var messageTimeColumn=messagesGrid.element("//datatable-body-cell[contains(@class,'sort-active')][2]/div[contains(@title,'.671')]");
        // ////datatable-body-row[@class='datatable-body-row active datatable-row-even']//div[contains(text(),'03/30/2018 17:57:04.080')]
        // //var messageTimeColumn=messagesGrid.$(".datatable-body-cell.")
        // //messageRowSelected.click();
        // browser.pause(3000);
        // console.log(messageRowSelected.getText());
        //console.log(timeColumn);
        // if (sequenceToolTipContent == timeColumn) {
        //     assertValue = true;
        // }
        // assert.equal(assertValue, true);
         console.log("verify synchronization of data on timesiries and sequence diagram widgets XYN-1985 -Done");
    });
});