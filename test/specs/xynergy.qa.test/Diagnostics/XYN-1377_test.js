let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    before(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();           
        }       
    });    
    it('Verify Diagnostics View Splitter window opened by default and details of the first record is displayed on splitter window XYN-1377, XYN-1643', function() {
        console.log("Test Started - to verify Diagnostics view Splitter window opened and details of the selected record is displayed XYN-1377, XYN-1643");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable)
        browser.pause(300);
        var diagnosticsGrid=browser.elements("//app-data-grid[@name='diagnosis-grid']");
        var selectedRow=diagnosticsGrid.elements("//datatable-body-row[contains(@class,'active')]");
        var splitterWindowData=browser.elements("//li[contains(@data-test-id,'Description')]");
        var descColumnValue=selectedRow.element("//datatable-body-cell[4]");
        console.log(selectedRow.getText(),descColumnValue.getText());
        console.log(splitterWindowData.getText());
        expect(splitterWindowData.getText()).to.include(descColumnValue.getText());
        console.log("Test Completed - to verify Diagnostics view Splitter window opened and details of the selected record is displayed XYN-1377, XYN-1643");
    });
    it('Verify Diagnostics View filtering on all columns XYN-1377', function() {
        console.log("Test Started - to verify Diagnostics view filtering works on All columns XYN-1377");
        if(isElectron()){
                browser.element("//app-header-bar//*[@data-test-id='Data']").click();
                browser.pause(500);
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);         
                var closeMBox=browser.element("//app-error-dialog//*[contains(@class,'close')]");
            if(closeMBox.isVisible()){
                closeMBox.click();    
            }             
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable)
        browser.pause(300);
        var diagnosticsGrid=browser.elements("//app-data-grid[@name='diagnosis-grid']");
        diagnosticsObject.enterFilterTextInDiagnostics("PHY");
        browser.pause(400);
        var j = 1;
        var rowCount = diagnosticsGrid.elements("//datatable-row-wrapper").value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements("//datatable-row-wrapper[" + j + "]");
            console.log(rowContent.element("//span[@title='PHY']"));
            expect(rowContent.element("//span[@title='PHY']").getText()).to.include('PHY');
            j++;
            rowCount--;
        }
        console.log("Test Completed - to verify Diagnostics view filtering works on All columns XYN-1377");
    });

});