let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();           
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        
    });
    it('Diagnostics - To verify Filtering text with double quotes  XYN-2767', function() {
        console.log("Test Started - Diagnostics - To verify Filtering text with double quotes XYN-2767");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable)
        browser.pause(300);
        var colSizeBefore=diagnosticsObject.getColumnWidthVal();
        console.log(colSizeBefore);       
        browser.execute(()=>{
            let x=document.querySelector('[title="Protocol"]');
            var widthVal="130px";
            if(!x.getAttribute("style").includes(widthVal)){
                x.style.width=widthVal;
            }else{
                x.style.width="160px";
            }           
        });
        var colSizeAfter=diagnosticsObject.getColumnWidthVal();
        console.log(colSizeAfter);
        expect(colSizeBefore).not.to.be.equals(colSizeAfter);
        console.log("Test Completed - Diagnostics - To verify Filtering text with double quotes - XYN-2767");
    });
});