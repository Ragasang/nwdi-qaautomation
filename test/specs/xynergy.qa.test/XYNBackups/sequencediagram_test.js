let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });
    afterEach(function () {
        if (isElectron()) {
            console.log("closing all open views");
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            browser.pause(500);
            console.log("closing all open views");
            var closeAllViews = browser.element("[data-test-id='close-all-btn']");
            if (closeAllViews.isVisible()) {
                closeAllViews.click();
                browser.pause(300);
                console.log("clicking on close btn");
                //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
                var modalBox = browser.element("//modal-box");
                var mBoxCloseBtn = modalBox.element("[data-test-id='close-all-views']");
                if (mBoxCloseBtn.isVisible()) {
                    mBoxCloseBtn.click();
                    console.log("clicked on close btn");
                }
                browser.pause(300);
                //close views menu
                var viewsHeader = browser.element("//views-menu");
                var closeViewsMenu = viewsHeader.element("[data-test-id='close-button']");
                if (closeViewsMenu.isVisible()) {
                    closeViewsMenu.click();
                }
            }
        }

    });

    // it('Verify clicking on themes', function () {
    //     console.log("Test started - for sequence diagram tab is displayed for multiple devices");
    //     baseMethods.clickOnUserInfo();
    //     var userinfo = browser.element("//div[@class='user-info']");
    //     var userinfomenu = userinfo.elements("//div[@class='user-info-menu']");
    //     var themetoggle = userinfomenu.element("//div[@class='app-theme-toggle']")
    //     console.log(themetoggle.getText());
    //     themetoggle.click();
    //     userinfo.element("//div[@class='profile-header pointer-cursor']").click();
    //     browser.pause(2000);
    //     console.log(userinfomenu.element("//div[@class='app-theme-toggle']").getText());
    //     console.log("Test completed - for sequence diagram tab is displayed for multiple devices");
    //     // browser.waitForExist("[data-highcharts-chart='0']");
    // });
    /*  it('Verify if sequence diagram tab is displayed for multiple devices', function(){
         console.log("Test started - for sequence diagram tab is displayed for multiple devices");
         this.skip();
         baseMethods.advancedSearchViewForSingleDeviceData();
         baseMethods.clickOnOpenInCanvasBtn();
         baseMethods.waitTillDataLoads();
         genericObject.clickOnDTTab(parameter.ViewsMenu);
         // baseMethods.mapDragAndDrop();
         browser.execute(dragAndDrop, "[data-test-id='WidgetUniqueIdDiagramViewComponent']", "[data-test-id='message-template']");
         var count= document.elements("[class='widget-menu text-overflow-ellipsis selected']").count;
         //<div class="message-template" data-test-id="message-template"><span>Drag and drop widgets from views panel to canvas</span></div>
         //<div class="widget-menu text-overflow-ellipsis selected" data-test-id="Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819"><i aria-hidden="true" class="icon-large-size icon-sequence-diagram"></i><span data-test-id="right-tab-menu-header" title="Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819">Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819</span><a class="tab-close-icon"><i class="material-icons grow">close</i></a></div>
         console.log(count);
         assert.notEqual(count, 1);
         console.log("Test completed - for sequence diagram tab is displayed for multiple devices");
        // browser.waitForExist("[data-highcharts-chart='0']");
     });
     it('Verify if sequence diagram tab is displayed for a device', function(){
         console.log("Test started - for sequence diagram tab is displayed for a device");
         this.skip();
         baseMethods.advancedSearchViewForSingleDeviceData();
         baseMethods.clickOnOpenInCanvasBtn();
         baseMethods.waitTillDataLoads();
         genericObject.clickOnDTTab(parameter.ViewsMenu);
         // baseMethods.mapDragAndDrop();
         //<div class="floating-box" dnd-draggable="" draggable="true" data-test-id="WidgetUniqueIdDiagramViewComponent" style="cursor: pointer;"><div><i aria-hidden="true" class="icon-large-size icon-sequence-diagram"></i></div><span class="views-widget-text">Sequence Diagram</span></div>
         browser.execute(dragAndDrop, "[type='WidgetUniqueIdDiagramViewComponent']", "[data-test-id='message-template']");
         var count= document.elements("[class='widget-menu text-overflow-ellipsis selected']").count;
         //<div class="widget-menu text-overflow-ellipsis selected" data-test-id="Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819"><i aria-hidden="true" class="icon-large-size icon-sequence-diagram"></i><span data-test-id="right-tab-menu-header" title="Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819">Phone_Qualcomm Phone_Unknown_990004677846028_310120037023819</span><a class="tab-close-icon"><i class="material-icons grow">close</i></a></div>
         console.log(count);
         assert.equal(count, 1);
         console.log("Test completed - for sequence diagram tab is displayed for a device");
        // browser.waitForExist("[data-highcharts-chart='0']");
     }); */
    // it('verify events and latency calculation on sequence diagram', function () {
    //     console.log("Test started- Events plot on sequence diagram Xyn-1027");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetRoot);
    //     baseMethods.clickOnCalculateLatency();
    //     baseMethods.selectDataToCalculateLatency();
    //     baseMethods.clickOnCalculate();
    //     baseMethods.clickOnColumnsbutton();
    //     var layerdata = browser.element("[data-test-id='TimeSeriesPlottedData']");
    //     baseMethods.clickOnSequenceData("Data Layers");
    //     baseMethods.clickOnHideBtn();
    //     browser.pause(5000);
    //     baseMethods.clickOnCloseBtn();
    //     browser.pause(5000);
    //     console.log("Test completed - Latency calculation and Events plot on sequence diagram Xyn-1027");
    // });

    it('Verify whether event symbol is displaying on the sequence diagram- XYN-2366', function () {
        console.log("Test started- event symbol is displaying on the sequence diagram XYN-2366");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        rowCount = browser.elements(parameter.seqRows).value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var imgVal = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']//img").isVisible();
                assert.equal(imgVal, true);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- event symbol is displaying on the sequence diagram XYN-2366");
    });

    it('Verify of two lanes in sequence diagram- XYN-1739', function () {
        console.log("Test started- verification of two lanes in the sequence diagram XYN-1739");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        var sequenceBlock = browser.elements(parameter.seqRows);
        var headerBlock = sequenceBlock.elements(parameter.seqHeaderBlock);
        var leftItem = headerBlock.element(parameter.seqHeaderLineLeft);
        expect(leftItem.getText()).to.include(config.sequenceLeftLaneItemName);
        var rightItem = headerBlock.element(parameter.seqHeaderLineRight);
        expect(rightItem.getText()).to.include(config.sequenceRightLaneItemName);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- verification of two lanes in the sequence diagram XYN-1739");
    });

    it('Verify whether RRC messages are present on Sequence diagram, XYN-2777', function () {
        console.log("Test started to verify RRC connection message");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        // var sequenceBlock = browser.elements("//xyn-sequence-item");
        var rowMessage = browser.elements(parameter.rowMessage);
        var Message = rowMessage.value;
        var value = false;
        Message.forEach(element => {
            var messageValue = element.getText();
            if (messageValue == config.rrcConnection) {
                value = true;
            }
        })
        assert.equal(value, true);
        console.log("Test completed to verify RRC connection message-XYN-2777");
    });

    it('Verify events and latency calculation sync with data layers, XYN-1710', function () {
        console.log("Test started to verify sync between Events and latency calculation with data layers");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        baseMethods.clickOnColumnsbutton();
        var datalayer = browser.element(parameter.dataLayersTab);
        datalayer.click();
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var CalculatedLatency = browser.element(parameter.calculateLatency);
        var bool = CalculatedLatency.isVisible();
        assert(bool, true);
        baseMethods.clickOnHideBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.clickOnCloseBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var datalayerEvent = browser.element(parameter.seqLayerData);
        var text = datalayerEvent.getText();
        expect(text).to.include(config.eventName);
        baseMethods.clickOnHideBtn();
        var j = 1;
        rowCount = browser.elements("//xyn-sequence-item").value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var img = rowImg.element("//img").isVisible();
                assert.equal(img, false);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.clickOnCloseBtn();
        var NoLayers = browser.isVisible(parameter.noLayerMsg);
        assert.equal(NoLayers, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test started to verify sync between Events and latency calculation with data layers - Xyn-1710");
    });


    it("Verify that selected messages to calculate latency is showing up under data layers - XYN-1467", function () {
        console.log("Selected message to calculate latency is showing under data layers");
        var result = [];
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        var result = baseMethods.SelectedDataToCalculateLatency();
        // var value = SelectedOne.value;
        SequenceDiagramObject.clickOnCalculateBtn();
        baseMethods.clickOnColumnsbutton();
        baseMethods.clickOnSequenceData(config.dataLayersTab);
        var datalayer = browser.element(parameter.seqLayerData);
        var text = datalayer.getText();
        browser.pause(200);
        console.log(result[0], result[1]);
        expect(text).to.include(result[0]);
        expect(text).to.include(result[1]);
        console.log("Test completed for - Selected message to calculate latency is showing under data layers-XYN-1467");
    });

    it("Verify that time stamp is showing time on sequence diagram on clicking - XYN-1460", function () {
        console.log("Timestamp should be visible on sequence diagram when clicked on Show Timestamps");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        var buttonActive = browser.element(parameter.timeStampsBtn).getAttribute("class");
        expect(buttonActive).to.include(config.activeBtn);
        browser.pause(300);
        var result = browser.element(parameter.timeStampsText).getAttribute("style");
        expect(result).to.include(config.block);
        SequenceDiagramObject.clickOnShowTimestampsBtn();
        buttonActive = browser.element(parameter.timeStampsBtn).getAttribute("class");
        expect(buttonActive).not.to.include(config.activeBtn);
        console.log("Test completed - Timestamp should be visible on sequence diagram when clicked on Show Timestamps - XYN-1460");


    });
    it('Verify whether event symbol is displaying info on hovering- XYN-2369', function () {
        console.log("Test started- To check event symbol is displaying info on hovering- XYN-2396");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        SequenceDiagramObject.clickOnPlotedEvent();
        var eventtooltip = browser.element(parameter.eventImgTooltip);
        var eventinfo = eventtooltip.isVisible();
        assert(eventinfo, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed-To check event symbol is displaying info on hovering- XYN-2396");

    });


    it('To verify selection of messages for latencty calculation- XYN-1466', function () {
        console.log("Test started- To verify selection of messages for latencty calculation- XYN-1466");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        //browser.waitForExist("[data-test-id='diagram-view-droppable']");
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        console.log("Test completed- To verify selection of messages for latencty calculation- XYN-1466");


    });

    // it('To verify legend and Node name in sequence diagram- XYN-1923', function () {
    //     console.log("Test started- To verify legend and node name in sequence diagram- XYN-1923");
    //     baseMethods.clickOnAdvancedSearchViewIcon_ALF();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
    //     browser.pause(300);
    //     var LegendTitle = browser.element("[data-test-id='legend-title']").getText();
    //     assert(LegendTitle == "Data Source");
    //     var NodeHeader = browser.element("[data-test-id='right-node-header-text']").getText();
    //     assert(NodeHeader == "UXM")
    //     console.log("Test completed- To verify legend and node name in sequence diagram- XYN-1923");

    // });
    it('To check Events & Latency  calculation sync with  Data layers, XYN-1449', function () {
        console.log("Test started To check Events & Latency  calculation sync with  Data layers, XYN-1449");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        baseMethods.clickOnColumnsbutton();
        browser.pause(200);
        var datalayer = browser.element(parameter.dataLayersTab);
        datalayer.click();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert.equal(Nolayer, true);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        baseMethods.clickOnHideBtn();
        assert(latency, false);
        baseMethods.clickOnCloseBtn();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert(Nolayer, true);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        SequenceDiagramObject.clickOnPlotedEvent();
        var eventtooltip = browser.element(parameter.eventImgTooltip);
        var eventinfo = eventtooltip.isVisible();
        assert(eventinfo, true);
        baseMethods.clickOnHideBtn();
        baseMethods.clickOnCloseBtn();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert(Nolayer, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed To check Events & Latency  calculation sync with  Data layers, XYN-1449");

    });
    it('To verify styling in sequence diagram, XYN-1461', function () {
        console.log("To verify styling in sequence diagram, XYN-1461");
        //  baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        var buttonActive = browser.element(parameter.activeButton).getAttribute("class");
        assert(buttonActive == config.selectedMessage);
        console.log("Test completed To verify styling in sequence diagram, XYN-1461");

    });


    //it("Verify whether text overflow is restricted to 30 char - XYN-1290", function () {
    //    console.log("Text character in tab is restricted to 30 char");
    //    baseMethods.clickOnAdvancedSearchViewIcon();
    //    genericObject.clickOnDTTab(parameter.ViewsMenu);
    //    baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetRoot);
    //    var tab = browser.element("[data-test-id = 'right-tab-menu-header']");
    //    var textvalue = tab.value;
    //    var length = textvalue.length;
    //    if (length <= 30) {
    //        value = true;
    //    } else value = false;
    //    assert.equal(value, true);
    //});

    it("Verify that dataset with single device is displaying single tab in sequence diagram - XYN-1429", function() {
        console.log("Test started - To verify that dataset with single device is displaying single tab in sequence diagram");
        genericObject.clickOnDTTab(parameter.scope);
        var device = SequenceDiagramObject.isDevicePresent(config.sprintMODeviceName);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        var tabDevice = browser.elements(parameter.seqTabs).value.length;
        assert.equal(device, tabDevice);
        assert.equal(config.compareValueWithOne, tabDevice);
        console.log("Test completed for tab verification in sequence diagram - XYN-1429");


    });
    it('To Verify the different selected Latencies color, XYN-1468', function () {
        console.log("To Verify the different selected Latencies color, XYN-1468");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor1 = browser.element(parameter.firstLatencyColor).getAttribute("style");
        console.log(latencyColor1);
        SequenceDiagramObject.selectDataToCalculateLatency2();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor2 = browser.element(parameter.secondlatencyColor).getAttribute("style");
        console.log(latencyColor2);
        assert.notEqual(latencyColor1, latencyColor2);
        console.log("Test completed - To Verify the different selected Latencies color, XYN-1468");

}); 

it.skip("Verify that dataset with multiple device is displaying multiple tab in sequence diagram - XYN-1430", function() {
    console.log("Test started - To verify that dataset with multiple device is displaying multiple tab in sequence diagram");
    //genericObject.clickOnDTTab(parameter.Scope);
    browser.pause(500);
    var device1 = SequenceDiagramObject.isDevicePresent(config.sprintMODeviceName);
    var device2 = SequenceDiagramObject.isDevicePresent(config.sprintMTDeviceName);
    console.log(device1);
    console.log(device2);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
    var tabDevice1 = SequenceDiagramObject.isTabPresent(config.sprintMODeviceName);
    var tabDevice2 = SequenceDiagramObject.isTabPresent(config.sprintMTDeviceName);
    var tabDeviceCount = SequenceDiagramObject.getSequenceTabCount(config.DeviceType);
    console.log(tabDeviceCount);
    //console.log(tabDeviceCount);
    assert.equal(device1, tabDevice1);
    assert.equal(device2, tabDevice2);
    var result = false;
    if(tabDeviceCount>1){
        result=true;
    }
    assert.equal(result,true);
    console.log("Test completed for tab verification in sequence diagram - XYN-1430");
});

}); 