let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });
    afterEach(function () {
        if (isElectron()) {
            eventsDataObject.clearEventName();
            browser.pause(200);
            eventsDataObject.closeEventWidget();
            /*console.log("closing all open views");
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            browser.pause(500);
            console.log("closing all open views");
                var closeAllViews=browser.element("[data-test-id='close-all-btn']");
                if(closeAllViews.isVisible()){
                closeAllViews.click();
                browser.pause(300);
                console.log("clicking on close btn");
                //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
                var modalBox=browser.element("//modal-box");
                var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
                if(mBoxCloseBtn.isVisible()){
                mBoxCloseBtn.click();
                console.log("clicked on close btn");
                }
                browser.pause(300);
                //close views menu
                var viewsHeader=browser.element("//views-menu");
                var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
                if(closeViewsMenu.isVisible()){
                    closeViewsMenu.click();
                }
                }*/
        }
    });
    it('Events | Add to Favorites to be remembered - XYN-1992', function () {
        console.log("Events | Add to Favorites to be remembered - XYN-1992");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        genericObject.setEventName(config.eventName);
        eventsDataObject.clickOnFilteredEvent();
        //console.log(addFavorites.value.length);
        eventsDataObject.clickOnAddToFavElem(type);
        eventsDataObject.closeEventWidget();
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.EventsMenu);
        genericObject.setEventName(config.eventName);
        eventsDataObject.clickOnFilteredEvent();
        assert.equal(true, eventsDataObject.isVisibleRemoveFromFavorites());
        browser.pause(200);
        eventsDataObject.clickOnRemoveFavElem();
        console.log("Test completed: Events | Add to Favorites to be remembered - XYN-1992");
    });

    it('Verify whether selected Event have options and those options are functioning - XYN-2109', function () {
        console.log("Test started for'Verify whether selected Event have options and those options are functioning - XYN-2109'");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        var addFavorites = browser.elements(parameter.addToFavorites);
        var addToFavName = addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText, addToFavName);
        eventsDataObject.clickOnAddToFavElem(type);
        eventsDataObject.clearEventName();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clickOnFilteredEvent();
        var Removefavorites = browser.elements(parameter.addToFavorites);
        var RemoveFavouriteBtn = Removefavorites.element(parameter.remFavButton).getText();
        assert.equal(config.removeFavText, RemoveFavouriteBtn);
        eventsDataObject.clickOnRemoveFavElem();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        browser.pause(200);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName2);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        browser.pause(200);
        var addToFavName2 = addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText, addToFavName2);  
        eventsDataObject.clickOnAddToFavElem(type);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        browser.pause(200);
        eventsDataObject.clickOnRemoveFavElem();        
        console.log("Test completed for 'Verify whether selected event have options and those options are functioning - XYN-2109'");
    });

    it('Verify whether unavailable events are greyed out - XYN-1989', function () {
        console.log("Verify whether unavailable events are greyed out - XYN-1989");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clickOnExpander();
        browser.pause(300);
        var eventRow = browser.elements(parameter.disabledEventOrMetric);
        //console.log(eventRow.value.length);
        eventsDataObject.clickOnToggle();
        var eventRow1 = browser.elements(parameter.disabledEventOrMetric).value.length;
        assert.equal(eventRow1, config.compareValueWithZero);
        console.log("Test completed for the verification of greyed out events when toggle button is off - XYN-1989");
    });

    it('To verify events widget dock pin, toggle button, hint messsage - XYN-1521', function () {
        console.log("Test started To verify events widget dock pin, toggle button, hint messsage - XYN-1521'");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        browser.pause(500);
        eventsDataObject.eventWidgetDock();
        eventsDataObject.clickOnToggle();
        console.log("Events widget succesfully docked");
        console.log("Toggle button is available");
        var Hintmessage = browser.element(parameter.eventsNotification).getText();
        assert.equal("Drag and drop items from below to views.", Hintmessage);
        console.log("Test To verify events widget dock pin, toggle button, hint messsage - XYN-1521'");
    });
    it('Verify whether typed in filter is remembered once it is closed/collapsed under Event widget - XYN-2087', function () {
        console.log("Test started for Verify whether typed in filter is remembered once it is closed/collapsed under Event widget- XYN-2087'");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName);
        browser.pause(200);
        var event1 = browser.elements(parameter.filteredEvent).getText();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        var clear = browser.elements(parameter.clearEventName);
        if (clear.isVisible()) {
            console.log("Entered keywords are remembered");
        }
        else {
            console.log("Entered keywords are not remembered");
        }
        var event2 = browser.elements(parameter.filteredEvent).getText();
        assert.equal(event1, event2, config.eventName);
        console.log("Test completed to Verify whether typed in filter is remembered once it is closed/collapsed under Event widget - XYN-2087'");

    });
});