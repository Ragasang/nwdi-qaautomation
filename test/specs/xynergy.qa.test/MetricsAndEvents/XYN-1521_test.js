let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });

    it('To verify events widget dock pin, toggle button, hint messsage - XYN-1521', function () {
        console.log("Test started To verify events widget dock pin, toggle button, hint messsage - XYN-1521'");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        browser.pause(500);
        eventsDataObject.eventWidgetDock();
        eventsDataObject.clickOnToggle();
        console.log("Events widget succesfully docked");
        console.log("Toggle button is available");
        var Hintmessage = browser.element(parameter.eventsNotification).getText();
        assert.equal("Drag and drop items from below to views.", Hintmessage);
        console.log("Test To verify events widget dock pin, toggle button, hint messsage - XYN-1521'");
    });

});