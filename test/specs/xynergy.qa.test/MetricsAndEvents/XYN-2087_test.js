let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });

    it('Verify whether typed in filter is remembered once it is closed/collapsed under Event widget - XYN-2087', function () {
        console.log("Test started for Verify whether typed in filter is remembered once it is closed/collapsed under Event widget- XYN-2087'");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName);
        browser.pause(200);
        var event1 = browser.elements(parameter.filteredEvent).getText();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        var clear = browser.elements(parameter.clearEventName);
        if (clear.isVisible()) {
            console.log("Entered keywords are remembered");
        }
        else {
            console.log("Entered keywords are not remembered");
        }
        var event2 = browser.elements(parameter.filteredEvent).getText();
        assert.equal(event1, event2, config.eventName);
        console.log("Test completed to Verify whether typed in filter is remembered once it is closed/collapsed under Event widget - XYN-2087'");

    });

});