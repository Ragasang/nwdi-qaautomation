let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

    it('Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113', function(){
        console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements(type);
        browser.pause(200);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.TimeseriesDroppable);
        metricsDataObject.clickOkOnErrorMsg();
        metricsDataObject.clickOnTimeseriesCloseBtn();

        // genericObject.clickOnDTTab(parameter.ViewsMenu);
        // genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
        // browser.pause(300);
        // baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.MapDroppable);
        // browser.pause(300);
        // metricsDataObject.clickOnMapErrorMsg();

        console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
    });

    it.skip('Verify whether selected metric have options and those options are functioning - XYN-1519', function () {
        console.log("Test started for'Verify whether selected metric have options and those options are functioning - XYN-1519'");
        var metricValue=config.metricName;
        try{genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements(type);
        genericObject.setMetricName(metricValue);
        browser.pause(500);
        metricsDataObject.clickFilteredMetric();
        browser.pause(400);
        var addFavorites = browser.elements(parameter.addToFavorites);
        var addToFavName=addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText , addToFavName);
        metricsDataObject.clickOnAddToFavElem(type);
        browser.pause(200);
        metricsDataObject.clearMetricName();
        metricsDataObject.closeMetricWidget();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        metricsDataObject.clickFirstMetric();
        var Removefavorites = browser.elements(parameter.addToFavorites);
        var RemoveFavouriteBtn = Removefavorites.element(parameter.remFavButton).getText();
        assert.equal(config.removeFavText, RemoveFavouriteBtn);
        metricsDataObject.clickOnRemoveFavElem();
        browser.pause(200);
        metricsDataObject.closeMetricWidget();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        genericObject.setMetricName(metricValue);
        browser.pause(200);
        metricsDataObject.clickFirstMetric();
        var addToFavName2=addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText, addToFavName2);
        }catch(err){
            console.log("test failed");
            console.log(err.message);
        }finally{
        metricsDataObject.clickOnAddToFavElem(type);
        browser.pause(200);
        metricsDataObject.clearMetricName();
        metricsDataObject.clickOnRemoveFavElem();        
        }    
        console.log("Test completed for 'Verify whether selected metric have options and those options are functioning - XYN-1519'");

    });
    it('To verify whether toggle button under metrics remembers the selection - XYN-2364', function(){
        console.log("Test started - To verify whether toggle button under metrics remembers the selection - XYN-2364");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);                     
        //When 'Show only available metrics' button is on
        metricsDataObject.clickOnToggle();
        var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics, false);
        //close and re-open metrics tab
        metricsDataObject.closeMetricWidget();
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);
        var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics, false);
        //Untoggle at end of script
        //metricsDataObject.clickOnToggle();
        metricsDataObject.closeMetricWidget();
        console.log("Test completed - To verify whether toggle button under metrics remembers the selection - XYN-2364");
    });
    it('Verify whether Metric widget options working accordingly - XYN-1516', function(){
        console.log("Test started - XYN-1516 - Verify Metric widget options");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);       
        var dockingpinUnpinned = browser.element(parameter.dockpinUnPin);        
        if (dockingpinUnpinned.isVisible()){             
            metricsDataObject.eventWidgetDock();
        }
        browser.pause(300);
        var dockingpinPinned = browser.element(parameter.dockpinStatus);
        assert.equal(dockingpinPinned.isVisible(), true);
        metricsDataObject.closeMetricWidget();
        //Search for metric
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        var dockingpinPinned = browser.element(parameter.dockpinStatus);
        metricsDataObject.enterMetricFilterValue(config.metricName);
        //verify pinned window not closed
        assert.equal(dockingpinPinned.isVisible(), true);
        //close metric widget and verify
        metricsDataObject.closeMetricWidget();
        browser.pause(300);
        var MetricsTabClosed = browser.element(parameter.metricTabStatus);
        assert.equal(MetricsTabClosed.isVisible(), false);
        //clear metric filter and undock and close metric view
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        metricsDataObject.clearMetricName();
        browser.pause(300);
        metricsDataObject.eventWidgetUnDock();
        browser.pause(300);
        metricsDataObject.closeMetricWidget();       
        console.log("Test completed - XYN-1516 - Verify Metric widget options");
    });
    it('Verify whether toggle button is working accordingly - XYN-1517', function(){
        console.log("Test started - XYN-1517 - Verify whether toggle button is working accordingly");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);       
        metricsDataObject.clickOnExpander();
        browser.pause(300);  
        //When 'Show only available metrics' button is off (default)
        var totGreyMetrics = browser.elements(parameter.greyMetricsList).value.length;
        if ( totGreyMetrics >=1 ) {
            var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
            assert.equal(grayMetrics[1], true);
        }              
        //When 'Show only available metrics' button is on
        metricsDataObject.clickOnToggle();
        browser.pause(300);  
        var grayMetrics_1 = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics_1, false); 
        //untoggle and close metric widget
        metricsDataObject.clickOnToggle();        
        metricsDataObject.closeMetricWidget();   
        console.log("Test completed - XYN-1517 - Verify whether toggle button is working accordingly");
    });
    it('Verify whether selection based metrics pop up selections when dropped on Timeseries/Map - XYN-3292 (Part-1)', function(){
        console.log("Test started - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries");
        try{

        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.selectionBaseMetricName, parameter.lteRankByCellType, parameter.PlotAcrossWidgets);
        browser.pause(300);
        var mbox = browser.element(parameter.mBoxTitleId).isVisible();
        assert.equal(mbox, true);
        var mboxTitle = browser.element(parameter.lteRankByCellTypeMBoxTitle).getText();
        assert.equal(mboxTitle, config.selectionBaseMetricName);
        var selectcellType = browser.element(parameter.selectCell).getText();
        assert.equal(selectcellType, config.psCellSpaces);
        metricsDataObject.clickSelectionMetricsOKBtn();
        //verify from ts tab
        timeseries.clickOnTimeSeriesLayerBtn();
        var tslayerinfo = browser.elements(parameter.TimeseriesLayerData).getText();
        console.log(tslayerinfo);
        browser.pause(300);
        expect(tslayerinfo[1]).to.include(config.selectionBaseMetricName);
        expect(tslayerinfo[1]).to.include(config.metricsSelection);
        }catch(e){
            console.log(e.Message());
        }finally{
        
        //close all views
        baseMethods.closeAllViews();
        }
        console.log("Test completed - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries");
    });
    it('Verify whether selection based metrics pop up selections when dropped on Timeseries/Map - XYN-3292 (Part-2)', function(){
        console.log("Test started - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries/Map");
        try{genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.WidgetRightBox);
        browser.pause(300);
        baseMethods.plotMetric(config.selectionBaseMetricName, parameter.lteRankByCellType, parameter.PlotAcrossWidgets);
        browser.pause(300);
        var mbox = browser.element(parameter.mBoxTitleId).isVisible();
        assert.equal(mbox, true);
        var mboxTitle = browser.element(parameter.lteRankByCellTypeMBoxTitle).getText();
        assert.equal(mboxTitle, config.selectionBaseMetricName);
        var selectcellType = browser.element(parameter.selectCell).getText();
        assert.equal(selectcellType, config.psCellSpaces);
        metricsDataObject.clickSelectionMetricsOKBtn();
        //verify from ts tab
        timeseries.clickOnTimeSeriesLayerBtn();
        var tslayerinfo = browser.elements(parameter.TimeseriesLayerData).getText();
        browser.pause(300);
        console.log(tslayerinfo);
        expect(tslayerinfo[1]).to.include(config.selectionBaseMetricName);
        expect(tslayerinfo[1]).to.include(config.metricsSelection);
        //verify from map tab
        metricsDataObject.legendTab();
        var maplayerinfo = browser.elements(parameter.dataLayers).getText();
        console.log(maplayerinfo);
        expect(maplayerinfo[2]).to.include(config.selectionBaseMetricName);
        expect(maplayerinfo[2]).to.include(config.metricsSelection);
        }catch(e){
            console.log(e.Message());
            assert.fail();
        }finally{
        //close all views
        baseMethods.closeAllViews();
        }
        console.log("Test completed - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries/Map");
    });
    it('To verify whether modal pop-up shows up when event Is plotted - XYN-1991', function(){
        console.log("Test started - To verify whether modal pop-up shows up when event Is plotted - XYN-1991 ");
        try{genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetRightBox);
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.WidgetRightBox);
        browser.pause(300);
        baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.PlotAcrossWidgets);
        browser.pause(300);
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        metricsDataObject.clickSelectionMetricsOKBtn();
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var mbox = browser.element(parameter.dialogBoxHead).isVisible();
        assert.equal(mbox, false);}
        catch(err){
            console.log("test failed");
            console.log(err.message);
            assert.fail(err.message);
        }
        finally{
        baseMethods.closeAllViews();
        }
        console.log("Test completed - To verify whether modal pop-up shows up when event Is plotted - XYN-1991 ");
    });
    it('To Verify Per Cell KPIs plotting selection option  - XYN-2757', function () {
        console.log("Test started To Verify Per Cell KPIs plotting selection option   - XYN-2757'");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TableDroppable);
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        metricsDataObject.clickSelectionMetricsOKBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var av=browser.elements(parameter.tableAppData);
        var headerRow = av.elements(parameter.tableHeaderRow);
        console.log(headerRow.value.length);     
        var headerColumn=browser.elements("//span[contains(text(),'"+config.ltePciMetric+"')]").value.length;
        assert.equal(headerColumn,1);
        baseMethods.closeAllViews();
        console.log("Test completed To Verify Per Cell KPIs plotting selection option   - XYN-2757'");

    });
    it('Verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327', function(){
        console.log("Verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements(type);
        browser.pause(200);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TimeseriesDroppable);
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        var forMetricValue = browser.element(parameter.selectMetricValue);
        forMetricValue.click();
        metricsDataObject.selectMetricValue(config.firstSelectionBasedMetricValue);
        browser.pause(100);
        metricsDataObject.clickSelectionMetricsOKBtn();
        browser.pause(100);
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        genericObject.dragAndDropViews(parameter.ltePciMetric, parameter.TimeseriesDroppable);
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        var forMetricValue = browser.element(parameter.selectMetricValue);
        forMetricValue.click();
        metricsDataObject.selectMetricValue(config.secondSelectionBasedMetricValue);
        browser.pause(300);
        metricsDataObject.clickSelectionMetricsOKBtn();
        browser.pause(500);
        var graphCount=browser.elements("//time-series-chart");
        console.log(graphCount.value.length);
        assert.equal(graphCount.value.length,config.compareValueWithOne);
        var firstMetricNameOnGraph=config.ltePciMetric+" ("+config.firstSelectionBasedMetricValue+")";
        console.log(firstMetricNameOnGraph);
        var secondMetricNameOnGraph=config.ltePciMetric+" ("+config.secondSelectionBasedMetricValue+")";
        console.log(secondMetricNameOnGraph);
        expect(graphCount.getText()).to.be.includes(firstMetricNameOnGraph);
        expect(graphCount.getText()).to.be.includes(secondMetricNameOnGraph);
        console.log("Test completed to verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327");
    });

    it('Verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328', function(){
        console.log("Verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements(type);
        browser.pause(200);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TableDroppable);
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        var forMetricValue = browser.element(parameter.selectMetricValue);
        forMetricValue.click();
        metricsDataObject.selectMetricValue(config.firstSelectionBasedMetricValue);
        browser.pause(100);
        metricsDataObject.clickSelectionMetricsOKBtn();
        browser.pause(100);
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        genericObject.dragAndDropViews(parameter.ltePciMetric, parameter.TableDroppable);
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        var forMetricValue = browser.element(parameter.selectMetricValue);
        forMetricValue.click();
        metricsDataObject.selectMetricValue(config.selectionBasedMetricForTableView);
        browser.pause(300);
        metricsDataObject.clickSelectionMetricsOKBtn();
        browser.pause(800);
        //browser.waitForExist(parameter.noDataAvailableOkBtn);
        metricsDataObject.clickOkBtn();
        browser.pause(300);    
        var headerColumn=browser.elements("//span[contains(text(),'"+config.ltePciMetric+"')]").value.length;
        assert.equal(headerColumn,1);
        console.log("Test completed to verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328");
    });    

});

describe('Metrics test2', function () {
    var init=2;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        }
        if(isElectron()){
            if(init==2){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
            init=1;
            }
        }
       // genericObject.clickOnDatasetReplaceButton();
       // genericObject.clickOnReplaceOkWithoutSaving();
    });
    afterEach(function () {
        // if(isElectron()){
        // console.log("closing all open views");
        // genericObject.clickOnDTTab(parameter.ViewsMenu);
        // browser.pause(500);
        // console.log("closing all open views");
        //     var closeAllViews=browser.element("[data-test-id='close-all-btn']");
        //     if(closeAllViews.isVisible()){
        //     closeAllViews.click();
        //     browser.pause(300);
        //     console.log("clicking on close btn");
        //     //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
        //     var modalBox=browser.element("//modal-box");
        //     var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
        //     if(mBoxCloseBtn.isVisible()){
        //     mBoxCloseBtn.click();
        //     console.log("clicked on close btn");
        //     }
        //     browser.pause(300);
        //     //close views menu
        //     var viewsHeader=browser.element("//views-menu");
        //     var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
        //     if(closeViewsMenu.isVisible()){
        //         closeViewsMenu.click();
        //     }
        //     }
        // }       
    });
    it('metrics2', function () {
        console.log("second one");
        var metricValue=config.metricName;
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements("metric");
        genericObject.setMetricName(metricValue);
        browser.pause(500);
        metricsDataObject.clickFilteredMetric();
        browser.pause(3000);
        
    });
});