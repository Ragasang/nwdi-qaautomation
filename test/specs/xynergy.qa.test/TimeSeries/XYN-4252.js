let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let xynergyconstants=require('../../test_xynergy.constants.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;
let config=require('../../../config.js');
require('../../commons/global.js')();

describe('Time Series',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });
    it('TimeSeries | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4252', function(){
        console.log("Test started - TimeSeries | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4252");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        //drop UXM MAC metric 
        baseMethods.plotMetric(config.metricName3, parameter.metric_UXMMACDLSCHDataRate, parameter.PlotAcrossWidgets);
        baseMethods.verifySpinnerVisibility();
        timeseries.clickOnTimeSeriesLayerBtn();       
        var timeLayerValue = baseMethods.verifyTimeSeriesMetricLayerData(config.metricName3);
        assert.equal(timeLayerValue, true);
        var transportChannelId = baseMethods.verifyTimeSeriesMetricLayerData(config.tableViewHeadData1);
        assert.equal(transportChannelId, true);
        baseMethods.closeTimeSeriesDataLayer();
        //drop another UXM MAC metric and verify
        baseMethods.plotMetric(config.metricName4, parameter.metric_UXMMACULSCHDataRate, parameter.PlotAcrossWidgets);
        baseMethods.verifySpinnerVisibility();
        timeseries.clickOnTimeSeriesLayerBtn();       
        var timeLayerValue = baseMethods.verifyTimeSeriesMetricLayerData(config.metricName4);
        assert.equal(timeLayerValue, true);
        var transportChannelId = baseMethods.verifyTimeSeriesMetricLayerData(config.tableViewHeadData1);
        assert.equal(transportChannelId, true);
        baseMethods.closeTimeSeriesDataLayer();
        console.log("Test competed - TimeSeries | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4252");
    });    
});