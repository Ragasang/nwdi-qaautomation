let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let xynergyconstants=require('../../test_xynergy.constants.js');
let timeseriesObject=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;
let config=require('../../../config.js');
require('../../commons/global.js')();

describe('Time Series',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    it('Verify whether event and metric can be plotted on Timeseries - XYN-2862', function(){
        console.log("Test started - Verify whether event and metric can be plotted on Timeseries - XYN-2862");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotEvent(config.eventName,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName,parameter.RSRPMetric,parameter.TimeseriesDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName2,parameter.VoiceSetupEvent,parameter.TimeseriesDroppable);
        browser.pause(300);
        timeseriesObject.clickOnTimeSeriesLayerBtn();       
        browser.pause(200);
        assert.equal(timeseriesObject.verifyPlottedEventData(config.eventName),true);
        assert.equal(timeseriesObject.verifyPlottedEventData(config.eventName2),true);
        assert.equal(timeseriesObject.verifyPlottedMetricData(config.metricName),true);
        console.log("Test competed - Verify whether event and metric can be plotted on Timeseries- XYN-2862");
    });    
});