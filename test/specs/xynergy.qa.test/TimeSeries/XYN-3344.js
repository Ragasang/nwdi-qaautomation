let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let xynergyconstants=require('../../test_xynergy.constants.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;
let config=require('../../../config.js');
require('../../commons/global.js')();

describe('Time Series',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    it('Verify Counts to Events in both Layer Control of Map and Time Series - XYN-3344', function(){
        console.log("Test started - Verify Counts to Events in both Layer Control of Map and Time Series - XYN-3344");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.WidgetRightBox);
        baseMethods.verifySpinnerVisibility();
        //drop event 
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        baseMethods.verifySpinnerVisibility();
        //1. verify event count in bracket for TimeSeries
        timeseries.clickOnTimeSeriesLayerBtn(); 
        var eventNameWithCount = config.eventName + "" + config.eventCount;
        console.log(eventNameWithCount);
        assert.equal(baseMethods.verifyTimeSeriesEventLayerData(eventNameWithCount), true);
        //get text in events layer to verify count in '(' and ')'
        baseMethods.closeTimeSeriesDataLayer();
        //2. verify event count in bracket for Map
        baseMethods.clickOnMapLayerBtn();
        browser.element(parameter.analyticsview_2).click();
        //Expand child node if it is in collapsed
        if (browser.element(parameter.mapExpandChildWrap).isVisible()){
            browser.element(parameter.mapExpandChildWrap).click();
        }        
        assert.equal(baseMethods.verifyMapEventLayerData(eventNameWithCount), true);
        //get text in events layer to verify count in '(' and ')'
        baseMethods.closeMapDataLayer();
        console.log("Test competed - Verify Counts to Events in both Layer Control of Map and Time Series - XYN-3344");
    });    
});