let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('To verify whether full-screen icon is working - XYN-1621', function () {
        console.log("Test started to verify full-screen icon");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        // var divElements = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        // console.log("number of elements: ", divElements.value.length);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetRightBox);
        var divElements = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        console.log("number of elements: ", divElements.value.length);
        var divElements1 = browser.elements("//widget-container//splitter-component[1]//div[3][@class='vertical-splitter-container']").getAttribute("style");
        //expect(divElements1.getAttribute('style')).to.include('block');
        console.log(divElements1);
        divElements1.forEach(element=>{
            console.log(element);
            if(element.includes('block')){
                console.log("there are multiple views");
            }
        })
        var countBefore=browser.element('div[data-test-id="widget-max-btn"]').value.length;
        console.log(countBefore);
        messageObject.clickFullScreenIcon();
        console.log("screen");
        browser.pause(1000);
        var countAfter=browser.element('div[data-test-id="widget-max-btn"]').value.length;
        console.log(countAfter);
        var divElements2 = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        console.log("number of div elements: ", divElements2.value.length);
        var hidden = browser.elements("//widget-container//splitter-component[1]//div[3][contains(@style,'none')]").getAttribute('style');
        console.log("style", divElements2.getAttribute('style'));
        console.log(hidden);
        hidden.forEach(element=>{
            console.log(element);
            expect(element).to.include('none');
        })
        //expect(hidden.getAttribute('style')).to.include('none');
    });
});
