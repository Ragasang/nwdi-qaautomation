let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify whether user can navigate through message data sequentially using Find Next after finding all filtered results - XYN-1115', function () {
        console.log("Test started for find next button working after finding all filtered results- XYN-1115");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(300);
        messageObject.clickFindall(parameter.FindAllButton);
        var j = 1;
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        console.log(findAllRow.value.length);
        messageObject.clickFindnext(parameter.FindNextButton);
        browser.pause(300);
        console.log("timeing");
        var actualTime = browser.elements(selectedTimeColValueInFindGrid).getText();
        console.log(actualTime);
        while (j <= 2) {

            messageObject.clickFindnext(parameter.FindNextButton);
            browser.pause(1000);
            console.log("next row contents");
            var findNextRow = browser.elements(parameter.MessagesFindGridSelectedRowContent);
            console.log(findNextRow.getText());
            var timeDiff = browser.elements(selectedTimeColValueInFindGrid).getText();
            console.log("printing each rows in findall");
            if (j > 1) {
                expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
                assert.notEqual(timeDiff, actualTime);
            }
            console.log("actual time before assigning", actualTime);
            actualTime = timeDiff;
            console.log("actual time after assigning", actualTime);
            j++;
        }
        console.log("Test completed for find next button working after finding all filtered results-XYN-1115");
    });
});
