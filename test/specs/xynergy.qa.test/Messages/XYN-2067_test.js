let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify ALF Index column in Messages View - XYN-2067', function () {
        console.log("Test started for verification of ALF Index column in Messages View - XYN-2067");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(800);
        var col=browser.elements(parameter.MessageColumnSelector);
        console.log(col.value.length);
        //col.click();
        var ColumnName = browser.element("[data-test-id='ALF Index']").getText();
        console.log(ColumnName);
        assert.equal("ALF Index ", ColumnName);
        console.log("Test completed for verification of ALF Index column in Messages View");
    });
});
