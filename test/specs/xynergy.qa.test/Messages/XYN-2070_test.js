let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify UE Log testing - Message View- XYN-2070', function () {
        console.log("Test started for verification UE Log testing in Messages View");
        this.skip();
        baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
        browser.pause(1000);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue4);
        browser.pause(1000);
        var j = 1;
        rowCount = browser.elements("//datatable-row-wrapper").value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements("//datatable-row-wrapper[" + j + "]");
            console.log(rowContent.getText());
            expect(rowContent.getText().toLowerCase()).to.include(config.findMessageFilterValue5);
            j++;
            rowCount--;
        }
        console.log("Test completed for verification UE Log testing in Messages View");

    });
});
