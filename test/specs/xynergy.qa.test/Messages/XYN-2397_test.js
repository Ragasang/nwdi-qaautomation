let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Sync between messages and sequence diagram view - XYN-2397', function () {
        console.log("Test started - Sync between messages and sequence diagram view - XYN-2397");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.WidgetRightBox);
        browser.pause(300);
        var dataTable = browser.elements(parameter.messagesTableRows).value.length;
        console.log(dataTable);
        var j=1;
        while (j <= dataTable ){
            //click few rows to get verify sync
            browser.element(parameter.analyticsview_1).click();            
            messageObject.clickMessagesDataRow(j);
            /*on clicking on Messages table row, Sequence diagram should display either of the following
            1. message 'Unavailable within 2 sec of the selected bin-raw' OR 2. class //div[@class='list-line-center selected']*/
            browser.element(parameter.analyticsview_2).click();
            var errMsg = browser.element(parameter.incorrectQuery).isVisible();
            if (errMsg){
                console.log(browser.element(parameter.incorrectQuery).getText());
                expect(errMsg, config.errMsg);            
            }
            else{
                var syncAvailable = browser.element(parameter.seqSyncRow).isVisible();
                assert.equal(syncAvailable, true);
            }
            j = j+5;
        }
        console.log("Test completed - Sync between messages and sequence diagram view - XYN-2397");
    });
});
