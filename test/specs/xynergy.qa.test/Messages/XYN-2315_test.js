let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify messages filter text with column selection - XYN-2315', function () {
        console.log("Test started for verification of messages filter text with column selection");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var selectRowElement = browser.element(parameter.SelectRowBtn);
        selectRowElement.click();
        browser.pause(1000);
        var option=browser.element(parameter.Message);
        option.click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        var rowData = browser.elements(parameter.MessagesRowContent);
        var objectCount = rowData.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        genericObject.clickOnDataMenu();
        //browser.element("[data-test-id='search-child-item-filtered']").click();
        browser.pause(200);
        searchdataObject.clearsearchkeyword();
        baseMethods.clickOnSearchViewIcon(config.datasetWithTwoDevice);
        genericObject.clickOnDatasetReplaceButton();
        genericObject.clickOnReplaceOkWithoutSaving();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var rowDataAfterDatasetReplace = browser.elements(parameter.MessagesRowContent).value.length;
        expect(rowDataAfterDatasetReplace).to.be.gt(2);

        console.log("Test completed for verification of messages filter text with column selection");

    });
});
