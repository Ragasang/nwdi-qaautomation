let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify search text retain  in find search bar - XYN-1536', function () {
        console.log("Test started for verification of search text retain in find search bar");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        browser.element(parameter.HamburgerMenu).click();
        browser.element(parameter.FindMsgBtn).click();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        var SearchTerm = browser.element("[data-test-id='input-find-msg']").getText();
        messageObject.clickFindall(parameter.FindAllButton);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var SearchTerm2 = browser.element("[data-test-id='msg-filter']").getText();
        assert.equal(SearchTerm, SearchTerm2);
        var eventsdataTab = browser.elements("//*[@name='messages-search-grid']//datatable-row-wrapper[2]");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount == 0) {
            count = true;
        }
        assert.equal(count, true);
        console.log("Test completed for verification of search text retain in find search bar");
    });
});
