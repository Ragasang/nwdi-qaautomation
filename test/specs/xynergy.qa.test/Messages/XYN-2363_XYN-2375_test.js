let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    var init=0;
    var selectedTimeColValue=parameter.MessagesSelectedRow+"//div[2]//datatable-body-cell[2]";
    var selectedTimeColValueInFindGrid=parameter.MessagesFindGridSelectedRowContent+"//div[2]//datatable-body-cell[2]";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }       
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });
    it('Verify whether Find section will expand and show all the result - XYN-2363, XYN-2375', function () {
        console.log("Test started for find section to display results - XYN-2363, XYN-2375");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
        browser.pause(400);
        messageObject.waitForEnabled();
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(400);
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        var count;
        count = findAllRow.value.length;
        console.log("count of filtered rows: ", count);
        expect(count).to.be.gt(2);
        console.log("Test completed for find section to display result- XYN-2363, XYN-2375");
    });
});
