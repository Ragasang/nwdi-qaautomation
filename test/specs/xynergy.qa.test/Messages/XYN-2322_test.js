let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('XYN-2322_Verify if Messages widget is showing messages', function () {
        console.log("XYN-2322 Test execution started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
        messageObject.enterValueInFindFilter("paging");
        messageObject.clickFindall();
        browser.pause(500);
        var f = baseMethods.verifyTextInDecodedMessagesGrid("paging");
        console.log(f);
        assert.equal(f, true, "verifyTextInDecodedMessagesGrid function not return as expcted");
        var actualMsg = baseMethods.VerifyActiveRowCellValue("systemInformation");
        console.log(actualMsg)
        assert.equal(actualMsg, true, actualMsg + ":expected message is not same as acutal message:" + true);
        console.log("XYN-2322 Test execution ended");
    });
});
