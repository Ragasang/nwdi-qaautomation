let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('To verify whether FindNext button is working depending on filtered messages - XYN-1531', function () {
        console.log("Test started to verify findNext functionality based on filtered messages");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findMessageFilterValue3);
        browser.pause(3000);
        //baseMethods.verifySpinnerVisibility();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        var rowClass = browser.elements(parameter.MessagesSelectedRow);
        console.log(rowClass.getText());
        // //datatable-body-row[@class='datatable-body-row active datatable-row-odd']//div[@class='datatable-row-center datatable-row-group']//datatable-body-cell[@class='datatable-body-cell sort-active']//div[@class='datatable-body-cell-label']//div//span[contains(text(),'09/25/2013 21:32:00.451')]
        var actualTime = browser.elements(selectedTimeColValue);
        var i = 0;
        while (i < 2) {
            messageObject.clickFindnext(parameter.FindNextButton);
            var rowNext = browser.elements(parameter.MessagesSelectedRow);
            var timeDiff = browser.elements(selectedTimeColValue);
            console.log(timeDiff.getText());
            expect(rowNext.getText().toLowerCase()).to.include(config.findMessageFilterValue);
            assert.notEqual(timeDiff, actualTime);
            console.log(rowNext.getText());
            i++;
        }
    });
});
