let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify error message if on filtering there is no data present on Messages - XYN-3032', function () {
        console.log("Test started - Verify error message if on filtering there is no data present on Messages - XYN-3032");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterMessageFilterValue(config.filterVal9);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var noDatamsg = browser.element(parameter.noData).getText();
        expect(noDatamsg, config.noDataAvailable);
        messageObject.enterMessageFilterValue(config.filterVal9);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var noDatamsg = browser.element(parameter.noData).getText();
        expect(noDatamsg, config.noDataAvailable);
        console.log("Test completed - Verify error message if on filtering there is no data present on Messages - XYN-3032");
    });
});
