let baseMethods = require('../../../testbase.js');
let genericObject = require('../../../generic_objects.js');
let parameter=require('../../../parameters.js');
let xynergyconstants=require('../../../test_xynergy.constants.js');
let timeseries=require("../../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;

describe('Time Series',function(){
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    it('Verify if graph is displayed for the selected metrics on TimeSeries', function(){
        console.log("Test started - Verify if graph is displayed for the selected metrics on TimeSeries");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.TimeseriesDroppable);
        var graphElement=browser.element("//*[@class='highcharts-tracker-line']");
        expect(graphElement.getAttribute('visibility')).to.include('visible');
        console.log("Test competed - Verify if graph is displayed for the selected metrics on TimeSeries");
    });    

    it('verify if events are plotting on time series', function () {
        console.log("Test started- Events plot on time series");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent, parameter.VoiceStartEvent, parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();
        var eventName = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
        assert.equal(eventName, true);
        console.log("Test completed - Events plot on time series");
    });

    it('verify if multiple events plot on time series', function () {
        console.log("Test started- Multiple Events plot on time series");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
        baseMethods.plotEvent(xynergyconstants.VoiceSetupEvent,parameter.VoiceSetupEvent ,parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();
        var event1 = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
        var event2 = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceSetupEvent);
        assert.equal(event1 && event2, true);
        console.log("Test completed - Multiple Events plot on time series");
    });

    it('Verify if metric with no data is plotting graph or not on Time series', function () {
        console.log("Test started- Graph should not be displayed for no data metric");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric("DL App Throughput", "[data-test-id='metricNameDL App Throughput']", "[data-test-id='time-series-droppable']");
        browser.pause(100);
        var noDataContainer = browser.isVisible("[data-test-id='ts-no-data-message']");
        var ClickOk = browser.element("[data-test-id='ts-no-data-button']");
        ClickOk.click();
        var timeseriesStack=browser.isExisting("//*[@class='height-width-full']");
        assert.equal(noDataContainer,true);
        assert.equal(timeseriesStack,false);
    });

    // it('Verify if graphs are displayed in stacks with scroll bar for fourth chart', function () {
    //     console.log("Test started- Verify if graphs are displayed in stacks with scroll bar for fourth chart");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
    //     baseMethods.plotMetric("Best Cell RSRP", "[data-test-id='metricNameBest Cell RSRP']", "[data-test-id='time-series-droppable']");
    //     baseMethods.plotMetric("Best Cell RSRQ", "[data-test-id='metricNameBest Cell RSRQ']", "[data-test-id='time-series-droppable']");
    //     baseMethods.plotMetric("Best Cell PCI", "[data-test-id='metricNameBest Cell PCI']", "[data-test-id='time-series-droppable']");
    //     baseMethods.plotMetric("DL Bandwidth", "[data-test-id='metricNameDL Bandwidth']", "[data-test-id='time-series-droppable']");
    //     console.log("Test completed - Verify if graphs are displayed in stacks with scroll bar for fourth chart");

    // });

    it('should verify tooltip content of timeseries', function(){
        console.log("should verify tooltip content of timeseries -started");
        var deviceName="Device: Phone_Qualcomm Phone_Unknown_352805091670097_310120240490390";
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048"; 
        baseMethods.clickOnAdvancedSearchViewIcon();
        baseMethods.skipSomeDataFiles();  
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
        baseMethods.clickOnTimeseriesEventElement(timestamp);
        browser.pause(100);
        var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
        var timeseriestooltipContent = timeseriesElement.getText();
        const text = new RegExp(deviceName + '*');
        if (text.test(timeseriestooltipContent)) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
        console.log("should verify tooltip content of timeseries -ended");
    });

    it('verify if metrics are plotted on time series', function () {
        console.log("Test started- verify if metrics are plotted on time series");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric("PDSCH BLER", "[data-test-id='metricNamePDSCH BLER']", "[data-test-id='time-series-droppable']");
        browser.element("[data-test-id='TimeSeriesLayerButton']").click();
        browser.pause(200);
        var metric = baseMethods.verifyTimeSeriesMetricLayerData("PDSCH BLER");
        assert.equal(metric,true);
        console.log("Test completed - verify if metrics are plotted on time series");

    });

    it('verify synchronization of data on timeseries and tableview', function () {
        console.log("verify synchronization of data on timeseries and tableview");
        browser.windowHandleFullscreen();
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048";
        baseMethods.clickOnAdvancedSearchViewIcon();
        baseMethods.skipSomeDataFiles();        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
        baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
        baseMethods.clickOnTimeseriesEventElement(timestamp);
        var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
        browser.pause(500);
        var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
        var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
        if (tablecellcontent && timeseriestooltipContent == timestamp) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
        console.log("verify synchronization of data on timeseries and tableview");
    });
    
    it('To verify whether full-screen icon is working', function () {
        console.log("Test started to verify full-screen icon");
        baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.MessageDroppable);
        var divElements = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-container'][1]");
        console.log("number of elements: ", divElements.value.length);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetRightBox);
        var divElements1 = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-container'][2]");
        expect(divElements1.getAttribute('style')).to.include('block');
        timeseries.clickFullScreenIcon();
        browser.pause(1000);
        var divElements2 = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-container'][2]");
        console.log("number of div elements: ", divElements2.value.length);
        var hidden = browser.elements("//widget-container//splitter-component[1]//div[contains(@style,'none')]");
        console.log("style", divElements2.getAttribute('style'));
        expect(divElements2.getAttribute('style')).to.include('none');
    });

    it('verify if graphs are displaying in stack', function () {
        console.log("Test started- verify if graphs are displaying in stack");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric("Best Cell RSRP", "[data-test-id='metricNameBest Cell RSRP']", "[data-test-id='time-series-droppable']");
        baseMethods.plotMetric("Best Cell RSRQ", "[data-test-id='metricNameBest Cell RSRQ']", "[data-test-id='time-series-droppable']");
        baseMethods.plotMetric("Best Cell PCI", "[data-test-id='metricNameBest Cell PCI']", "[data-test-id='time-series-droppable']");
        var chartTite=browser.elements("//*[@class='highcharts-title']");
        var titleElmnt = chartTite.value;
        var value = 0;
        titleElmnt.forEach(element => {
            var textValue=element.getText();
            if (textValue == "Best Cell RSRP" || "Best Cell RSRQ" || "Best Cell PCI") {
                value++;
            }
        })
        var graphSeperator=browser.elements("//div[@class='timeseries-chart-seperator']");
        var graphElement=graphSeperator.value;
        assert.equal(graphElement.length,2);
        assert.equal(value,3);
        console.log("Test completed - verify if graphs are displaying in stack");

    });

    it('Verify whether graph is showing metric info', function () {
        console.log("Test started- Verify whether graph is showing metric info");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric("Best Cell RSRP", "[data-test-id='metricNameBest Cell RSRP']", "[data-test-id='time-series-droppable']");
        var chartTite=browser.element("//*[@class='highcharts-title']").getText();
        assert.equal(chartTite,"Best Cell RSRP");
        console.log("Test completed - Verify whether graph is showing metric info");
    });

    //To verify whether tool tip is present on the right side of the window
    it('verify if tool tip is present on the right side of the window', function () {
        console.log("Test started- verify if tool tip is present on the right side of the window");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent, parameter.VoiceStartEvent, parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();
        var eventName = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
        assert.equal(eventName, true);
        console.log("Test completed - verify if tool tip is present on the right side of the window");
    });

    it('Verify if data line is shown on time series graph', function () {
        console.log("Test started- Verify if data line is shown on time series graph");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric("Best Cell RSRP", "[data-test-id='metricNameBest Cell RSRP']", "[data-test-id='time-series-droppable']");
        var dataLine=browser.element("//*[@class='highcharts-plot-lines-0']").isVisible();
        assert.equal(dataLine,true);
        console.log("Test completed - Verify if data line is shown on time series graph");
    });

    // it('Verify hide graph feature of time series', function () {
    //     console.log("Test started- Verify hide graph feature of time series");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
    //     baseMethods.plotMetric("Best Cell RSRP", "[data-test-id='metricNameBest Cell RSRP']", "[data-test-id='time-series-droppable']");
    //     var graphElement=browser.element("//*[@class='highcharts-tracker-line']");
    //     expect(graphElement.getAttribute('visibility')).to.include('visible');
    //     browser.element("[data-test-id='TimeSeriesLayerButton']").click();
    //     browser.element("[data-test-id='hide-btn']").click();
    //     var graphElement=browser.element("//*[@class='highcharts-tracker-line']");
    //     expect(graphElement.getAttribute('visibility')).to.include('hidden');
    //     var timeSeriesLayerBtn=browser.element("[data-test-id='close-button']");
    //     timeSeriesLayerBtn.click();
    //     baseMethods.plotMetric("Best Cell PSS Quality", "[data-test-id='metricNameBest Cell PSS Quality']", "[data-test-id='time-series-droppable']");
    //     var noDataContainer = browser.isVisible("[data-test-id='ts-no-data-message']");
    //     var ClickOk = browser.element("[data-test-id='ts-no-data-button']");
    //     if(ClickOk.isVisible()){
    //         ClickOk.click();
    //     }
    //     var graphCount=browser.elements("//*[@class='height-width-full']").value;
    //     var graphElement=browser.element("//*[@class='highcharts-tracker-line']");
    //     expect(graphElement.getAttribute('visibility')).to.include('hidden');
    //     assert.equal(graphCount.length,1);
    //     console.log("Test completed - Verify hide graph feature of time series");
    // });


});