let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('Verify Data panel styling - XYN-2312', function(){
        console.log("Test Started - Verify Data panel styling - XYN-2312");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }        
        browser.pause(500);
        browser.element("[data-test-id='Datasets']").click();
        var validationToolTip = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility = validationToolTip.isVisible();
        assert(visibility, true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.scopeWidget).click();        
        var validationToolTip1 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility1 = validationToolTip1.isVisible();
        assert(visibility1, true);
        browser.element("[data-test-id='Files']").click();
        browser.element("[data-test-id='Files']").click();    
        var validationToolTip2 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);        
        var checkToolTip = true;
        if(validationToolTip2.isVisible()){
            checkToolTip = false;
        }
        assert(checkToolTip, true);
        browser.element(parameter.incorrectQueryClose).click();       
        console.log("Test Completed - Verify Data panel styling - XYN-2312");        
    
    });

});