let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('Verify Landing page & Analytics View redesign - XYN-2046', function(){
        console.log("Test Started - Verify Landing page & Analytics View redesign - XYN-2046");
        browser.element(parameter.DataMenu).click();
        var importData = browser.element("[data-test-id='Import Data']");        
        var checkImportData = true;
        if(importData.isVisible()){
            checkImportData = true;
        }
        assert(checkImportData, false);
        var searchData = browser.element("[data-test-id='Search Data']");        
        var checkSearchData = true;
        if(searchData.isVisible()){
            checkSearchData = true;
        }
        assert(checkSearchData, false);
        var browseData = browser.element("[data-test-id='Browse Data']");        
        var checkBrowseData = true;
        if(browseData.isVisible()){
            checkBrowseData = true;
        }
        assert(checkBrowseData, false);
        browser.element(parameter.AVMenu).click();
        browser.pause(5000);
        var scope = browser.element(parameter.scope);        
        var checkScope = true;
        if(scope.isVisible()){
            checkScope = true;
        }
        assert(checkScope, false);
        var view = browser.element(parameter.ViewsMenu);       
        var checkView = true;
        if(view.isVisible()){
            checkView = true;
        }
        assert(checkView, false);
        var metric = browser.element(parameter.MetricsMenu);        
        var checkMetric = true;
        if(metric.isVisible()){
            checkMetric = true;
        }
        assert(checkMetric, false);
        var event = browser.element(parameter.EventsMenu);        
        var checkEvent = true;
        if(event.isVisible()){
            checkEvent = true;
        }
        assert(checkEvent, false);
        var workspace = browser.element(parameter.workspacesMenu);        
        var checkWorkspace = true;
        if(workspace.isVisible()){
            checkWorkspace = true;
        }
        assert(checkWorkspace, false);
        //browser.element(parameter.incorrectQueryClose).click();    
        browser.pause(200);   
        var menuBarExpander = browser.element("//analytics-view//div[@data-test-id='vertical-menu-item']//div[@class='bottom-expander']");
        menuBarExpander.click();
        var menuBarExpander = browser.element("//analytics-view//div[@data-test-id='vertical-menu-item']//div[@class='bottom-expander']");
        menuBarExpander.click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
        var scopeTab = browser.element(parameter.scopeWidget);
        if((scopeTab.getAttribute("class")).includes('selected')){
        scopeTab.click();
    } 
        browser.element("[data-test-id='Mapclose-btn']").click();                      
        console.log("Test Completed - Verify Landing page & Analytics View redesign - XYN-2046");

    });

});