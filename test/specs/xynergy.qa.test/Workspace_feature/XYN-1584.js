let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('Workspace - To verify date and time in Sequence Diagram view are restored: XYN-1584' ,function(){
    console.log("Test Started - Workspace - To verify date and time in Sequence Diagram view are restored: XYN-1584");              
    baseMethods.SelectTabs("SEARCHDATA");
    /* Import SprintMO Dataset to AV */
    baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
    searchdataObject.selectdatasetresult();
    browser.element(parameter.searchDataOpenBtn).click();
    baseMethods.openInCanvasValidation();
    baseMethods.verifySpinnerVisibility();
    browser.waitForExist("[data-test-id='Datasets']");
    /* Drop sequence diagram view on AV and verify if date and time is shown by default*/
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
    baseMethods.verifySpinnerVisibility();
    browser.waitForExist(parameter.LegendTitle);
    var seqTimestamp = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
    console.log(seqTimestamp + ' is timestamp for Seq Diag');
    var enabled = false;
    var timeBtn = browser.element(parameter.timeStampsBtn);
    if((timeBtn.getAttribute("class")).includes('active')) {
        enabled = true;
    }
    assert.equal(enabled,true);
   
    /* Save the view state as workspace and close all views */
    genericObject.clickOnDTTab(parameter.workspacesMenu);
    browser.element(parameter.workspaceSaveAsBtn).click();
    var workspacename = baseMethods.saveFileName("a");
    console.log(workspacename);
    searchdataObject.enterSaveWorkspaceNameValue(workspacename);
    browser.element(parameter.workspaceSaveBtn).click();
    browser.waitForExist("[data-test-id='"+workspacename+"']");
    browser.element(parameter.incorrectQueryClose).click();        
    baseMethods.closeAllViews();        
    browser.pause(500);        
    /* Open saved workspace and verify if it open in same state as it saved */
    workspacesObject.openSavedWorkspace(workspacename);
    browser.waitForExist(parameter.LegendTitle);
    var enabled1 = false;
    var timeBtn = browser.element(parameter.timeStampsBtn);
    if((timeBtn.getAttribute("class")).includes('active')) {
        enabled1 = true;
    }
    assert.equal(enabled1,true);
    var seqTimestamp1 = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
    console.log(seqTimestamp1 + ' is timestamp for Seq Diag');
    assert.equal(seqTimestamp,seqTimestamp1);
    /* Hide the date and time and verify if it is hidden*/
    timeBtn.click();
    var seqTimestampBtn = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").isVisible();
    assert.equal(seqTimestampBtn,false);
    browser.pause(1500);
    /* Save the view as workspace with same name and close all views*/
    genericObject.clickOnDTTab(parameter.workspacesMenu);
    browser.element(parameter.workspaceSaveAsBtn).click();
    searchdataObject.enterSaveWorkspaceNameValue(workspacename);
    browser.element("[data-test-id='"+workspacename+'dropdown'+"']").click();
    browser.element(parameter.workspaceSaveBtn).click();
    browser.pause(1500);
    browser.waitForExist("[data-test-id='"+workspacename+"']");
    browser.element(parameter.incorrectQueryClose).click();        
    baseMethods.closeAllViews();        
    browser.pause(500);  
    /* Open saved workspace and verify if it saved with latest changes made on the view*/
    workspacesObject.openSavedWorkspace(workspacename);
    browser.waitForExist(parameter.LegendTitle);
    var enabled1 = false;
    var timeBtn = browser.element(parameter.timeStampsBtn);
    if((timeBtn.getAttribute("class")).includes('active')) {
        enabled1 = true;
    }
    assert.notEqual(enabled1,true);
    var seqTimestampBtnverify = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").isVisible();
    assert.equal(seqTimestampBtnverify,false);
    /* Close all views and delete the workspace */
    baseMethods.closeAllViews();        
    browser.pause(500);
    var wspacedel = workspacesObject.deleteWorkspace(workspacename);
    assert.equal(wspacedel,false);
    console.log('Workspace ' + workspacename + ' is deleted successfully');
    browser.element(parameter.incorrectQueryClose).click();
    browser.element(parameter.DataMenu).click();
    searchdataObject.newSearchPage();
    console.log("Test Completed - Workspace - Verified date and time in Sequence Diagram view are restored: XYN-1584");
    });
});