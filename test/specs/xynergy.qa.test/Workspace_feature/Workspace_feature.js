// let baseMethods = require('../../testbase.js');
// let parameter = require('../../parameters.js');
// let genericObject=require('../../generic_objects.js');
// let xynergyconstants=require('../../test_xynergy.constants.js');
// var dragAndDrop = require('html-dnd').codeForSelectors;
// var assert = require('assert');

// describe('Workspace', function () {
//     beforeEach(function () {
//         browser.url('/account/login');
//         browser.setValue('input[name="username"]', 'demotest', 100);
//         browser.setValue('input[name="password"]', 'Xceed123', 100);
//         var submitButton = browser.element("//button[@type='submit']");
//         submitButton.click();
//         //baseMethods.verifySpinnerVisibility();
//         browser.windowHandleMaximize();
//         browser.waitForExist("[data-test-id='Analytics View']");
//     });

//     it('Verify if save Workspace would save the views on canvas', function(){
//         this.skip();
//         console.log("Test started - for workspace if it can save and reload the views only XYN-1160");
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.MessageDroppable);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.clickOnLayerMenuBtn();
//         var metricName = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
//         assert.equal(metricName, true);
//         var workspacename = "QAWorkspace0";
//         baseMethods.saveWorkspace(workspacename);
//         var verifyWorkspace = browser.element("[data-test-id='QAWorkspace0']").isExisting();
//         assert.equal(verifyWorkspace, true);
//         console.log("Successfully workspace saved");
//         var closeMap = browser.element("[class='tab-close-icon']");
//         closeMap.click();
//         console.log("Map view is successfully closed");
        
//         var workSpace = browser.element("[data-test-id='QAWorkspace0']");
//         workSpace.click();
//         var openBtn = browser.element("[data-test-id='open-ws-btn']");
//         openBtn.click(); 
//         console.log("Successfully clicked on Restore workspace");
//         browser.waitForVisible("[data-test-id='right-tab-menu-header']", 20000);
//         var viewName = baseMethods.verifyRightMenuHeader("Map");
//         assert.equal(viewName, true);
//         console.log("Restore workspace worked successfully");
//     });
//     it('Verify restore workspace reloads data table once column is hidden and saved', function(){
//         this.skip();
//         console.log("Test started - to verify restore workspace reloads data table once column is hidden and saved XYN-1214");
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.TableDraggable,parameter.MessageDroppable);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.tableViewColumnHamBurger();
//         var columns = browser.element("[class='xyn-table-contextmenu']");
//         var hideColumn = columns.element("[data-test-id='Device NameColumn-name']");
//         hideColumn.click();
//         browser.moveToObject("[data-test-id='1']", 400, 150);
//         //browser.buttonDown();
//         //browser.buttonUp();
//         //browser.element("[data-test-id='1SelectRow-Btn']").click();
//         //baseMethods.tableViewHideColumn("Device Name");
//         console.log("Selected column is hidden");
//         browser.pause(1000);
//         var workspacename = "QAWorkspace1";
//         baseMethods.saveWorkspace(workspacename);
//         var verifyWorkspace = browser.element("[data-test-id='QAWorkspace1']").isExisting();
//         assert.equal(verifyWorkspace, true);
//         console.log("Successfully workspace saved");
//         browser.element("[data-test-id='close-btn-icon']").click();
//         var view = browser.element("[data-test-id='Metric View']");
//         var closeView = view.element("[class='tab-close-icon']");
//         closeView.click();
//         console.log("Table view is successfully closed");
//         genericObject.clickOnDTTab(parameter.WorkspacesMenu);
//         var workSpace = browser.element("[data-test-id='QAWorkspace1']");
//         workSpace.click();
//         var openBtn = browser.element("[data-test-id='open-ws-btn']");
//         openBtn.click();        
//         console.log("Restore workspace worked successfully");
//         //var columnName = hamBurger.element("//div[@type='checkbox']");
//     });

//     it('Verify if save-as workspace would save the views', function () {
//         this.skip();
//         console.log("Test started - Verify if save Workspace would save the view");
//         var workspaceName = "Auto-Test-save";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var verifyWorkspace = browser.element("[data-test-id='Auto-Test-save']").isExisting();
//         assert.equal(verifyWorkspace, true);
//     });

//     it('Verify if delete workspace would delete the workspace', function () {
//         this.skip();
//         console.log("Test started - Verify if delete workspace would delete the workspace");
//         var workspaceName = "Auto-Test-delete";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var deletebtn = browser.element("[data-test-id='Auto-Test-deleteDelete']");
//         deletebtn.click()
//         var deleteOK=browser.element("[data-test-id='ws-delete-btn']");
//         deleteOK.click();
//         browser.pause(500);
//         var verifyWorkspace = browser.element("[data-test-id='Auto-Test-delete']").isExisting();
//         assert.equal(verifyWorkspace, false);
//     });

//     it('Verify if rename workspace would rename the workspace', function () {
//         this.skip();
//         console.log("Test started - Verify if rename workspace would rename the workspace");
//         var workspaceName = "Auto-Test";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var renamebtn = browser.element("[data-test-id='Auto-TestRename']");
//         renamebtn.click();
//         baseMethods.renameWorkspace("Auto-Test-rename")
//         var verifyWorkspace = browser.element("[data-test-id='Auto-Test']").isExisting();
//         var verifyRenamedWorkspace = browser.element("[data-test-id='Auto-Test-rename']").isExisting();
//         assert.equal(verifyWorkspace, false) && assert.equal(verifyRenamedWorkspace,true);
//     });

//     it('Verify if open workspace would open the workspace', function () {
//         this.skip();
//         console.log("Test started - Verify if open workspace would open the workspace");
//         var workspaceName = "Auto-Test-open";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var workspace = browser.element("[data-test-id='Auto-Test-open']");
//         workspace.click();
//         var openbtn=browser.element("[data-test-id='open-ws-btn']");
//         openbtn.click();
//         baseMethods.verifySpinnerVisibility();
//         browser.pause(8000);
//         baseMethods.clickOnLayerMenuBtn();
//         var layerValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
//         var tabName=baseMethods.verifyRightMenuHeader("Map");
//         assert.equal(layerValue && tabName, true);
//     });

//     it('Verify if save workspace would save the changes made to opened workspace', function () {
//         this.skip();
//         console.log("Test started - Verify if save workspace would save the changes made to opened workspace");
//         var workspaceName = "Autotest-save-change";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var workspace = browser.element("[data-test-id='Autotest-save-change']");
//         workspace.click();
//         baseMethods.clickOnWSOpenBtn();
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         genericObject.clickOnDTTab(parameter.WorkspacesMenu);
//         baseMethods.clickOnWorkspaceSaveBtn();
//         var closeBtn=browser.element("[data-test-id='close-btn-icon']");
//         closeBtn.click();
//         var tabCloseBtn=browser.element("[data-test-id='Mapclose-btn']");
//         tabCloseBtn.click();
//         genericObject.clickOnDTTab(parameter.WorkspacesMenu);
//         var workspace = browser.element("[data-test-id='Autotest-save-change']");
//         workspace.click();
//         baseMethods.clickOnWSOpenBtn();
//         baseMethods.clickOnLayerMenuBtn();
//         var layerRSRPValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
//         var layerRSRQValue = baseMethods.verifyLayerData(xynergyconstants.RSRQMetric);
//         assert.equal(layerRSRPValue && layerRSRQValue, true);
//     });

//     it('Verify if warning message is displayed when existing name is used', function () {
//         this.skip();
//         console.log("Test started - Verify if warning message is displayed when existing name is used");
//         var workspaceName = "test-warning-msg";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         baseMethods.plotMetric(xynergyconstants.RSRQMetric,parameter.RSRQMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var verifyWarningMsg = browser.element("[data-test-id='ws-exists-msg']").isExisting();
//         assert.equal(verifyWarningMsg, true);
//     });

//     it('Verify if error message is displayed when invalid name is used', function () {
//         this.skip();
//         console.log("Test started - Verify if error message is displayed when invalid name is used");
//         var workspaceName = "unknown@keysight.com";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var verifyWarningMsg = browser.element("[data-test-id='ws-invalid-msg']").isExisting();
//         assert.equal(verifyWarningMsg, true);
//     });

//     it('Verify if warning message is displayed when delete workspace is clicked', function () {
//         this.skip();
//         console.log("Test started - Verify if warning message is displayed when delete workspace is clicked");
//         var workspaceName = "test-delete-warning-msg";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var deletebtn = browser.element("[data-test-id='test-delete-warning-msgDelete']");
//         deletebtn.click()
//         var verifyWarningMsg = browser.element("[data-test-id='test-delete-warning-msgdelete-msg']").isExisting();
//         assert.equal(verifyWarningMsg, true);
//     });

//     it('Verify if rename workspace would rename the workspace', function () {
//         this.skip();
//         console.log("Test started - Verify if rename workspace would rename the workspace");
//         var workspaceName = "Auto-test-rename";
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         genericObject.clickOnDTTab(parameter.ViewsMenu);
//         baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
//         baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
//         baseMethods.saveWorkspace(workspaceName);
//         var renamebtn = browser.element("[data-test-id='Auto-test-renameRename']");
//         renamebtn.click();
//         var renameTab = browser.element("[data-test-id='Rename-Input']");
//         renameTab.click();
//         renameTab.keys(workspaceName);
//         var warningtext = browser.element("[data-test-id='rename-exists-warning']").isExisting();
//         assert.equal(warningtext,true);
//     });  

// });