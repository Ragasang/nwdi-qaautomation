let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });


    it('Workspace - save/restore Latency calculation in Sequence diagram through Close All Views function: XYN-2790' ,function(){
        console.log("Test Started - To Verify Workspace - save/restore Latency calculation in Sequence diagram through Close All Views function: XYN-2790");              
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);        
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        SequenceDiagramObject.clickOnCalculateLatency();        
        SequenceDiagramObject.selectDataToCalculateLatency();        
        SequenceDiagramObject.clickOnCalculateBtn();        
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.element(parameter.viewsCloseAllBtn).click();
        browser.pause(1000);
        var modalBox = browser.element("//modal-box");
        var mBoxSaveBtn = modalBox.element("[data-test-id='save-as-btn']");
        if (mBoxSaveBtn.isVisible()) {
            mBoxSaveBtn.click();
        }
        browser.pause(300);
        //browser.element(parameter.viewSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(2000);
        workspacesObject.openSavedWorkspace(workspacename);
        var calculatedLatency1 = browser.element(parameter.calculateLatency);        
        var latency1 = calculatedLatency1.isVisible();
        assert(latency1, true);  
        baseMethods.closeAllViews();     
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();           
        console.log("Test Completed - Verified Workspace - save/restore Latency calculation in Sequence diagram through Close All Views function: XYN-2790");
    });

});