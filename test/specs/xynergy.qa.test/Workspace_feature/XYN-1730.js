let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify the error message when user uses invalid name to save workspace (Ex: you@keysight.com): XYN-1730' ,function(){
        console.log("Test Started - To verify the error message when user uses invalid name to save workspace (Ex: you@keysight.com): XYN-1730");
        
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        var workspacename = "you@keysight.com";
        console.log(workspacename);
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.pause(1000);
        var verifyMsg= browser.element(parameter.workspaceINvalidMsg).getText();
        assert.equal(config.workspaceInvalidMsg, verifyMsg);
        browser.pause(1000);
        var saveBtn = browser.element(parameter.workspaceSaveBtn);
        var saveBtnStatus = false;
        if((saveBtn.getAttribute("class")).includes('disabled')){
            saveBtnStatus = true;
        }
        assert.equal(saveBtnStatus,true);
        browser.element(parameter.workspaceCancelBtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        console.log("Test Completed - Verified the error message when user uses invalid name to save workspace (Ex: you@keysight.com): XYN-1730");

    });

});