let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    afterEach(function () {
        if (isElectron()) {
            console.log("Test Completed");
        }
    });

    it('To Verify Workspace - Map View state to remember layer order and docked view AND deleting workspace: XYN-1391, XYN-1726' ,function(){
        console.log("Test Started - Verify Workspace - Map View state to remember layer order and docked view AND deleting workspace: XYN-1391, XYN-1726");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.plotMetric(config.metricNameRSRQ,parameter.RSRQMetric,parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue1 = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue1,true);
        browser.pause(500);
        browser.element(parameter.dockpinPinned).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        //browser.element(parameter.dataLayers).click();
        //browser.element(parameter.mapLayersTab).click();
        browser.pause(1500);
        var layerValue3 = baseMethods.verifyLayerData(config.metricNameRSRQ);
        assert.equal(layerValue3,true);
        console.log("verifiedRSRQ");
        browser.pause(1000);
        var layerValue4 = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue4,true);
        console.log("verifiedRSRP");
        browser.pause(500);
        var maplayer = browser.element(parameter.mapLayersTab);
        expect(maplayer.getAttribute("class")).to.include('selected');
        console.log("verified maplayer tab docked");
        baseMethods.closeAllViews();
        var workspacedeleted = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(workspacedeleted,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified Workspace - Map View state to remember layer order and docked view AND deleting workspace: XYN-1391, XYN-1726");
    });

    it('To verify whether workspace saved successfully and status message pops-up: XYN-2365, XYN-1709' ,function(){
        console.log("Test Started - To verify whether workspace saved successfully and status message pops-up: XYN-2365, XYN-1709");        
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);               
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        // var successIcon = browser.element(parameter.workspaceSuccessIcon).isVisible();
        // assert(successIcon,true);
        var successMsg = browser.element(parameter.workspaceSuccessMsg).getText();
        assert.equal(successMsg,config.successfullMsg);     
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+'Delete'+"']").click();
        browser.element(parameter.workspaceDeleteBtn).click();
        browser.pause(500);
        var workspacedeleted = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert.equal(workspacedeleted,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified whether workspace saved successfully and status message pops-up: XYN-2365, XYN-1709");

    });

    it('To verify if user able to rename the saved workspace name: XYN-1725' ,function(){
        console.log("Test Started - To verify if user able to rename the saved workspace name: XYN-1725");        
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);               
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+'Rename'+"']").click();
        var rename = baseMethods.saveFileName("Rename");
        var textbox = browser.element(parameter.workspaceRenameTxtBx);
        textbox.click();
        textbox.keys(rename);
        browser.element(parameter.workspaceRenameSaveBtn).click();
        browser.pause(1000);
        var workspaceRename = browser.element("[data-test-id='"+rename+"']").isVisible();
        assert(workspaceRename,true);
        console.log("Workspace Renamed Successfully");
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+rename+'Delete'+"']").click();
        browser.element(parameter.workspaceDeleteBtn).click();
        browser.pause(500);
        var workspacedeleted = browser.element("[data-test-id='"+rename+"']").isVisible();
        assert.equal(workspacedeleted,false);
        console.log('Workspace ' + rename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified if user able to rename the saved workspace name: XYN-1725");

    });

    it('To Verify the overwritten Workspaces saves and loads fine: XYN-1463' ,function(){
        console.log("Test Started - To Verify the overwritten Workspaces saves and loads fine: XYN-1463");
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        // searchdataObject.selectdatasetresult();
        // browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.dockpinPinned).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.pause(3000);
        var layerValue3 = baseMethods.verifyLayerData(config.metricName);
        assert.equal(layerValue3,true);
        console.log("verifiedRSRP");
        browser.element(parameter.mapViewClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.TableDroppable);
        var headerRow = browser.element(parameter.MetricNameInHeader).getText();        
        expect(headerRow).to.include(config.metricName);
        console.log(headerRow);
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element("[data-test-id='"+workspacename+'dropdown'+"']").click();
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(1000);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var headerRowtest = browser.element(parameter.MetricNameInHeader).getText();        
        expect(headerRowtest).to.include(config.metricName);
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified the overwritten Workspaces saves and loads fine: XYN-1463");
    });


    it('To verify whether prompt message is displayed when user enters the same workspace name that already exist in the workspace list: XYN-2372' ,function(){
        console.log("Test Started - To verify whether prompt message is displayed when user enters the same workspace name that already exist in the workspace list: XYN-2372");
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        // searchdataObject.selectdatasetresult();
        // browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapViewClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.FreshViewsDroppable);
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element("[data-test-id='"+workspacename+'dropdown'+"']").click();
        browser.pause(1000);
        var replacemsg = browser.element(parameter.workspaceOverwriteMsg).getText();
        assert.equal(replacemsg,config.workspaceOverwriteTxt);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - verified whether prompt message is displayed when user enters the same workspace name that already exist in the workspace list: XYN-2372");
    });

    it('To verify the warning text when user clicks on delete workspace button: XYN-1727' ,function(){
        console.log("Test Started - To verify the warning text when user clicks on delete workspace button: XYN-1727");              
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);               
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(500);     
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+'Delete'+"']").click();
        //var deleteWarningMsg = browser.element(parameter.mBoxTitleId).isVisible();
        //assert(deleteWarningMsg,true);  
        workspacesObject.validateDeleteWindowWarningMsgs(workspacename);      
        browser.element(parameter.workspaceDeleteBtn).click();
        browser.pause(500);
        var workspacedeleted = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert.equal(workspacedeleted,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified the warning text when user clicks on delete workspace button: XYN-1727");
    });

    it('To Verify Restore workspace reloads data table state one column(s) is/are added and saved: XYN-4206' ,function(){
        console.log("Test Started - Verify Restore workspace reloads data table state one column(s) is/are added and saved: XYN-4206");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.TableDroppable);
        browser.element(parameter.hamburgerMenu).click();
        browser.pause(500);
        browser.element(parameter.fileName).click();
        browser.element(parameter.hamburgerMenu).click();
        //browser.moveToObject("[data-test-id='1']");
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        browser.element(parameter.hamburgerMenu).click();
        //browser.pause(2000);
        var filenamechckbx = browser.element(parameter.fileName).isEnabled();
        console.log(filenamechckbx);
        assert(filenamechckbx,true);
        browser.element(parameter.hamburgerMenu).click();
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        
        console.log("Test Completed - Verified Restore workspace reloads data table state one column(s) is/are added and saved: XYN-4206");

    });

    it('To Verify Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393' ,function(){
        console.log("Test Started - Verify Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        // searchdataObject.selectdatasetresult();
        // browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findAllMessageFilterValue);
        //browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        messageObject.clickMessagesDataRow(1);
        //baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.TableDroppable);
        browser.element(parameter.hamburgerMenu).click();
        browser.pause(500);
        browser.element(parameter.fileName).click();
        browser.element(parameter.hamburgerMenu).click();
        messageObject.clickDecodeMessageIcon();
        //browser.moveToObject("[data-test-id='1']");
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var filtervalue = browser.element('input[data-test-id="msg-filter"]').getValue();
        console.log(filtervalue);
        expect(filtervalue).to.include(config.findAllMessageFilterValue);
        browser.element(parameter.hamburgerMenu).click();
        //browser.pause(2000);
        var filenamechckbx = browser.element(parameter.fileName).isEnabled();
        console.log(filenamechckbx);
        assert(filenamechckbx,true);
        browser.element(parameter.hamburgerMenu).click();
        var decodebtn = browser.element("[data-test-id='decode-msg-btn']");
        expect(decodebtn.getAttribute("class")).to.include('active-icon');
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        //searchdataObject.newSearchPage();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393");

    });

    it('To verify Workspace save/restore Latency calculation in Sequence diagram: XYN-1583' ,function(){
        console.log("Test Started - To verify Workspace save/restore Latency calculation in Sequence diagram: XYN-1583");              
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);        
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        SequenceDiagramObject.clickOnCalculateLatency();        
        SequenceDiagramObject.selectDataToCalculateLatency();        
        SequenceDiagramObject.clickOnCalculateBtn();        
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.incorrectQueryClose).click();        
        baseMethods.closeAllViews();        
        browser.pause(500);
        workspacesObject.openSavedWorkspace(workspacename);
        var calculatedLatency1 = browser.element(parameter.calculateLatency);        
        var latency1 = calculatedLatency1.isVisible();
        assert(latency1, true);       
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }            
        console.log("Test Completed - Verified Workspace save/restore Latency calculation in Sequence diagram: XYN-1583");
    });
    
    it('To Verify the Workspace save/restore Layers in Sequence diagram: XYN-1582' ,function(){
        console.log("Test Started - To Verify the Workspace save/restore Layers in Sequence diagram: XYN-1582");        
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();        
        baseMethods.verifySpinnerVisibility();          
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);       
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        baseMethods.clickOnColumnsbutton();        
        baseMethods.clickOnSequenceData("Data Layers");
        //baseMethods.clickOnSequenceData(config.dataLayersTab);                        
        var datalayerEvent = browser.element(parameter.seqLayerData);
        var text = datalayerEvent.getText();
        expect(text).to.include(config.eventName);         
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.incorrectQueryClose).click();        
        baseMethods.closeAllViews();        
        browser.pause(500);        
        workspacesObject.openSavedWorkspace(workspacename);
        var datalayerEvent1 = browser.element(parameter.seqLayerData);
        var text1 = datalayerEvent1.getText();
        expect(text1).to.include(config.eventName);
        baseMethods.clickOnHideBtn();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element("[data-test-id='"+workspacename+'dropdown'+"']").click();
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.incorrectQueryClose).click();        
        baseMethods.closeAllViews();        
        browser.pause(500);        
        workspacesObject.openSavedWorkspace(workspacename);
        var j = 1;
        rowCount = browser.elements("//xyn-sequence-item").value.length;        
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();            
            if (rowContent == config.eventName) {
                var img = rowImg.element("//img").isVisible();
                assert.equal(img, false);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.closeAllViews();        
        browser.pause(500);
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }                 
        console.log("Test Completed - Verified the Workspace save/restore Layers in Sequence diagram: XYN-1582");
    });

    it('To Verify whether event sync is restoring when user opens saved workspace: XYN-3464' ,function(){
        console.log("Test Started - Verify whether event sync is restoring when user opens saved workspace: XYN-3464");
        var timestamp = "03/30/2018 17:54:51.314";
        var assertvalue = false;
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRightBox);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetBottomBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='h-box-bottom']");
            obj.outerHTML = ''
        });
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='h-box-bottom']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetBottomBox);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(3000);
        browser.element("[data-test-id='widget-max-btn']").click();
        baseMethods.clickOnTimeseriesEventElement(timestamp);
        browser.element("[data-test-id='widget-max-btn']").click();
        browser.pause(1000);
        var msgTimestamp = messageObject.getTimestampforRow(1);
        messageObject.clickMessagesDataRow(1);
        console.log(msgTimestamp + ' is timestamp for Messages');
        var seqTimestamp = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
        console.log(seqTimestamp + ' is timestamp for Seq Diag');
        if(msgTimestamp && seqTimestamp == timestamp) {
            assertvalue = true;
        }
        assert.equal(assertvalue,true);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var msgTimestampVerify = messageObject.getTimestampforRow(1);
        console.log(msgTimestampVerify);
        var seqTimestampVerify = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
        console.log(seqTimestampVerify);
        // // var i = 1;
        // // seqRowCount2 = browser.elements(parameter.seqRows).value.length;
        // // //console.log(seqRowCount);
        // // while (seqRowCount2 > 0) {
        // //     var row = browser.elements("//xyn-sequence-item[" + i + "]//div//div[@data-test-id='list-line-center']");
        // //     if((row.getAttribute("class")).includes('selected')){
        // //         var seqTimestampVerify = browser.elements("//xyn-sequence-item[" + i + "]//div//span[@data-test-id='timestamp-text']").getText();
        // //         console.log(seqTimestampVerify);
        // //     }
        // //     i++;
        // //     seqRowCount2--;
        // // }
        if(msgTimestampVerify && seqTimestampVerify == timestamp) {
            assertvalue2 = true;
        }
        assert.equal(assertvalue2,true);
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        console.log("Test Completed - Verified whether event sync is restoring when user opens saved workspace: XYN-3464");

    });

});