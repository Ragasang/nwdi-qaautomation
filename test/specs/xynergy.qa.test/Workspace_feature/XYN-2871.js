let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var parameterComparisonObject = require('../../../pageobjects/parameterComparisonPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('Parameter comparision - To Verify Workspace with Parameter comparision -Cells selections: XYN-2871' ,function(){
        console.log("Test Started - To Verify Workspace with Parameter comparision -Cells selections: XYN-2871");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForPComparison5GLTETest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //Select three filenames to be compared later once workspace is restored
            parameterComparisonObject.searchInSelectCells(config.pComparisonLTEFile1);
            parameterComparisonObject.selectCheckBox();
            browser.element(parameter.datasetsFilterClear).click();
            parameterComparisonObject.searchInSelectCells(config.pComparisonLTEFile3);
            parameterComparisonObject.selectCheckBox();
            browser.element(parameter.datasetsFilterClear).click();
            parameterComparisonObject.searchInSelectCells(config.pComparison5GFile1);
            parameterComparisonObject.selectCheckBox();
            browser.element(parameter.datasetsFilterClear).click();            
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var headerverify = parameterComparisonObject.getMainHeaderFileNameValue(4);
        expect(headerverify.toLowerCase()).to.include(config.pComparisonLTEFile1.toLowerCase());
        var headerverify1 = parameterComparisonObject.getMainHeaderFileNameValue(5);
        expect(headerverify1.toLowerCase()).to.include(config.pComparisonLTEFile3.toLowerCase());
        var headerverify2 = parameterComparisonObject.getMainHeaderFileNameValue(6);
        expect(headerverify2.toLowerCase()).to.include(config.pComparison5GFile1.toLowerCase());
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
               
        console.log("Test Completed - Verified Workspace with Parameter comparision -Cells selections: XYN-2871");

    });

});