let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify if Diagnostics view restores workspace with columns added/deleted: XYN-2882' ,function(){
        console.log("Test Started - To verify if Diagnostics view restores workspace with columns added/deleted: XYN-2882");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        // searchdataObject.selectdatasetresult();
        // browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        //baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.TableDroppable);
        browser.element(parameter.hamburgerMenu).click();
        browser.pause(500);
        browser.element(parameter.diagnosis).click();
        browser.element(parameter.hamburgerMenu).click();
        //browser.moveToObject("[data-test-id='1']");
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        browser.element(parameter.hamburgerMenu).click();
        //browser.pause(2000);
        var diagnosischckbx = browser.element(parameter.diagnosis).isEnabled();
        console.log(diagnosischckbx);
        assert(diagnosischckbx,true);
        browser.element(parameter.hamburgerMenu).click();
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        //searchdataObject.newSearchPage();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        
        console.log("Test Completed - Verified if Diagnostics view restores workspace with columns added/deleted: XYN-2882");

    });

});