let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var diagnosticsObject = require('../../../pageobjects/diagnosticsPage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify workspace restore with Diagnostic View -Filter text and results: XYN-2881' ,function(){
        console.log("Test Started - To verify workspace restore with Diagnostic View -Filter text and results: XYN-2881");
            /* Import Diagnostics dataset to AV */
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
            /* Drop diagnostics view on AV and filter with text and verify if it filtered properly */
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        diagnosticsObject.enterFilterTextInDiagnostics("Problem");
        browser.pause(400);
        var j = 0;
        var rowvalue = browser.elements(parameter.diagnosticsRecords);
        var rowText = rowvalue.getText();
        var rowCount = rowvalue.value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            expect(rowText[j].toLowerCase()).to.include("problem");
            j++;
            rowCount--;
        }
            /* Save the view as workspace and close all views */
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        browser.waitForExist("[data-test-id='"+workspacename+"']");
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
            /* Open saved workspace and verify if filter value is remembered and records shown is filtered results */
        workspacesObject.openSavedWorkspace(workspacename);
        var filtervalue = browser.element('input[data-test-id="diagnosis-filter"]').getValue();
        console.log(filtervalue);
        expect(filtervalue.toLowerCase()).to.include("problem");
        var j = 0;
        var rowvalue = browser.elements(parameter.diagnosticsRecords);
        var rowText = rowvalue.getText();
        var rowCount = rowvalue.value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            expect(rowText[j].toLowerCase()).to.include("problem");
            j++;
            rowCount--;
        }
            /* Close all views and delete the workspace */
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        
        console.log("Test Completed - Verified workspace restore with Diagnostic View -Filter text and results: XYN-2881");

    });

});