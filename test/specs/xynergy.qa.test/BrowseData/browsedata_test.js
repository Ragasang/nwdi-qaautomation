let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    var dataset = baseMethods.saveFileName(config.datasetName);
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    afterEach(function () {
        if (isElectron()) {
            console.log("Test Completed");
        }
    });
    it('Verification of adding datasets using a deleted dataset name: XYN-2876 ,  Verify if user is able to delete datasets XYN-2866', function () {
        console.log("Test start - Delete a dataset and create it again with same name- XYN-2876, Verify if user is able to delete datasets XYN-2866");
        //Import data to create a dataset        
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
    });
    it('Verification of adding datasets using a deleted dataset name: XYN-2876 ,  Verify if user is able to delete datasets XYN-2866', function () {
        console.log("Test start - Delete a dataset and create it again with same name- XYN-2876, Verify if user is able to delete datasets XYN-2866");
        //Import data to create a dataset        
        baseMethods.SelectTabs(config.browseDataTab);
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        browseDataObject.enter_datsetname('"' + dataset + '"');
        browser.pause(2000);
        browser.element(parameter.checkMarkLeft).click();
        browser.pause(200);
        browseDataObject.deleteBtn();
        browser.pause(5000);
        var delDatasetsWarning = browser.element(parameter.delDatasetsWarning).isVisible();
        assert(delDatasetsWarning, true);
        browseDataObject.confirmDeleteBtn();
        browser.pause(3000);
        browseDataObject.filterDataSetName(dataset);
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        if (errorMsg == true) {
            browser.pause(200);
            assert(errorMsg, 1);
            browser.pause(200);
            browseDataObject.importIconClick();
            browser.pause(200);
            browseDataObject.addDataIconClick();
            browser.pause(200);
            importDataObject.enterDataSetName(dataset);
            browser.pause(300);
            browseDataObject.fileUpload(dataset);
            baseMethods.SelectTabs(config.browseDataTab);
            browser.pause(200);
            browser.element(parameter.checkMarkLeft).click();
            browser.pause(200);
            var replaceddataset = browser.element(parameter.replacedData);
            expect(replaceddataset.getText()).to.include(dataset);
            browser.element(parameter.datasetsFilterClear).click();
            console.log("Test end - Delete a dataset and create it again with same name XYN-2876,  Verify if user is able to delete datasets XYN-2866");

        }
        else {
            console.log("Dataset not deleted");
            browser.element(parameter.datasetsFilterClear).click();
        }
    });
});
describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    afterEach(function () {
        if (isElectron()) {
            console.log("Test Completed");
        }
    });
    //Uncomment below script once it got fixed for XYN-2370
    /*
    it('To verify whether filtering is working for every column: XYN-2370', function(){
        console.log("Test start - To verify whether filtering is working for every column - For 'Last' tab - Days");               
        baseMethods.SelectTabs("BROWSEDATA");         
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.clickApplyBtn();
        browseDataObject.clickLastDownBtn();        
        browseDataObject.clickApplyBtn();
        browseDataObject.clickLastUpBtn();
        browseDataObject.clickApplyBtn(); 
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var gridXPath = "//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]";
            browseDataObject.getCreatedGridList(gridXPath);        
        }     
        console.log("Test End - To verify whether filtering is working for every column - For 'Last' tab - Days");
    });
    it('To verify whether filtering is working for every column: XYN-2370', function(){
        console.log("Test start - To verify whether filtering is working for every column - For 'Last' tab - Weeks");               
        baseMethods.SelectTabs("BROWSEDATA");         
        browseDataObject.LastFilters(config.LastFilterWeeksValue);                
        browseDataObject.LastdropdownWeeks()
        browseDataObject.clickApplyBtn();
        baseMethods.verifySpinnerVisibility();
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var gridXPath = "//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]";
            browseDataObject.getCreatedGridList(gridXPath);        
        }
        console.log("Test End - To verify whether filtering is working for every column - For 'Last' tab - Weeks");
    });    
    it('To verify whether filtering is working for every column: XYN-2370', function () {
        console.log("Test start - To verify whether filtering is working for every column - For 'Start Time' tab");
        baseMethods.SelectTabs("BROWSEDATA");
        var Dates = browseDataObject.getDates();
        var StartDate = Dates[0];
        var EndDate = Dates[1]
        console.log(StartDate, EndDate);
        baseMethods.setStartTime(StartDate, EndDate);
        browser.pause(500);
        new_sTime = config.StartTime + config.amORpm;
        new_eTime = config.EndTime + config.amORpm;
        console.log(new_sTime + "," + new_eTime);
        browser.pause(500);
        browseDataObject.setTime_1_2(new_sTime, new_eTime)
        browser.pause(500);
        browseDataObject.clickApplyBtn();
        browser.pause(500);
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var fTime = StartDate + " " + config.StartTime + " " + config.amORpm;
            var lTime = EndDate + " " + config.EndTime + " " + config.amORpm;
            console.log(fTime + "," + lTime);
            new_fTime = browseDataObject.ConvToUNIXdates(fTime);
            new_lTime = browseDataObject.ConvToUNIXdates(lTime);
            var list = browser.elements("//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]");
            var noOfRows = list.value.length
            console.log(noOfRows);
            var j=0;
            while (noOfRows >=1 ){
                var elem = browser.elements("//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]").getText();
                browser.pause(300);
                console.log(elem[j]);
                var listTime = elem[j];
                randomTime = browseDataObject.ConvToUNIXdates(listTime);
                console.log("Unix style timing are:" + new_fTime + "," + new_lTime + "," + randomTime);
                var res = browseDataObject.compareDates(new_fTime, new_lTime, randomTime);
                console.log(res);
                assert(res, true);
                j++;
                noOfRows--;
            }
        }
        console.log("Test End - To verify whether filtering is working for every column - For 'Start Time' tab");
    });     */

    // it.skip('Verification of adding datasets using a deleted dataset name: XYN-2876 ,  Verify if user is able to delete datasets XYN-2866', function () {
    //     console.log("Test start - Delete a dataset and create it again with same name- XYN-2876, Verify if user is able to delete datasets XYN-2866");
    //     //Import data to create a dataset        
    //     importDataObject.clickImportDataMenu();
    //     importDataObject.clickImportAddDataMenu();
    //     var dataset = baseMethods.saveFileName(config.datasetName);
    //     console.log(dataset);
    //     importDataObject.enterDataSetName(dataset);
    //     browser.pause(200);
    //     browseDataObject.fileUpload(dataset);
    //     baseMethods.SelectTabs(config.browseDataTab);
    //     browseDataObject.enter_datsetname('"' + dataset + '"');
    //     browser.pause(300);
    //     browser.element(parameter.checkMarkLeft).click();
    //     browser.pause(200);
    //     browseDataObject.deleteBtn();
    //     browser.pause(200);
    //     var delDatasetsWarning = browser.element(parameter.delDatasetsWarning).isVisible();
    //     assert(delDatasetsWarning, true);
    //     browseDataObject.confirmDeleteBtn();
    //     browser.pause(200);
    //     browseDataObject.filterDataSetName(dataset);
    //     browser.pause(300);
    //     var errorMsg = browser.element(parameter.noData).isVisible();
    //     if (errorMsg == true) {

    //         browser.pause(200);
    //         assert(errorMsg, 1);
    //         browser.pause(200);
    //         browseDataObject.importIconClick();
    //         browser.pause(200);
    //         browseDataObject.addDataIconClick();
    //         browser.pause(200);
    //         importDataObject.enterDataSetName(dataset);
    //         browser.pause(300);
    //         browseDataObject.fileUpload(dataset);
    //         baseMethods.SelectTabs(config.browseDataTab);
    //         browser.pause(200);
    //         browser.element(parameter.checkMarkLeft).click();
    //         browser.pause(200);
    //         var replaceddataset = browser.element(parameter.replacedData);
    //         expect(replaceddataset.getText()).to.include(dataset);
    //         browser.element(parameter.datasetsFilterClear).click();
    //         console.log("Test end - Delete a dataset and create it again with same name XYN-2876,  Verify if user is able to delete datasets XYN-2866");

    //     }
    //     else {
    //         console.log("Dataset not deleted");
    //     }
    // });

    it('Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865', function () {
        console.log("Test started - Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865");
        baseMethods.SelectTabs(config.searchDataTab);
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        var datasetname = baseMethods.saveFileName(config.datasetName);
        console.log(datasetname);
        browser.pause(1000);
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.dataSetSaveCancelBtn).click();
        baseMethods.SelectTabs(config.browseDataTab);
        browseDataObject.LastFilters(config.LastFilterWeeksValue);
        browseDataObject.LastdropdownWeeks();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        browseDataObject.enter_datsetname('"' + datasetname + '"');
        browser.pause(1000);
        var openDisabled = browser.element(parameter.openDataDisable).getAttribute(config.class);
        expect(openDisabled).to.include(config.disabled);
        var delDisabled = browser.element(parameter.deldataDisable).getAttribute(config.class);
        expect(delDisabled).to.include(config.disabled);
        browser.element(parameter.checkMarkLeft).click();
        browser.pause(500);
        browser.element(parameter.datasetsOpenBtn).click();
        browser.pause(500);
        var ScopePanel = browser.element(parameter.scope);
        var visibility = ScopePanel.isVisible();
        assert(visibility, true);
        browser.element(parameter.DataMenu).click();
        browser.pause(200);
        browser.element(parameter.datasetsFilterClear).click();
        browser.pause(200);
        baseMethods.SelectTabs(config.searchDataTab);
        browser.pause(200);
        searchdataObject.clearsearchkeyword();
        console.log("Test completed - Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865");
    });
    it('Browse Data - To verify Filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865', function () {
        console.log("Test started - Browse Data - To verify Filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865");
        browseDataObject.importIconClick();
        browser.pause(500);
        browseDataObject.clickImportAddDataIcon();
        var dataset = baseMethods.saveFileName(config.datasetName);
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(300);
        browseDataObject.fileUpload(dataset);
        baseMethods.SelectTabs(config.browseDataTab);
        baseMethods.cLickFilesTab(config.filesTab);
        browser.pause(1000);
        browseDataObject.enter_Filename('"' + dataset + '"');
        browser.pause(1000);
        var openDisabled = browser.element(parameter.openDataDisable).getAttribute(config.class);
        expect(openDisabled).to.include(config.disabled);
        var delDisabled = browser.element(parameter.deldataDisable).getAttribute(config.class);
        expect(delDisabled).to.include(config.disabled);
        browseDataObject.filesCheckmark();
        browser.pause(200);
        browser.element(parameter.filesOpenBtn).click();
        browser.pause(200);
        var modalBox = browser.element(parameter.modalBox).isVisible();
        if (modalBox == true) {
            browser.element(parameter.dataScopeAdd).click();
            browser.pause(200);
            var ScopePanel = browser.element(parameter.scope);
            var visibility = ScopePanel.isVisible();
            assert(visibility, true);
            browser.element(parameter.DataMenu).click();
            browser.pause(2000);
            browser.element(parameter.fileNameClear).click();

        }
        else {
            browser.pause(200);
            var ScopePanel = browser.element(parameter.scope);
            var visibility = ScopePanel.isVisible();
            assert(visibility, true);
            browser.element(parameter.DataMenu).click();
            browser.pause(2000);
            browser.element(parameter.fileNameClear).click();
        }
        console.log("Test completed - Browse Data - To verify Filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865");
    });

    it('Browse Data - Verify Custom Datasets created using Search terms under Search Data- XYN-2729', function () {
        console.log("Test started - Browse Data - Verify Custom Datasets created using Search terms under Search Data- XYN-2729");
        baseMethods.SelectTabs(config.searchDataTab);
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        browser.pause(200);
        searchdataObject.selectdatasetresult();
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        var datasetname = baseMethods.saveFileName(config.datasetName);
        console.log(datasetname);
        browser.pause(1000);
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.dataSetSaveCancelBtn).click();
        baseMethods.SelectTabs(config.browseDataTab);
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename(datasetname);
        browser.pause(1000);
        var rowCount = browser.elements(parameter.filesRowsCount).value.length;
        console.log(rowCount);
        browser.element(parameter.datasetsFilterClear).click();
        browser.pause(300);
        baseMethods.SelectTabs(config.searchDataTab);
        searchdataObject.clearsearchkeyword();
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        browser.pause(200);
        searchdataObject.selectdatasetresult();
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browseDataObject.dropDownClick(datasetname);
        console.log("Append warning message is visible");
        var warningText = browser.element(parameter.appendWarningText).isVisible();
        assert.equal(warningText, true);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.newSearchPage();
        baseMethods.SelectTabs(config.browseDataTab);
        browser.pause(500);
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename(datasetname);
        browser.pause(1000);
        var rowCount2 = browser.elements(parameter.filesRowsCount).value.length;
        console.log(rowCount2);
        browser.pause(300);
        browser.element(parameter.datasetsFilterClear).click();
        assert(rowCount2 > rowCount);
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Files are succesfully appending to the existing dataset");
        console.log("Test completed - Browse Data - Verify Custom Datasets created using Search terms under Search Data- XYN-2729");
    });

    it('Verify Save Workspace from Add/replace window under Browse Data - XYN-2904', function(){
        console.log("Test started - Verify save workspace from Add/Replace window under Browse Data - XYN-2904");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Messages']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab[0]).to.include("Messages");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test completed - Verify save workspace from Add/Replace window under Browse Data - XYN-2904");
    });

    it('Verify delete datasets associated with a workspace - XYN-2868', function(){
        console.log("Test started - Verify delete datasets associated with a workspace - XYN-2868");
        var dataset = baseMethods.saveFileName(config.datasetName);
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
        browser.pause(3000);
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        // if (openBtn.isVisible()){
        //     browser.element(parameter.selectCheckBox).click();
        //     browser.element(parameter.datasetsFilterClear).click();
        // }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        browseDataObject.deleteBtn();
        browseDataObject.confirmDeleteBtn();
        browser.pause(2000);
        var workspaceList = browser.elements(parameter.workspaceList).getText();
        console.log(workspaceList);
        var listItemLength = browser.elements(parameter.workspaceList).value.length;
        console.log(listItemLength);
        var result = false;
        var i = 0;      
        var value1=0;
        while (listItemLength > i) {
            //value1= workspaceList[i];
            //console.log(value1);
            //console.log(compare);
            if (workspaceList == workspacename){
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
            }
            i++;
        }
        assert(result,true);
        browseDataObject.confirmDeleteBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(3000);
        browser.element(parameter.datasetsFilterClear).click();
        browser.pause(1000);
        baseMethods.cLickFilesTab(config.datasetsTab);
        browseDataObject.filterDataSetName(dataset);
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        assert(errorMsg,true);
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test completed - Verify delete datasets associated with a workspace - XYN-2868");
    });

    it.skip('Verify delete file for dataset associated with workspace - XYN-2906', function(){
        console.log("Test started - Verify delete file for dataset associated with workspace - XYN-2906");
        var dataset = baseMethods.saveFileName(config.datasetName);
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
        browser.pause(2000);
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename('"' +dataset+ '"');
        browser.pause(2000);
        //baseMethods.checkDatasetFilter(dataset);

        //browser.waitForVisible(parameter.selectCheckBox);
        browseDataObject.filesCheckmark();
        var open = browser.element(parameter.filesOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var verifyBtn1 = browser.element(parameter.filesOpenBtn).isExisting();
        if(verifyBtn1 == true){
            //console.log(1);
            browseDataObject.filesCheckmark();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else if(verifyBtn1 == false){
            //console.log(2);
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        browser.pause(1000);
        //browser.element(parameter.datasetsFilterClear).click();
        browseDataObject.enter_Filename(config.searchALFDataset);
        browser.pause(1000);
        browseDataObject.filesCheckmark();
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        //browser.element(parameter.datasetsFilterClear).click();
        if(open.isExisting()){
            //console.log(1);
            browseDataObject.filesCheckmark();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            //console.log(2);
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename(dataset);
        browser.pause(2000);
        browseDataObject.filesCheckmark();
        browser.element(parameter.filesDeleteBtn).click();
        browseDataObject.confirmDeleteBtn();
        browser.pause(2000);
        var workspaceList = browser.elements(parameter.workspaceList).getText();
        console.log(workspaceList);
        var listItemLength = browser.elements(parameter.workspaceList).value.length;
        console.log(listItemLength);
        var result = false;
        var i = 0;      
        while (listItemLength > i) {
            if (workspaceList == workspacename){
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
            }
            i++;
        }
        assert(result,true);
        browseDataObject.confirmDeleteBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(3000);
        browser.element(parameter.datasetsFilterClear).click();
        browseDataObject.enter_Filename('"' +dataset+ '"');
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        assert(errorMsg,true);
        browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify delete file for dataset associated with workspace - XYN-2906");
    });

    it('Verify Add Data to already opened Data with no changes in scope - XYN-2982', function(){
        console.log("Test started - Verify Add Data to already opened Data with no changes in scope - XYN-2982");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var datasetID = browser.element(parameter.replacedData);
        var dataset1 = datasetID.getText();
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        var datasetID1 = browser.element(parameter.replacedData);
        var dataset2 = datasetID1.getText();
        open.click();
        browser.element(parameter.dataScopeAddBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.scopeWidget).click();
        scopeDataObject.scopeFilter(dataset1);
        var verify1 = browser.elements("[data-test-id='"+dataset1+"']");
        expect(verify1.getText()).to.include(dataset1);
        scopeDataObject.scopeFilter(dataset2);
        var verify2 = browser.elements("[data-test-id='"+dataset2+"']");
        expect(verify2.getText()).to.include(dataset2);
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify Add Data to already opened Data with no changes in scope - XYN-2982");
    });

    it('Verify Add Data to already opened Data with changes in scope - XYN-2982', function(){
        console.log("Test started - Verify Add Data to already opened Data with changes in scope - XYN-2982");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTestMO);
        var datasetID = browser.element(parameter.replacedData);
        var dataset1 = datasetID.getText();
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        scopeDataObject.scopeFilter(config.sprintMOFileName1);
        var fileUncheck = browser.element("[data-test-id='"+config.sprintMOFileName1+"']");
        fileUncheck.click();
        browser.element(parameter.scopeFilterClear).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        var datasetID1 = browser.element(parameter.replacedData);
        var dataset2 = datasetID1.getText();
        open.click();
        browser.element(parameter.dataScopeAddBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.scopeWidget).click();
        scopeDataObject.scopeFilter(dataset1);
        var verify1 = browser.elements("[data-test-id='"+dataset1+"']");
        expect(verify1.getText()).to.include(dataset1);
        scopeDataObject.scopeFilter(dataset2);
        var verify2 = browser.elements("[data-test-id='"+dataset2+"']");
        expect(verify2.getText()).to.include(dataset2);
        browser.element(parameter.scopeFilterClear).click();
        scopeDataObject.scopeFilter(config.sprintMOFileName1);
        var fileUncheck = browser.element("[data-test-id='"+config.sprintMOFileName1+"']");
        var fileStatus = fileUncheck.isSelected();
        //console.log(fileStatus);
        assert.equal(fileStatus,false);
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify Add Data to already opened Data with changes in scope - XYN-2982");
    });

});
