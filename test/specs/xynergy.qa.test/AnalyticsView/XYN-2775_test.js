let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let timeseriesObject=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;
let config=require('../../../config.js');
require('../../commons/global.js')();

describe('Analytics Views',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    it('To verify whether right pane of the view will not open auotmatically when left pane is unpinned - XYN-2775', function(){
        console.log("Test started - To verify whether right pane of the view will not open auotmatically when left pane is unpinned - XYN-2775");
        genericObject.clickOnDTTab(parameter.ViewsMenu);           
        browser.pause(200);
        var viewsPin=browser.element("//analytics-view//views-menu//tabs//*[@class='header-button']//*[contains(@class,'pin')]");
        viewsPin.click();
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable); 
        browser.pause(200);   
        viewsPin.click();
        var tabOpen=browser.element("//*[@title='Plotted Data Layers']");
        console.log(tabOpen);
        assert.equal(tabOpen.isExisting(),false);
        console.log("Test competed - To verify whether right pane of the view will not open auotmatically when left pane is unpinned- XYN-2775");
    });    
});