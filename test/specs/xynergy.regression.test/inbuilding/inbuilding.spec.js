let baseMethods = require('../../testbase.js');
let xynergy_Constants = require('../../test_xynergy.constants.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
describe('login', function () {
    beforeEach(function () {
        console.log('Before each function- started');
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        browser.waitUntil(function() {
            console.log(browser.url().value);
            var url = browser.url().value;
            return url.indexOf('/app/drive-test') > -1;
        }, 10 * 1000);
        browser.windowHandleMaximize();
        //baseMethods.verifySpinnerVisibility();
        browser.waitForVisible("[data-test-id='grow menu-btn']",1000);
        var changePanelBtn=browser.element("[data-test-id='grow menu-btn']");
        changePanelBtn.click();
        browser.waitForVisible("[data-test-id='leftMenuTab']", 1000);
    });

    // it('should verify inbuilding layer data', function () {
    //     baseMethods.clickOnInbuildingTestMenu();
    //     baseMethods.clickOnLayerMenuBtn();
    //     baseMethods.verifySpinnerVisibility();
    //     browser.waitForVisible("[data-test-id='legend-data']",40000);
    //     var value = baseMethods.verifyLayerData(xynergy_Constants.Inbuilding);
    //     assert.equal(value, true);
    // });
});
