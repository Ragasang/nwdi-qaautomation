let baseMethods = require('../../testbase.js');
let xynergy_Constants = require('../../test_xynergy.constants.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('login page', function () {

    it('should let you login',function(){
        console.log("should let you login -started")
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        var bool=browser.isExisting("[data-test-id='Analytics View']");
        assert.equal(bool,true);
        console.log("should let you login -done")
    });  
});
