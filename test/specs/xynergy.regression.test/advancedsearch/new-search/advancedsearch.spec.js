// let baseMethods = require('../../../testbase.js');
// let xynergy_Constants = require('../../../test_xynergy.constants.js');
// var dragAndDrop = require('html-dnd').codeForSelectors;
// var assert = require('assert');

// describe('advancedsearch page', function () {
//     beforeEach(function () {
//         browser.url('/account/login');
//         browser.setValue('input[name="username"]', 'demotest', 100);
//         browser.setValue('input[name="password"]', 'Xceed123', 100);
//         var submitButton = browser.element("//button[@type='submit']");
//         submitButton.click();
//         browser.windowHandleMaximize();
//         //baseMethods.verifySpinnerVisibility();
//         browser.pause(2000);
//         browser.waitForVisible("[data-test-id='Analytics View']", 100);
//     });

//     it('should save filtered dataset on click of save-dataset button', function () {
//         console.log("should save filtered dataset on click of save-dataset button -started");
//         this.skip();
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         var saveButton = browser.element("[data-test-id='saveDataset']");
//         saveButton.click();
//         var datasetName = browser.element("[data-test-id='SavedNameinput']");
//         datasetName.setValue("Auto_Test");
//         var saveBtn = browser.element("[data-test-id='SaveBtn']");
//         saveBtn.click();
//         var openSaved = browser.element("[data-test-id='advanced-search-open-filters']");
//         openSaved.click();
//         browser.pause(800);
//         var savedDataset = browser.element("[data-test-id='SavedSearchBtn']");
//         savedDataset.setValue("Auto_Test");
//         var listData = browser.element("[data-test-id='listdata']");
//         var dataText = listData.getText();
//         assert.equal("Auto_Test", dataText);
//         console.log("should save filtered dataset on click of save-dataset button -done");
//     });

//     it('should save searched terms on click of save-search button', function () {
//         console.log("should save searched terms on click of save-search button -started");
//         this.skip();
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         var saveButton = browser.element("[data-test-id='saveResultBtn']");
//         saveButton.click();
//         var datasetName = browser.element("[data-test-id='SavedNameinput']");
//         datasetName.setValue("Auto_Test");
//         var saveBtn = browser.element("[data-test-id='SaveBtn']");
//         saveBtn.click();
//         var openSaved = browser.element("[data-test-id='advanced-search-open-filters']");
//         openSaved.click();
//         browser.pause(800);
//         var savedDataset = browser.element("[data-test-id='SavedSearchBtn']");
//         savedDataset.setValue("Auto_Test");
//         var listData = browser.element("[data-test-id='listdata']");
//         var dataText = listData.getText();
//         assert.equal("Auto_Test", dataText);
//         console.log("should save searched terms on click of save-search button -done");
//     });

//     it('shold display warning-text on using existing name to save the terms', function () {
//         console.log("should display warning-text on existing name to save the terms -started");
//         this.skip();
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         var saveButton = browser.element("[data-test-id='saveResultBtn']");
//         saveButton.click();
//         var datasetName = browser.element("[data-test-id='SavedNameinput']");
//         datasetName.setValue("Auto_Test");
//         var modalTitleBar = browser.isVisible("//div[@class='modal-title-bar']");
//         assert.equal(modalTitleBar, true);
//         console.log("should display warning-text on using existing name to save the terms -done");
//     });

//     it('should display warning-text on using existing name to save dataset', function () {
//         console.log("should display warning-text on using existing name to save dataset -started");
//         this.skip();
//         baseMethods.clickOnAdvancedSearchViewIcon();
//         var saveButton = browser.element("[data-test-id='saveDataset']");
//         saveButton.click();
//         var datasetName = browser.element("[data-test-id='SavedNameinput']");
//         datasetName.setValue("Auto_Test");
//         var modalTitleBar = browser.isVisible("//div[@class='modal-title-bar']");
//         assert.equal(modalTitleBar, true);
//         console.log("shold display warning-text on using existing name to save dataset -done");
//     });

//     it('should open saved dataset list on click of open saved search button', function () {
//         console.log("should open saved dataset list on click of open saved search button -started");
//         this.skip();
//         var openListBtn = browser.element("[data-test-id='advanced-search-open-filters']");
//         openListBtn.click();
//         baseMethods.verifySpinnerVisibility();
//         var OpenSavedSrchTab = browser.element("[data-test-id='Open Saved Search-Title-Bar']");
//         var text = OpenSavedSrchTab.getText();
//         assert.equal(text, "Open Saved Search");
//         console.log("shold open saved dataset list on click of open saved search button -done");
//     })

//     it('should filter saved dataset', function () {
//         console.log("should filter saved dataset -started");
//         this.skip();
//         var openListBtn = browser.element("[data-test-id='advanced-search-open-filters']");
//         openListBtn.click();
//         baseMethods.verifySpinnerVisibility();
//         browser.pause(1000);
//         browser.waitForVisible("[data-test-id='SavedSearchBtn']", 40000);
//         browser.setValue("[data-test-id='SavedSearchBtn']", "Auto_test");
//         browser.waitForVisible("[data-test-id='SavedSearchList']");
//         var searchedElmnt = browser.element("[data-test-id='SavedSearchList']");
//         var text = searchedElmnt.getText();
//         assert.equal(text, "Auto_test");
//         console.log("should filter saved dataset -done");
//     })

//     it('should open saved query on click of open saved search button', function () {
//         console.log("should open saved query on click of open saved search button -started");
//         this.skip();
//         var openListBtn = browser.element("[data-test-id='advanced-search-open-filters']");
//         openListBtn.click();
//         baseMethods.verifySpinnerVisibility();
//         browser.waitForVisible("[data-test-id='SavedSearchBtn']", 40000);
//         browser.setValue("[data-test-id='SavedSearchBtn']", "Auto_test");
//         var savedsrchBtn = browser.element("[data-test-id='listdata']");
//         savedsrchBtn.click();
//         var openSavedSrchBtn = browser.element("[data-test-id='OpenSavedSrchBtn']")
//         openSavedSrchBtn.click();
//         baseMethods.verifySpinnerVisibility();
//         browser.pause(1000);
//         var nqlTextArea = browser.element("[data-test-id='NQLquery-text-area']");
//         var nqlText = nqlTextArea.getText();
//         assert.equal(nqlText, "Channel in  (5230)");
//         console.log("should open saved query on click of open saved search button -done");
//     })

//     it('should verify open saved search cancel button', function () {
//         console.log("should verify open saved search cancel button -started");
//         this.skip();
//         var openListBtn = browser.element("[data-test-id='advanced-search-open-filters']");
//         openListBtn.click();
//         baseMethods.verifySpinnerVisibility();
//         browser.pause(1000);
//         browser.waitForVisible("[data-test-id='SavedSearchBtn']", 40000);
//         var cancelBtn = browser.element("[data-test-id='OpenSavedSrchCancel']");
//         cancelBtn.click();
//         var openListBtn = browser.isVisible("[data-test-id='modal-title-bar']");
//         assert.equal(openListBtn, false);
//         console.log("should verify open saved search cancel button -done");
//     })

//     it('should open filter option on click of filters button', function () {
//         console.log("open filters option on click of filters button -started");
//         this.skip();
//         var filtersButton = browser.element("[data-test-id='filterButton']");
//         filtersButton.click();
//         browser.pause(1000);
//         var headerElmnt = browser.element("[data-test-id='asc-sidebar-header']");
//         var headerElmntText = headerElmnt.getText();
//         assert.equal(headerElmntText, "Time Filters");
//         console.log("open filters option on click of filters button -done");
//     })
// });
