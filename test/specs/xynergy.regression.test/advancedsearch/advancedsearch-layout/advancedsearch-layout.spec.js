let baseMethods = require('../../../testbase.js');
let parameter= require('../../../parameters.js');
genericObject=require('../../../generic_objects.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('advancedsearch page', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        browser.windowHandleMaximize();
       // baseMethods.verifySpinnerVisibility();
        browser.waitForVisible("[data-test-id='Analytics View']");
        baseMethods.checkForWorkspaceWidget();
    });

    it('should have import data menu', function () {
        console.log("should have import data menu -started");
        var value = false;
        var importText=browser.element("[data-test-id='Import Data']").getText();
        if (importText == "Import Data") {
                value = true;
            }
          assert.equal(value, true)
        console.log("should have import data menu -done");
    });

    it('should have search data menu', function () {
        console.log("should have search data menu -started");
        var value = false;
        var searchText=browser.element("[data-test-id='Search Data']").getText();
        if (searchText == "Search Data") {
                value = true;
            }
        assert.equal(value, true)
        console.log("should have search data menu -done");
    });

    // it('should have search button', function () {
    //     console.log("should have search button -started");
    //     var searchTab=browser.element("[data-test-id='Search Data']");
    //     searchTab.click();
    //     var searchButton = browser.isVisible("[data-test-id='searchButton']");
    //     assert.equal(searchButton, true);
    //     console.log("should have search button -done");
    // });

    it('shuold have filters button', function () {
        console.log("should have filters button -started");
        var searchTab=browser.element("[data-test-id='Search Data']");
        searchTab.click();
        var filtersButton = browser.isVisible("[data-test-id='filterButton']");
        assert.equal(filtersButton, true);
        console.log("should have filters button -done");
    });

    it('should have Enter NQL Query message', function () {
        console.log("should have Enter NQL Query message -started");
        var searchTab=browser.element("[data-test-id='Search Data']");
        searchTab.click();
        var text = "Enter NQL Query";
        browser.pause(500);
        var queryTab = browser.element("[data-test-id='NQLquery-text-area']");
        var queryTabText = queryTab.getText();
        assert.equal(queryTabText, text);
        console.log("should have Enter NQL Query message -done");
    });

    it('should display error message on searching invalid query', function () {
        console.log("should display error message on searching invalid query -started");
        var searchTab=browser.element("[data-test-id='Search Data']");
        searchTab.click();
        browser.pause(500);
        var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
        newQueryTab.click();
        newQueryTab.keys("channel in ");
        var searchButton = browser.element("[data-test-id='searchButton']");
        searchButton.click();
        baseMethods.verifySpinnerVisibility();
        var errorMsg = browser.isVisible("[data-test-id='error-message']");
        assert.equal(errorMsg, true);
        console.log("should display error message on searching invalid query -done");
    });
});