let baseMethods = require('../../../testbase.js');
let parameter= require('../../../parameters.js');
genericObject=require('../../../generic_objects.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('Analytics Builder as selected widget', function () {

        it('should open new flowchart page when clicked on create new', function () {
            console.log("should open new flowchart page when clicked on create new -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.AnalyticsbuilderDraggable,parameter.WidgetRoot);
            var createnewBtn=browser.element("[data-test-id='create-new']");
            createnewBtn.click();
            var newFlowChart=browser.isVisible("[data-test-id='New Flowchart 1']");
            assert.equal(newFlowChart, true);
            console.log("should open new flowchart page when clicked on create new -done");
        });

        it('should open Analytics builder menu', function () {
            console.log("should open Analytics builder menu -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.AnalyticsbuilderDraggable,parameter.WidgetRoot);
            var text="Analytics Builder (Beta)";
            var value=false;
            var analyticsBuilder=browser.element("[data-test-id='right-tab-menu-header']").getText();
            if(analyticsBuilder === text){
                value = true;
            }
            assert.equal(value,true);
            console.log("should open Analytics builder menu -done");
        });

    });
});
