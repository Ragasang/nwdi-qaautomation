let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let timeseries=require("../../../../pageobjects/timeseries.js");
let parameter = require("../../../parameters.js");
var xynergyconstants=require('../../../test_xynergy.constants.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        browser.pause(2000);
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('timeseries as selected widget', function () {

        it('should delete layer data on click of remove all button', function () {
            console.log("should delete layer data on click of remove all button -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
            baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.TimeseriesDroppable);
            timeseries.clickOnTimeSeriesLayerBtn();
            var layerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
            assert.equal(layerValue, true);
            baseMethods.clickOnTimeSeriesRemoveAllBtn();
            var layerTabValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
            assert.equal(layerTabValue, false);
            console.log("should delete layer data on click of remove all button -done");
        });
        
        it('should render multiple metrics', function () {
            console.log("should render multiple metrics -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
            baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.TimeseriesDroppable);
            baseMethods.plotMetric(xynergyconstants.RSRQMetric,parameter.RSRQMetric,parameter.TimeseriesDroppable);
            timeseries.clickOnTimeSeriesLayerBtn();
            var layerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
            var layerTabValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRQMetric);
            assert.equal(layerValue && layerTabValue, true);
            console.log("should render multiple metrics -done");
        });

        it('should render multiple events', function () {
            console.log("should render multiple events -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
            baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
            baseMethods.plotEvent(xynergyconstants.VoiceSetupEvent,parameter.VoiceSetupEvent,parameter.TimeseriesDroppable);
            timeseries.clickOnTimeSeriesLayerBtn();
            var layerValue = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
            var layerTabValue=baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceSetupEvent);
            assert.equal(layerValue && layerTabValue, true);
            console.log("should render multiple events -done");
        });

        it('should render Voice Call Start event', function () {
            console.log("should render Voice Call Start event -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRoot);
            baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
            timeseries.clickOnTimeSeriesLayerBtn();
            var layerValue = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
            assert.equal(layerValue, true);
            console.log("should render Voice Call Start event -done");
        });
    });
});