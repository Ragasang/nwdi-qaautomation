let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let parameter = require("../../../parameters.js");
let sequence_diagram = require("../../../../pageobjects/sequence-diagram.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('sequence-diagram as selected widget', function () {

        it('should load sequence diagram with data', function () {
            console.log("should load sequence diagram with data -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.SequencediagramDraggable,parameter.WidgetRoot);
            var dataTab = browser.elements("[data-test-id='list-line-center']");
            var objectCount = dataTab.value;
            var elementCount = objectCount.length;
            assert(elementCount >= 1);
            console.log("should load sequence diagram with data -Done");
        });

        // it('should calculate latency', function () {
        //     console.log("should calculate latency -Started");
        //     this.skip();
        //     baseMethods.clickOnAdvancedSearchViewIcon();
        //     genericObject.clickOnDTTab(parameter.ViewsMenu);
        //     baseMethods.dragAndDrop(parameter.SequencediagramDraggable,parameter.WidgetRoot);
        //     sequence_diagram.clcikOnCalculateLatency();
        //     sequence_diagram.selectDataToCalculateLatency();
        //     sequence_diagram.clickOnCalculateBtn();
        //     var latencyValue = baseMethods.calculateLatency();
        //     var calculatedLatency = sequence_diagram.getCalculatedLatency();
        //     assert.equal(calculatedLatency, latencyValue);
        //     console.log("should calculate latency -Done");
        // });

        it('should load the Timestamp on click of Show Timestamps', function () {
            console.log("should load the Timestamp on click of Show Timestamps- Started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.SequencediagramDraggable,parameter.WidgetRoot);
            var latencyBtn = browser.elements("[data-test-id='timestamp-text']");
            var objectCount = latencyBtn.value;
            var elementCount = objectCount.length;
            assert(elementCount >= 1);
            console.log("should load the Timestamp on click of Show Timestamps -Done");
        });

        it('should display the calculate latency options on click of Calculate Latency', function () {
            console.log("should display the calculate latency options on click of Calculate Latency -Started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.SequencediagramDraggable,parameter.WidgetRoot);
            sequence_diagram.clickOnCalculateLatency();
            sequence_diagram.selectDataToCalculateLatency();
            sequence_diagram.clickOnCalculateBtn();
            var latencyMsg = browser.isVisible("[data-test-id='Latency-message']");
            var calculateBtn = browser.isVisible("[data-test-id='Calculate-btn']");
            var cancelBtn = browser.isVisible("[data-test-id='Cancel-btn']");
            assert.equal(latencyMsg && calculateBtn && cancelBtn, true);
            console.log("should display the calculate latency options on click of Calculate Latency -Done");
        });

        it('should hide the calculate latency options on click of Cancel', function () {
            console.log("should hide the calculate latency options on click of Cancel -Started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.SequencediagramDraggable,parameter.WidgetRoot);
            sequence_diagram.clickOnCalculateLatency();
            sequence_diagram.clickOnCalculateLatencyCancelBtn();
            var latencyMsg = browser.isVisible("[data-test-id='Latency-message']");
            var calculateBtn = browser.isVisible("[data-test-id='Calculate-btn']");
            var cancelBtn = browser.isVisible("[data-test-id='Cancel-btn']");
            assert.equal(latencyMsg && calculateBtn && cancelBtn, false);
            console.log("should hide the calculate latency options on click of Cancel -Done");
        });
    })
});