let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let parameter=require("../../../parameters.js");
let xynergyconstants=require("../../../test_xynergy.constants.js");
var assert = require('assert');

describe('drive-test metrics menu', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    it('should have search bar', function () {
        console.log("should have search bar -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        var searchtabValue = false;
        if (browser.isVisible("[data-test-id='Metrics-search-box']")) {
            searchtabValue = true;
        }
        assert.equal(searchtabValue, true);
        console.log("should have search bar -done");
    });

    it('should have drag and drop message', function () {
        console.log("should have drag and drop message -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        var notificationPanel = browser.element("[data-test-id='Metrics-notification-panel']");
        var notificationPanelText = notificationPanel.getText();
        assert.equal("Drag and drop items from below to views.", notificationPanelText);
        console.log("should have drag and drop message -done");
    });

    it('should filter RSRP metric under metrics component', function () {
        console.log("should filter RSRP metric under metrics component -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        var filteredMetric=baseMethods.filterMetric(xynergyconstants.RSRPMetric);
        assert.equal(filteredMetric, xynergyconstants.RSRPMetric);
        console.log("should filter RSRP metric under metrics component -done");
    });

    it('should have LTE technology in filtered RSRP metric', function () {
        console.log("should have LTE technology in filtered RSRP metric -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        var text= baseMethods.verifyFilteredMetricTechnology(xynergyconstants.RSRPMetric);     
        assert.equal(text, "LTE")
        console.log("should have LTE technology in filtered RSRP metric -done");
    });

    it('render RSRP metric and verify list tab in layer component', function () {
        console.log("render RSRP metric and verify list tab in layer component -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
        baseMethods.clickOnLayerMenuBtn();
        var value = baseMethods.verifyDTMenuHeader("List");
        assert.equal(value, true);
        console.log("render RSRP metric and verify list tab in layer component -done");
    });

    // it('render RSRP metric and verify histogram tab in layer component', function () {
    //     console.log("render RSRP metric and verify histogram tab in layer component -started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
    //     baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
    //     baseMethods.clickOnLayerMenuBtn();
    //     var value = baseMethods.verifyDTMenuHeader("Histogram");
    //     assert.equal(value, true);
    //     console.log("render RSRP metric and verify histogram tab in layer component -done");
    // });

    it('should render RSRP and RSRQ metric', function () {
        console.log("should render RSRP and RSRQ metric -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
        browser.pause(1000);
        baseMethods.plotMetric(xynergyconstants.RSRQMetric,parameter.RSRQMetric,parameter.PlotAcrossWidgets);
        baseMethods.clickOnLayerMenuBtn();
        var layerData=baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
        var layerTabData=baseMethods.verifyLayerData(xynergyconstants.RSRQMetric);
        assert.equal(layerData && layerTabData, true);
        console.log("should render RSRP and RSRQ metric -done");
    });
    
});