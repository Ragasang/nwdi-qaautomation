let baseMethods = require('../../testbase.js');
let genericObject = require('../../genericObjects.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
describe('login', function () {
    beforeEach(function () {
        console.log('Before each function- started');
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        browser.waitUntil(function() {
            console.log(browser.url().value);
            var url = browser.url().value;
            return url.indexOf('/app/drive-test') > -1;
        }, 10 * 1000);
        browser.windowHandleMaximize();
        //baseMethods.verifySpinnerVisibility();
        browser.waitForVisible("[data-test-id='grow menu-btn']",1000);
        var changePanelBtn=browser.element("[data-test-id='grow menu-btn']");
        changePanelBtn.click();
        browser.waitForVisible("[data-test-id='leftMenuTab']", 1000);
    });
     
    // it('Should verify drive route on geo map', function () {
    //     baseMethods.clickOnGeoMenu();
    //     baseMethods.clickOnLayerMenuBtn();
    //     browser.waitForVisible("[data-test-id='legend-data']", 40000);
    //     var value=baseMethods.verifyLayerData("Drive Route");
    //     assert.equal(value, true);
    // });

    // it('Should verify delete geo drive route from layers data', function () {
    //     baseMethods.clickOnGeoMenu();
    //     baseMethods.clickOnLayerMenuBtn();
    //     var maplayerTabs = browser.elements("[data-test-id='legend-data']");
    //     var deleteButton = browser.element("[data-test-id='Drive Routeclosebtn']");
    //     deleteButton.click();
    //     var value = baseMethods.verifyLayerData("Drive Route");
    //     assert.equal(value, false);
    // });

    // it('Should verify geo drive test header in map layers tab', function () {
    //     baseMethods.clickOnGeoMenu();
    //     //baseMethods.clickOnLayerMenuBtn();
    //     baseMethods.verifySpinnerVisibility();
    //     baseMethods.clickOnLayerMenuBtn();
    //     var value = baseMethods.verifyLayerData(xynergy_Constants.Drive_Test);
    //     assert.equal(value, true);
    // });

    // it('Should verify geo metric filter', function () {
    //     baseMethods.clickOnGeoMenu();
    //     genericObject.clickOnDTTab(parameter.MetricsMenu);
    //     browser.waitForVisible("[class='angular-tree-component']", 40000);
    //     var metricsTab = browser.element("//input[@type='search']");
    //     metricsTab.setValue("Best Active RSCP (Best Avg)");
    //     browser.waitForVisible("[data-test-id='filteredMetric']", 40000);
    //     var treeElement = browser.element("[data-test-id='filteredMetric']");
    //     var text = treeElement.getText();
    //     assert.equal(text, "Best Active RSCP (Best Avg)")
    // });

    // it('Should verify geo event filter', function () {
    //     baseMethods.clickOnGeoMenu();
    //     genericObject.clickOnDTTab(parameter.EventsMenu);
    //     browser.waitForVisible("[class='angular-tree-component']", 40000);
    //     var metricsTab = browser.element("//input[@type='search']");
    //     metricsTab.setValue("LTE RRC CONNECTION DROPPED");
    //     browser.waitForVisible("[data-test-id='filteredEvent']", 40000);
    //     var treeElement = browser.element("[data-test-id='filteredEvent']");
    //     var text = treeElement.getText();
    //     assert.equal(text, "LTE RRC CONNECTION DROPPED")
    // });
    
    // it('Should verify geo metric plot', function () {
    //     baseMethods.clickOnGeoMenu();
    //     genericObject.clickOnDTTab(parameter.DataMenu);
    //     baseMethods.verifySpinnerVisibility();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.mapDragAndDrop();
    //     genericObject.clickOnDTTab(parameter.MetricsMenu);
    //     browser.waitForVisible("[class='angular-tree-component']", 40000);
    //     var metricsTab = browser.element("//input[@type='search']");
    //     metricsTab.setValue("Best Active RSCP (Best Avg)");
    //     browser.waitForVisible("[data-test-id='metricNameundefined']",40000);
    //     browser.execute(dragAndDrop,"[data-test-id='metricNameBest Active RSCP (Best Avg)']","[data-test-id='map-root']");
    //     baseMethods.verifySpinnerVisibility();
    //     baseMethods.clickOnLayerMenuBtn();
    //     var layerValue=baseMethods.verifyLayerData("Best Active RSCP (Best Avg)");
    //     assert.equal(layerValue , true);
    // });

    // it('should verify geo event plot', function () {
    //     baseMethods.clickOnGeoMenu();
    //     genericObject.clickOnDTTab(parameter.DataMenu);
    //     baseMethods.verifySpinnerVisibility();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.mapDragAndDrop();
    //     genericObject.clickOnDTTab(parameter.EventsMenu);
    //     browser.waitForVisible("[class='angular-tree-component']", 40000);
    //     var metricsTab = browser.element("//input[@type='search']");
    //     metricsTab.setValue("LTE RRC CONNECTION DROPPED");
    //     browser.waitForVisible("[data-test-id='eventNameundefined']",40000);
    //     browser.execute(dragAndDrop,"[data-test-id='eventNameLTE RRC CONNECTION DROPPED']","[data-test-id='map-root']");
    //     baseMethods.verifySpinnerVisibility();
    //     baseMethods.clickOnLayerMenuBtn();
    //     var layerValue=baseMethods.verifyLayerData("Single Heatmap- LTE RRC CONNECTION DROPPED");
    //     assert.equal(layerValue, true);
    // });
});
