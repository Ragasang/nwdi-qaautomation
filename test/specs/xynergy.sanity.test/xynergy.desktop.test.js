let baseMethods = require('../testbase.js');
let genericObject = require("../generic_objects.js");
let parameter = require("../parameters.js");
let timeseries= require("../../pageobjects/timeseries.js");
let xynergyconstants = require("../test_xynergy.constants.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
require('../commons/global.js')();

describe('drive-test page', function () {

    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });
    it.only('should render RSRP metric on drive route', function () {
        console.log("should render RSRP metric on drive route -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
        baseMethods.closeMapDataLayer();
        assert.equal(layerValue, true);
        console.log("should render RSRP metric on drive route -done");
    });

    it('verify synchronization of data on timeseries,table view and messages widget', function () {
        console.log("verify synchronization of data on timeseries,table view and messages widget");
        if(!isElectron()){
        browser.windowHandleFullscreen();
        }
        
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048";
        if(!isElectron()){
        baseMethods.clickOnAdvancedSearchViewIcon();
        baseMethods.skipSomeDataFiles();        
        }
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.MessageTemplate);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='v-box-right']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
        baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
        if(!isElectron()){var windowSize=browser.windowHandleSize();
        console.log(windowSize);}
        baseMethods.clickOnTimeseriesEventElement(timestamp);

        var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
        browser.pause(500);
        var msgcellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]/div[1]/div[1]").getText();
        var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
        var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
        if (tablecellcontent && msgcellcontent && timeseriestooltipContent == timestamp) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
        console.log("verify synchronization of data on timeseries,table view and messages widget");
    });

    it('should render RSRP metric on time series', function () {
        console.log("should render RSRP metric on time series -started");
        if(!isElectron()){
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500)
        //genericObject.clickOnDTTab(parameter.ViewsMenu);
        }
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.MessageTemplate);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();
        var layerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
        if(isElectron){
            baseMethods.closeTimeSeriesDataLayer();
        }
        assert.equal(layerValue, true);
        console.log("should render RSRP metric on time series -done");
    });

    it('should verify table view', function () {
        console.log("should verify table view -started");
        if(!isElectron()){
        baseMethods.clickOnAdvancedSearchViewIcon();
        }
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.MessageTemplate);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.TableDroppable);
        var value = baseMethods.verifyData();
        assert.equal(value, true);
        console.log("should verify table view -done");
    });

    it('Verify import data feature', function () {
        console.log("Verify import data feature");
        if(!isElectron()){
        baseMethods.clickOnAdvancedSearchViewIcon();
        }
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.MessageTemplate);
        browser.pause(200);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData("Drive Route");
        baseMethods.closeMapDataLayer();
        assert.equal(layerValue, true);
        console.log("Verify import data feature");
    });

    it('Verify browse data feature', function () {
        console.log("Verify browse data feature");
        this.skip();
        baseMethods.checkForWorkspaceWidget();
        var browseData = browser.element("[data-test-id='Browse Data']");
        browseData.click();
        baseMethods.setStartTime("07/01/2018", "07/08/2019");
        baseMethods.filterBrowseData("voice_sprint-mt");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        browser.pause(200);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData("Drive Route");
        assert.equal(layerValue, true);
        console.log("Verify browse data feature");
    });
    
    // it('should verify plot across multiple widgets on single drag and drop', function(){
    //     console.log("should verify plot across multiple widgets on single drag and drop -started");
    //     if(!isElectron()){
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     }
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.MapDraggable,parameter.MessageTemplate);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRightBox);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
    //     baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
    //     browser.pause(1000);
    //     timeseries.clickOnTimeSeriesLayerBtn();
    //     var timeLayerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
    //     var timeSeriesLayerBtn=browser.element("[data-test-id='close-button']");
    //     timeSeriesLayerBtn.click();
    //     baseMethods.clickOnLayerMenuBtn();
    //     var metricLayerValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
    //     var value = baseMethods.verifyRightMenuHeader("Metric View");
    //     if(isElectron){
    //          baseMethods.closeMapDataLayer();
    //     }
    //     assert.equal(metricLayerValue && timeLayerValue && value, true);  
    //     console.log("should verify plot across multiple widgets on single drag and drop -Done");        
    // });

    // it('verify synchronization of data on timesiries and sequence diagram widgets', function () {
    //     console.log("verify synchronization of data on timesiries and sequence diagram widgets -Started");       
    //     var assertValue = false;
    //     var timestamp = "03/30/2018 18:09:34.048"; 
    //     if(isElectron()){
    //         this.skip();
    //     }
    //     if(!isElectron()){ 
    //     browser.windowHandleFullscreen();      
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     baseMethods.skipSomeDataFiles();  
    //     }      
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.MessageTemplate);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetBottomBox);
    //     baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
    //     baseMethods.clickOnTimeseriesEventElement(timestamp);
    //     var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
    //     var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
    //     browser.pause(500);
    //     var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
    //     sequenceRowSelected.click();
    //     browser.pause(100);
    //     var sequenceToolTip=browser.element("//div[@class='tooltip-container']");
    //     var sequenceToolTipContent=sequenceToolTip.getText();
    //     console.log(sequenceToolTipContent);
    //     console.log(timeseriestooltipContent);
    //     if (sequenceToolTipContent && timeseriestooltipContent == timestamp) {
    //         assertValue = true;
    //     }
    //     assert.equal(assertValue, true);
    //     console.log("verify synchronization of data on timesiries and sequence diagram widgets -Done");
    // });

    // it('verify synchronization of data on sequence-diagram,table view and messages widget', function () {
    //     console.log("verify synchronization of data on sequence-diagram,table view and messages widget -Started");
    //     var assertValue = false;
    //     if(!isElectron()){
    //     browser.windowHandleFullscreen();      
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     }
    //     if(isElectron()){
    //         this.skip();
    //     }
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.MessageTemplate);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
    //     browser.execute(() => {
    //         let obj = document.querySelector("[data-test-id='v-box-right']");
    //         obj.outerHTML = ''
    //     });
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetRightBox);
    //     baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
    //     var eventtableRow=browser.element("//datatable-row-wrapper[4]");
    //     eventtableRow.click();      
    //     var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
    //     browser.pause(500);
    //     var msgcellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]/div[1]/div[1]").getText();
    //     browser.pause(500);
    //     var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
    //     console.log("clicking on sequence diagram event");
    //     sequenceRowSelected.click();
    //     console.log("clicked");
    //     var sequenceToolTip=browser.element("//div[@class='tooltip-container']").getText();
    //     if (tablecellcontent && msgcellcontent && sequenceToolTip == tablecellcontent) {
    //         assertValue = true;
    //     }
    //     assert.equal(assertValue, true);
    //     console.log("verify synchronization of data on sequence-diagram,table view and messages widget -Done");
    // });
});