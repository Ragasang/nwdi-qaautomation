let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
require('../../commons/global.js')();

describe('drive-test page', function () {

    var init=0;
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest)
            
            console.log("Logging successful");
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }

    });

    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }        
    });
    it('should render RSRP metric on drive route', function () {
        console.log("should render RSRP metric on drive route -started");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.pause(300);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert.equal(layerValue, true);
        console.log("should render RSRP metric on drive route -done");
    });
});