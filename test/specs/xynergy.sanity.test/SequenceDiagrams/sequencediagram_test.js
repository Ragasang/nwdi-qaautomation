let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });
    afterEach(function () {
        if (isElectron()) {
            console.log("closing all open views");
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            browser.pause(500);
            console.log("closing all open views");
            var closeAllViews = browser.element("[data-test-id='close-all-btn']");
            if (closeAllViews.isVisible()) {
                closeAllViews.click();
                browser.pause(300);
                console.log("clicking on close btn");
                //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
                var modalBox = browser.element("//modal-box");
                var mBoxCloseBtn = modalBox.element("[data-test-id='close-all-views']");
                if (mBoxCloseBtn.isVisible()) {
                    mBoxCloseBtn.click();
                    console.log("clicked on close btn");
                }
                browser.pause(300);
                //close views menu
                var viewsHeader = browser.element("//views-menu");
                var closeViewsMenu = viewsHeader.element("[data-test-id='close-button']");
                if (closeViewsMenu.isVisible()) {
                    closeViewsMenu.click();
                }
            }
        }

    });

    it('Verify whether event symbol is displaying on the sequence diagram- XYN-2366', function () {
        console.log("Test started- event symbol is displaying on the sequence diagram XYN-2366");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        rowCount = browser.elements(parameter.seqRows).value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var imgVal = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']//img").isVisible();
                assert.equal(imgVal, true);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- event symbol is displaying on the sequence diagram XYN-2366");
    });

    it('Verify of two lanes in sequence diagram- XYN-1710', function () {
        console.log("Test started- verification of two lanes in the sequence diagram XYN-1710");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        var sequenceBlock = browser.elements(parameter.seqRows);
        var headerBlock = sequenceBlock.elements(parameter.seqHeaderBlock);
        var leftItem = headerBlock.element(parameter.seqHeaderLineLeft);
        expect(leftItem.getText()).to.include(config.sequenceLeftLaneItemName);
        var rightItem = headerBlock.element(parameter.seqHeaderLineRight);
        expect(rightItem.getText()).to.include(config.sequenceRightLaneItemName);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- verification of two lanes in the sequence diagram XYN-1710");
    });

    it('Verify whether RRC messages are present on Sequence diagram, XYN-2777', function () {
        console.log("Test started to verify RRC connection message");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        // var sequenceBlock = browser.elements("//xyn-sequence-item");
        var rowMessage = browser.elements(parameter.rowMessage);
        var Message = rowMessage.value;
        var value = false;
        Message.forEach(element => {
            var messageValue = element.getText();
            if (messageValue == config.rrcConnection) {
                value = true;
            }
        })
        assert.equal(value, true);
        console.log("Test completed to verify RRC connection message-XYN-2777");
    });

    it('Verify events and latency calculation sync with data layers, XYN-1710', function () {
        console.log("Test started to verify sync between Events and latency calculation with data layers");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        baseMethods.clickOnColumnsbutton();
        var datalayer = browser.element(parameter.dataLayersTab);
        datalayer.click();
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var CalculatedLatency = browser.element(parameter.calculateLatency);
        var bool = CalculatedLatency.isVisible();
        assert(bool, true);
        baseMethods.clickOnHideBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.clickOnCloseBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var datalayerEvent = browser.element(parameter.seqLayerData);
        var text = datalayerEvent.getText();
        expect(text).to.include(config.eventName);
        baseMethods.clickOnHideBtn();
        var j = 1;
        rowCount = browser.elements("//xyn-sequence-item").value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var img = rowImg.element("//img").isVisible();
                assert.equal(img, false);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.clickOnCloseBtn();
        var NoLayers = browser.isVisible(parameter.noLayerMsg);
        assert.equal(NoLayers, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test started to verify sync between Events and latency calculation with data layers - Xyn-1710");
    });


    it("Verify that selected messages to calculate latency is showing up under data layers - XYN-1467", function () {
        console.log("Selected message to calculate latency is showing under data layers");
        var result = [];
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        var result = baseMethods.SelectedDataToCalculateLatency();
        // var value = SelectedOne.value;
        SequenceDiagramObject.clickOnCalculateBtn();
        baseMethods.clickOnColumnsbutton();
        baseMethods.clickOnSequenceData(config.dataLayersTab);
        var datalayer = browser.element(parameter.seqLayerData);
        var text = datalayer.getText();
        browser.pause(200);
        console.log(result[0], result[1]);
        expect(text).to.include(result[0]);
        expect(text).to.include(result[1]);
        console.log("Test completed for - Selected message to calculate latency is showing under data layers-XYN-1467");
    });

    it('To verify selection of messages for latencty calculation- XYN-1466', function () {
        console.log("Test started- To verify selection of messages for latencty calculation- XYN-1466");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        //browser.waitForExist("[data-test-id='diagram-view-droppable']");
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        console.log("Test completed- To verify selection of messages for latencty calculation- XYN-1466");
    });
    it('To check Events & Latency  calculation sync with  Data layers, XYN-1449', function () {
        console.log("Test started To check Events & Latency  calculation sync with  Data layers, XYN-1449");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        baseMethods.clickOnColumnsbutton();
        browser.pause(200);
        var datalayer = browser.element(parameter.dataLayersTab);
        datalayer.click();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert.equal(Nolayer, true);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        baseMethods.clickOnHideBtn();
        assert(latency, false);
        baseMethods.clickOnCloseBtn();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert(Nolayer, true);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        SequenceDiagramObject.clickOnPlotedEvent();
        var eventtooltip = browser.element(parameter.eventImgTooltip);
        var eventinfo = eventtooltip.isVisible();
        assert(eventinfo, true);
        baseMethods.clickOnHideBtn();
        baseMethods.clickOnCloseBtn();
        var Nolayer = browser.element(parameter.noLayerMsg).isVisible();
        assert(Nolayer, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed To check Events & Latency  calculation sync with  Data layers, XYN-1449");

    });
    it('To verify styling in sequence diagram, XYN-1461', function () {
        console.log("To verify styling in sequence diagram, XYN-1461");
        //  baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        var buttonActive = browser.element(parameter.activeButton).getAttribute("class");
        assert(buttonActive == config.selectedMessage);
        console.log("Test completed To verify styling in sequence diagram, XYN-1461");
    });
    
    it('To Verify the different selected Latencies color, XYN-1468', function () {
        console.log("To Verify the different selected Latencies color, XYN-1468");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor1 = browser.element(parameter.firstLatencyColor).getAttribute("style");
        console.log(latencyColor1);
        SequenceDiagramObject.selectDataToCalculateLatency2();
        SequenceDiagramObject.clickOnCalculateBtn();
        var latencyColor2 = browser.element(parameter.secondlatencyColor).getAttribute("style");
        console.log(latencyColor2);
        assert.notEqual(latencyColor1, latencyColor2);
        console.log("Test completed - To Verify the different selected Latencies color, XYN-1468");

}); 
}); 