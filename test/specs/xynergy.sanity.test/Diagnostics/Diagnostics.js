let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
            init=1;
            }
        }
        
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }      
    });
    it('Verify Diagnostics View tab is displayed on canvas with the required columns XYN-1377', function() {
        console.log("Test Started - to verify Diagnostics view is displayed on canvas with the required columns");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log(browser.element(parameter.DiagnostaticsDraggable).value.length);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        var tabNAme=browser.element("[data-test-id='Diagnostics']").getText();
        expect(tabNAme).to.include("Diagnostics");
        var diagnosticsGrid=browser.elements("//app-data-grid[@name='diagnosis-grid']");
        var header=diagnosticsGrid.elements("//datatable-header");
        var headerCell=header.elements("//datatable-header-cell");
        console.log(headerCell.getText());
        expect(headerCell.getText()).to.include('Type');
        expect(headerCell.getText()).to.include('Protocol');
        expect(headerCell.getText()).to.include('Time');
        expect(headerCell.getText()).to.include('Description');
        console.log("Test Completed - to verify Diagnostics view is displayed on canvas with the required columns XYN-1377");
    });
    it('Verify message diagnostics view for rename and extended - XYN-1525', function() {        
        console.log("Test Started - Verify message diagnostics view for rename and extended XYN-1525");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        var rowClass = browser.elements("//*[@name='diagnosis-grid']//datatable-scroller[@class='datatable-scroll']//datatable-row-wrapper//datatable-body-row[contains(@class,'datatable-body-row active')]");
        console.log(rowClass.getText());
        var DiagnosticsDetails = browser.element("[data-test-id='details']");
        var visibility = DiagnosticsDetails.isVisible();
        assert(visibility,true);        
        console.log("Test Completed - Verify message diagnostics view for rename and extended XYN-1525");

    });
});