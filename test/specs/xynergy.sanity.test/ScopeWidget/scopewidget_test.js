let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });

    it('Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844', function(){
        console.log("Test started - Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        browser.pause(200);
        var checkBox = browser.element(parameter.scopeCheckBox);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            checkBox.click();
        }
        var validationTitle = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility1 = validationTitle.isVisible();
        assert(visibility1, true);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            checkBox.click();
        }
        //browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844");

    });

    it('Data Scope - Verify formatting issue on information for all sections - XYN-2068', function(){
        console.log("Test started - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        var scopeDataFilter = browser.element(parameter.treeNodeDropDown);
        browser.pause(1000);
        var checkBox = browser.element(parameter.scopeCheckBox);
        var scopeFilter = browser.element(parameter.scopeFilter);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilter.click();
        }
        var value = 'datasets';
        scopeFilter.keys(value);
        console.log("Datasets");
        var scopeDataSet = browser.element(parameter.scopeDatasetDropDown);        
        var visibility = scopeDataSet.isEnabled();
        assert(visibility, true);
        var scopeFilterClear = browser.element(parameter.scopeFilterClear);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilterClear.click();
        }
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
    
    });

    it('Verify Data panel styling - XYN-2312', function(){
        console.log("Test Started - Verify Data panel styling - XYN-2312");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }        
        browser.pause(500);
        browser.element("[data-test-id='Datasets']").click();
        var validationToolTip = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility = validationToolTip.isVisible();
        assert(visibility, true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.scopeWidget).click();        
        var validationToolTip1 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility1 = validationToolTip1.isVisible();
        assert(visibility1, true);
        browser.element("[data-test-id='Files']").click();
        browser.element("[data-test-id='Files']").click();    
        var validationToolTip2 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);        
        var checkToolTip = true;
        if(validationToolTip2.isVisible()){
            checkToolTip = false;
        }
        assert(checkToolTip, true);
        browser.element(parameter.incorrectQueryClose).click();       
        console.log("Test Completed - Verify Data panel styling - XYN-2312");        
    
    });

});