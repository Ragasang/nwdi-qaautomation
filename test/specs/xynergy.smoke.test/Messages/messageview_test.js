let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    var init=0;
    var selectedTimeColValue=parameter.MessagesSelectedRow+"//div[2]//datatable-body-cell[2]";
    var selectedTimeColValueInFindGrid=parameter.MessagesFindGridSelectedRowContent+"//div[2]//datatable-body-cell[2]";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }       
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });

    it('Verify whether context menu FIND contains all sub features - XYN-1124', function () {
        console.log("Test started to check sub features of context menu FIND");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);

        var findAllButtonPresence, findAllButtonVisible;
        findAllButtonPresence = browser.element(parameter.FindAllButton).isExisting();
        findAllButtonVisible = browser.element(parameter.FindAllButton).isVisible();

        var findNextButtonPresence, findNextButtonVisible;
        findNextButtonPresence = browser.element(parameter.FindNextButton).isExisting();
        findNextButtonVisible = browser.element(parameter.FindNextButton).isVisible();

        var closeButtonPresence, closeButtonVisible;
        closeButtonPresence = browser.element(parameter.CloseButton).isExisting();
        closeButtonVisible = browser.element(parameter.CloseButton).isVisible();

        var abortButtonPresence, abortButtonVisible;
        abortButtonPresence = browser.element(parameter.AbortButton).isExisting();
        abortButtonVisible = browser.element(parameter.AbortButton).isVisible();
        var priresult = false, totalresult = false;

        if (findAllButtonPresence && findAllButtonVisible && findNextButtonPresence && findNextButtonVisible && closeButtonPresence
            && closeButtonVisible && abortButtonPresence && abortButtonVisible) {
            priresult = true;
        }
        assert.equal(priresult, true);
    });  
    it('To verify whether filter is working accordingly - XYN-1532', function () {
        console.log("Test started to verify filter functionality, XYN-1532");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        rowCount = browser.elements(parameter.MessagesRowContent).value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements(parameter.MessagesRowContent+"[" + j + "]");
            console.log(rowContent.getText());
            expect(rowContent.getText().toLowerCase()).to.include(config.findMessageFilterValue);
            j++;
            rowCount--;
        }
        console.log("Test completed to verify filter functionality, XYN-1532");
    });
});
