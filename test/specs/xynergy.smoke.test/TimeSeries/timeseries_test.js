let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let xynergyconstants=require('../../test_xynergy.constants.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var config = require('../../../config.js')
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Time Series',function(){
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }
        
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });  

    it('verify if events are plotting on time series', function () {
        console.log("Test started- Events plot on time series");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent, parameter.VoiceStartEvent, parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();       
        var eventName = baseMethods.verifyTimeSeriesEventLayerData(xynergyconstants.VoiceStartEvent);
        baseMethods.closeTimeSeriesDataLayer();
        assert.equal(eventName, true);      
        console.log("Test completed - Events plot on time series");
    });
    it('Verify whether graph is showing metric info', function () {
        console.log("Test started- Verify whether graph is showing metric info");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var chartTite=browser.element("//*[@class='highcharts-title']").getText();
        assert.equal(chartTite,xynergyconstants.RSRPMetric);
        console.log("Test completed - Verify whether graph is showing metric info");
    });
});