let baseMethods = require('../testbase.js');
let genericObject = require("../generic_objects.js");
let parameter = require("../parameters.js");
let timeseries= require("../../pageobjects/timeseries.js");
let xynergyconstants = require("../test_xynergy.constants.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
require('../commons/global.js')();

describe('drive-test page', function () {

    beforeEach(function () {
        if (!isElectron()) {
            browser.url('/account/login');
            browser.setValue('input[name="username"]', 'demotest', 100);
            browser.setValue('input[name="password"]', 'Xceed123', 100);
            var submitButton = browser.element("//button[@type='submit']");
            submitButton.click();                                          
            browser.windowHandleMaximize();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('should render RSRP metric on drive route', function () {
        console.log("should render RSRP metric on drive route -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
        assert.equal(layerValue, true);
        console.log("should render RSRP metric on drive route -done");
    });

    it('verify synchronization of data on timeseries,table view and messages widget', function () {
        console.log("verify synchronization of data on timeseries,table view and messages widget");
        browser.windowHandleFullscreen();
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048";
        baseMethods.clickOnAdvancedSearchViewIcon();
        baseMethods.skipSomeDataFiles();        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='v-box-right']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
        baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
        var windowSize=browser.windowHandleSize();
        console.log(windowSize);
        baseMethods.clickOnTimeseriesEventElement(timestamp);

        var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
        browser.pause(500);
        var msgcellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]/div[1]/div[1]").getText();
        var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
        var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
        if (tablecellcontent && msgcellcontent && timeseriestooltipContent == timestamp) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
        console.log("verify synchronization of data on timeseries,table view and messages widget");
    });

    it.only('should render RSRP metric on time series', function () {
        console.log("should render RSRP metric on time series -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.TimeseriesDroppable);
        timeseries.clickOnTimeSeriesLayerBtn();
        var layerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
        assert.equal(layerValue, true);
        console.log("should render RSRP metric on time series -done");
    });

    it('should verify table view', function () {
        console.log("should verify table view -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetRoot);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric, parameter.RSRPMetric, parameter.TableDroppable);
        var value = baseMethods.verifyData();
        assert.equal(value, true);
        console.log("should verify table view -done");
    });

    // it('should verify map bin tooltip data', function () {
    //     this.skip();
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
    //     var count = baseMethods.verifyBinTooltipData();
    //     assert.equal(count, 2);
    // });

    it('Verify import data feature', function () {
        console.log("Verify import data feature");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        browser.pause(200);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData("Drive Route");
        assert.equal(layerValue, true);
        console.log("Verify import data feature");
    });

    it('Verify browse data feature', function () {
        console.log("Verify browse data feature");
        baseMethods.checkForWorkspaceWidget();
        var browseData = browser.element("[data-test-id='Browse Data']");
        browseData.click();
        baseMethods.setStartTime("07/01/2018", "07/08/2019");
        baseMethods.filterBrowseData("voice_sprint-mt");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        browser.pause(200);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyLayerData("Drive Route");
        assert.equal(layerValue, true);
        console.log("Verify browse data feature");
    });
    
    it('should verify plot across multiple widgets on single drag and drop', function(){
        console.log("should verify plot across multiple widgets on single drag and drop -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.WidgetRightBox);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
        baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.PlotAcrossWidgets);
        browser.pause(1000);
        timeseries.clickOnTimeSeriesLayerBtn();
        var timeLayerValue = baseMethods.verifyTimeSeriesMetricLayerData(xynergyconstants.RSRPMetric);
        var timeSeriesLayerBtn=browser.element("[data-test-id='close-button']");
        timeSeriesLayerBtn.click();
        baseMethods.clickOnLayerMenuBtn();
        var metricLayerValue = baseMethods.verifyLayerData(xynergyconstants.RSRPMetric);
        var value = baseMethods.verifyRightMenuHeader("Metric View");
        assert.equal(metricLayerValue && timeLayerValue && value, true);  
        console.log("should verify plot across multiple widgets on single drag and drop -Done");        
    });

    it('verify synchronization of data on timesiries and sequence diagram widgets', function () {
        console.log("verify synchronization of data on timesiries and sequence diagram widgets -Started");
        browser.windowHandleFullscreen();
        var assertValue = false;
        var timestamp = "03/30/2018 18:09:34.048";        
        baseMethods.clickOnAdvancedSearchViewIcon();
        baseMethods.skipSomeDataFiles();        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetBottomBox);
        baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
        baseMethods.clickOnTimeseriesEventElement(timestamp);
        var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
        var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
        browser.pause(500);
        var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
        sequenceRowSelected.click();
        browser.pause(100);
        var sequenceToolTip=browser.element("//div[@class='tooltip-container']");
        var sequenceToolTipContent=sequenceToolTip.getText();
        console.log(sequenceToolTipContent);
        console.log(timeseriestooltipContent);
        if (sequenceToolTipContent && timeseriestooltipContent == timestamp) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
         console.log("verify synchronization of data on timesiries and sequence diagram widgets -Done");
    });

    it('verify synchronization of data on sequence-diagram,table view and messages widget', function () {
        console.log("verify synchronization of data on sequence-diagram,table view and messages widget -Started");
        browser.windowHandleFullscreen();
        var assertValue = false;
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='v-box-right']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetRightBox);
        baseMethods.plotEvent(xynergyconstants.VoiceCoversationStart, parameter.VoiceCoversationStart, parameter.PlotAcrossWidgets);
        var eventtableRow=browser.element("//datatable-row-wrapper[4]");
        eventtableRow.click();      
        var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
        browser.pause(500);
        var msgcellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]/div[1]/div[1]").getText();
        browser.pause(500);
        var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
        sequenceRowSelected.click();
        var sequenceToolTip=browser.element("//div[@class='tooltip-container']").getText();
        if (tablecellcontent && msgcellcontent && sequenceToolTip == tablecellcontent) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
        console.log("verify synchronization of data on sequence-diagram,table view and messages widget -Done");
    });


    //Important 
    // console.log("Current screen resolution is:")
        // var windowSize=browser.windowHandleSize();
        // console.log(windowSize.value);
        // console.log("setViewportSize: W=800 H=600");
        // browser.setViewportSize({width: 800, height: 600});
        // var windowSize=browser.windowHandleSize();
        // console.log(windowSize.value);
        // console.log("windowHandleSize: W=1080 H=1920");
        // browser.windowHandleSize({width: 1920, height: 1080});  
        // var windowSize=browser.windowHandleSize();
        // console.log(windowSize.value);
        // console.log("windowHandleFullscreen");
        // browser.windowHandleFullscreen();
        // var windowSize=browser.windowHandleSize();
        // console.log(windowSize.value);
        // const height=browser.getWindowSize("//div[@class='analytics-view']");
        // var elem=browser.element("//div[@class='analytics-view']");
        // var size=elem.getSize();
        // PS script added in jenkins configuration:
        // set - displayresolution 1920 1080 - Force
        // local window size: W = 1920 H = 1080
        // Server window size: W = 1024  H = 768
        // Prints browser screen resolution:
        // const height = browser.getWindowSize("//div[@class='analytics-view']");
        // var elem = browser.element("//div[@class='analytics-view']");
        // var size = elem.getSize();
        // WDIO config:
        // chromeOptions: {
        //args: ["--window-size=1920,1080"]
        // },
        // browser.pause(100);
        // var okBtn=browser.element("[data-test-id='ok-btn']");
        // okBtn.click();
        // browser.pause(1500);
});
