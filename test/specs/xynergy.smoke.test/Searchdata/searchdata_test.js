let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    afterEach(function () {
        if(isElectron()){
        console.log("Clearing the Search keyword and results");
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();
        console.log("Cleared the Search keyword and results");
        }
        
    });

    it('Verify if user is able to search by FileName XYN-1474', function(){
        console.log("Test started - to verify if user is able to search by Filename XYN-1474");
        //baseMethods.navigateToAdvancedSearchMenu(); 
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        //baseMethods.searchNQLQueryForSingleDevice();
        browser.element(parameter.treeNodeDropDown).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Filename results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j]).to.include(config.searchTextForFIleName);
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        console.log("Test completed - to verify if user is able to search by Filename");
    });

});