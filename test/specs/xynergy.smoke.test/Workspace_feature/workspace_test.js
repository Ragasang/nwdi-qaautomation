let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    afterEach(function () {
        if (isElectron()) {
            console.log("Test Completed");
        }
    });

   
    it('To Verify whether event sync is restoring when user opens saved workspace: XYN-3464' ,function(){
        console.log("Test Started - Verify whether event sync is restoring when user opens saved workspace: XYN-3464");
        var timestamp = "03/30/2018 17:54:51.314";
        var assertvalue = false;
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRightBox);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetBottomBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='h-box-bottom']");
            obj.outerHTML = ''
        });
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='h-box-bottom']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetBottomBox);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(3000);
        browser.element("[data-test-id='widget-max-btn']").click();
        baseMethods.clickOnTimeseriesEventElement(timestamp);
        browser.element("[data-test-id='widget-max-btn']").click();
        browser.pause(1000);
        var msgTimestamp = messageObject.getTimestampforRow(1);
        messageObject.clickMessagesDataRow(1);
        console.log(msgTimestamp + ' is timestamp for Messages');
        var seqTimestamp = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
        console.log(seqTimestamp + ' is timestamp for Seq Diag');
        if(msgTimestamp && seqTimestamp == timestamp) {
            assertvalue = true;
        }
        assert.equal(assertvalue,true);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var msgTimestampVerify = messageObject.getTimestampforRow(1);
        console.log(msgTimestampVerify);
        var seqTimestampVerify = browser.elements("//xyn-sequence-item[1]//div//span[@data-test-id='timestamp-text']").getText();
        console.log(seqTimestampVerify);
        // // var i = 1;
        // // seqRowCount2 = browser.elements(parameter.seqRows).value.length;
        // // //console.log(seqRowCount);
        // // while (seqRowCount2 > 0) {
        // //     var row = browser.elements("//xyn-sequence-item[" + i + "]//div//div[@data-test-id='list-line-center']");
        // //     if((row.getAttribute("class")).includes('selected')){
        // //         var seqTimestampVerify = browser.elements("//xyn-sequence-item[" + i + "]//div//span[@data-test-id='timestamp-text']").getText();
        // //         console.log(seqTimestampVerify);
        // //     }
        // //     i++;
        // //     seqRowCount2--;
        // // }
        if(msgTimestampVerify && seqTimestampVerify == timestamp) {
            assertvalue2 = true;
        }
        assert.equal(assertvalue2,true);
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        console.log("Test Completed - Verified whether event sync is restoring when user opens saved workspace: XYN-3464");

    });

    it('To Verify Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393' ,function(){
        console.log("Test Started - Verify Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        // searchdataObject.selectdatasetresult();
        // browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findAllMessageFilterValue);
        //browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        messageObject.clickMessagesDataRow(1);
        //baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.TableDroppable);
        browser.element(parameter.hamburgerMenu).click();
        browser.pause(500);
        browser.element(parameter.fileName).click();
        browser.element(parameter.hamburgerMenu).click();
        messageObject.clickDecodeMessageIcon();
        //browser.moveToObject("[data-test-id='1']");
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var filtervalue = browser.element('input[data-test-id="msg-filter"]').getValue();
        console.log(filtervalue);
        expect(filtervalue).to.include(config.findAllMessageFilterValue);
        browser.element(parameter.hamburgerMenu).click();
        //browser.pause(2000);
        var filenamechckbx = browser.element(parameter.fileName).isEnabled();
        console.log(filenamechckbx);
        assert(filenamechckbx,true);
        browser.element(parameter.hamburgerMenu).click();
        var decodebtn = browser.element("[data-test-id='decode-msg-btn']");
        expect(decodebtn.getAttribute("class")).to.include('active-icon');
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        //searchdataObject.newSearchPage();
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        console.log("Test Completed - Verified Workspace - Messages View state to remember Filter, visible/hidden columns and if decode window is opened: XYN-1393");

    });

    

});