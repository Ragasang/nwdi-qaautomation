let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });
    afterEach(function () {
        if (isElectron()) {
            console.log("closing all open views");
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            browser.pause(500);
            console.log("closing all open views");
            var closeAllViews = browser.element("[data-test-id='close-all-btn']");
            if (closeAllViews.isVisible()) {
                closeAllViews.click();
                browser.pause(300);
                console.log("clicking on close btn");
                //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
                var modalBox = browser.element("//modal-box");
                var mBoxCloseBtn = modalBox.element("[data-test-id='close-all-views']");
                if (mBoxCloseBtn.isVisible()) {
                    mBoxCloseBtn.click();
                    console.log("clicked on close btn");
                }
                browser.pause(300);
                //close views menu
                var viewsHeader = browser.element("//views-menu");
                var closeViewsMenu = viewsHeader.element("[data-test-id='close-button']");
                if (closeViewsMenu.isVisible()) {
                    closeViewsMenu.click();
                }
            }
        }

    });

    it('Verify of two lanes in sequence diagram- XYN-1710', function () {
        console.log("Test started- verification of two lanes in the sequence diagram XYN-1710");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        var sequenceBlock = browser.elements(parameter.seqRows);
        var headerBlock = sequenceBlock.elements(parameter.seqHeaderBlock);
        var leftItem = headerBlock.element(parameter.seqHeaderLineLeft);
        expect(leftItem.getText()).to.include(config.sequenceLeftLaneItemName);
        var rightItem = headerBlock.element(parameter.seqHeaderLineRight);
        expect(rightItem.getText()).to.include(config.sequenceRightLaneItemName);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- verification of two lanes in the sequence diagram XYN-1710");
    });

    it('Verify whether RRC messages are present on Sequence diagram, XYN-2777', function () {
        console.log("Test started to verify RRC connection message");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        // var sequenceBlock = browser.elements("//xyn-sequence-item");
        var rowMessage = browser.elements(parameter.rowMessage);
        var Message = rowMessage.value;
        var value = false;
        Message.forEach(element => {
            var messageValue = element.getText();
            if (messageValue == config.rrcConnection) {
                value = true;
            }
        })
        assert.equal(value, true);
        console.log("Test completed to verify RRC connection message-XYN-2777");
    });
}); 