var tableViewpage = function(){
    
    this.enterTableFilterValue=function(value){
        browser.setValue('input[data-test-id="table-view-filter"]',value,100);
        browser.pause(1000);
    };
    this.clickHamburgermenu = function () {
        var menu = browser.$(".datatable-header-cell.col-selector");
        menu.click();
    };
    this.clickFullScreenIcon=function(){
        browser.element('div[class="widget-max flex-centered"]').click();
    };
    this.getfileElement=function(value){
        return browser.element("//div[@class='datatable-body-cell-label']//span[@title='" + value +"']").getText();
    };
    this.legendOpenButton=function(){
        browser.element("[data-test-id='IconUniqueDataIdLegend']").click();
        browser.pause(300);
    };
    this.closeButton=function(){
        browser.element("[data-test-id='close-button']").click();
        browser.pause(300);
    };
    this.enterTableFilterValue=function(value){
        browser.setValue('input[data-test-id="grid-filter-input"]',value,100);
        browser.pause(1000);
    };
    this.getEventElement=function(value){
        return browser.element("//app-header-grid//datatable-row-wrapper[" + value +"]//datatable-body-cell[3]//span").getText();
    };
};
module.exports=new tableViewpage();