let parameter = require('../specs/parameters.js');
let baseMethods = require('../specs/testbase');
let config = require('../config');
var expect = require('chai').expect;
var messagepage = function(){
    //findAllButton
    this.enterMessageFilterValue=function(value){
        var msgFilter=browser.element('input[data-test-id="msg-filter"]');
        msgFilter.click();
        msgFilter.keys(value);
        console.log("test");
        //browser.setValue('input[data-test-id="msg-filter"]',value,100);
        browser.pause(1000);
    };
    this.enterValueInFindFilter=function(value){
        var findFilter=browser.element('input[data-test-id="input-find-msg"]');
        findFilter.click();
        findFilter.keys(value);
        //browser.setValue('input[data-test-id="input-find-msg"]',value,100);
        browser.pause(500);
    };

    this.selectdropdown=function(value){
        browser.setValue('select[data-test-id="select-column"]',value,100);
    };
   
    this.removeFindFilterText=function(){
        browser.element('i[data-test-id="cancel-input"]').click();
    };
    this.clickDecodeMessageIcon=function(){
        var decodemsgBtn=browser.element("[data-test-id='decode-msg-btn']");
        decodemsgBtn.click();
    };

    /*
    this.enterValueInSearchFilter=function(value)
    {
        browser.enterValueInSearchFilter('input[data-test-id="msg-filter"]',value,100);
    }
    */

    

    this.clickFindall=function(){
        browser.element('button[data-test-id="findAllButton"]').click();
        browser.pause(1000);
    }

    this.clickFindnext=function(){
        browser.element('button[data-test-id="findNextButton"]').click();
        browser.pause(1000);
    }

    this.clickClose=function(){
        browser.element('button[data-test-id="closeButton"]').click();
    }
    this.clickHamburger=function(){
        browser.element('button[data-test-id="1menu-btn"]').click();
    }
    
    this.clickAbort=function(){
        browser.element('button[data-test-id="cancelButton"]').click();
    }    
    this.clickFullScreenIcon=function(){
        browser.element('div[data-test-id="widget-max-btn"]').click();
    }
    this.getFullScreenIconCount=function(){
        return browser.element('div[data-test-id="widget-max-btn"]').value.length;
    }
    this.clickHamburgerFind=function(){
        browser.element('a[data-test-id="find-msg-btn"]').click();
    }  
    this.waitForEnabled=function(){
        browser.waitForVisible('[data-test-id="findAllButton"]');
    }
    this.clickMessagesDataRow=function(j){
        var newEle = "//analytics-view//ngx-datatable//datatable-row-wrapper[" + j + "]//datatable-body-cell[2]//span";
        var decodemsgBtn=browser.element(newEle);
        decodemsgBtn.click();
    };
    this.getTimestampforRow=function(j){
        var timestamp = browser.element("//analytics-view//ngx-datatable//datatable-row-wrapper[" + j + "]//datatable-body-cell[2]//span").getText();
        return timestamp;
    };

    this.messagesRowContentAttribute=function(protocolName, protocolColorCode){
        var msgFilter=browser.element('input[data-test-id="msg-filter"]');
        msgFilter.click();
        msgFilter.keys(protocolName);        
        browser.pause(1000);
        baseMethods.verifySpinnerVisibility();
        var noDatamsg = browser.element(parameter.noData).isVisible();
        if (noDatamsg) {
            return false;
        }
        else{
            var j = 1;
            var count = browser.elements(parameter.messagesTableRows).value.length;
            while (count >= j) {               
                var msgContentResult = baseMethods.messagesRowContent(j);
                expect(msgContentResult.toLowerCase()).to.include(protocolName);
                var styleResult = baseMethods.messagesgetAttribute(parameter.msgColumnInFilter, config.style);
                expect(styleResult).to.include(protocolColorCode);
                j++;            
            }
            return true;
        }        
    };
    this.clearFilterContent=function(){
        browser.element('[data-test-id="msg-filter-cancel-btn"]').click();
        baseMethods.verifySpinnerVisibility();        
    };
};
module.exports=new messagepage();