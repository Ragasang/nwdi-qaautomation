let baseMethods = require('../specs/testbase.js');
let parameter = require('../specs/parameters.js');
var config = require('../config.js');
var expect = require('chai').expect;
var searchdatapage = function(){
    this.enterSaveSearchResultNameValue=function(value){
        //browser.setValue('input[data-test-id="SavedNameinput"]',value,100);
        //browser.pause(1000);
        var textbox = browser.element("[data-test-id='SavedNameinput']");
        textbox.click();
        textbox.keys(value);
        // return nametext;
    };

    this.selectdatasetresult=function(){
        browser.waitForVisible("[data-test-id='listItem']");
        browser.element('[data-test-id="listItem"]').click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
    };

    this.clickSaveasbutton=function(){
        //var Saveas = browser.element("//bottom-pane//footer-buttons//div[@class='action-button'][1]");
        var Saveas = browser.element("//button[@class='action-button']");
        Saveas.click();
    };

    this.selectsavesearchresultsas=function(){
        browser.element('[data-test-id="saveDataset"]').click();
    }

    this.clearsearchkeyword=function(value){
        var clearBtn=browser.element('[data-test-id="search-cancel-btn"]');
        clearBtn.click();
        browser.pause(100);
    };

    this.clickOpeninCanvasbutton=function(){
        //var Saveas = browser.element("//bottom-pane//footer-buttons//div[@class='action-button'][1]");
        var Open = browser.element("[data-test-id='openInCanvasbtn']");
        Open.click();
    };

    this.enterSaveWorkspaceNameValue=function(value){
        var textbox = browser.element("[data-test-id='ws-name-input']");
        textbox.click();
        textbox.keys(value);
    };

    this.clickCancelinOpenSavedSearch=function(){
        var Cancel = browser.element("[data-test-id='OpenSavedSrchCancel']");
        Cancel.click();
    };

    this.clickOpeninOpenSavedSearch=function(){
        var Open = browser.element("[data-test-id='OpenSavedSrchBtn']");
        Open.click();
    };

    this.selectsavesearchteamsas=function(){
        browser.element('[data-test-id="saveResultBtn"]').click();
    };

    this.enterSaveSearchTermsResultNameValue=function(value){        
        var textbox = browser.element("[data-test-id='SavedNameinput']");
        textbox.click();
        textbox.keys(value);
        // return nametext;
    };

    
    this.ClickPolygon=function(){
    browser.element("[data-test-id='IconUniqueDataIdpolygon']").click();
    };

    this.ClickCircle=function(){
    browser.element("[data-test-id='IconUniqueDataIdcircle']").click();
    };

    this.ClickEdit=function(){
    browser.element("[data-test-id='IconUniqueDataIdpen']").click();
    };
    
    this.EnterWeekCount=function(value){
    var textbox = browser.element("[data-test-id='units-input']");
    textbox.click();
    textbox.keys(value);
};
    this.searchDataFilterButton=function(){
        browser.element('[data-test-id="filterButton"]').click();
    };

    this.clickDataTabButton=function(){
        var Data = browser.element("[data-test-id='Data']");
        Data.click();
    };
    this.DataSetSaveCancelBtn=function(){
        var Data = browser.element("[data-test-id='DataSetSaveCancelBtn']");
        Data.click();
        browser.pause(100);
    };

    this.newSearchPage=function(){
        browser.element("[data-test-id='Open Search Terms']").click();
        browser.pause(1000);
        noSearchTermsCancel = browser.element("[data-test-id='cancel-btn']");
        searchTermsCancel = browser.element("[data-test-id='OpenSavedSrchCancel']");
        if(noSearchTermsCancel.isVisible()){
            noSearchTermsCancel.click();
            browser.pause(400);
        }
        else if(searchTermsCancel.isVisible()){
            searchTermsCancel.click();
        }
        browser.element("[data-test-id='New Search']").click();
    };

    this.lessthanequalto=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            value1= parseInt(value[0]);
            console.log(value1);
            console.log(compare);
            if (value1 < compare){
                console.log(value1 + " is less than " + compare);
                result = true;
            }
            else if(value1 = compare){
                console.log(value1 + " is equal to " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };

    this.equalto=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            value1= parseInt(value[0]);
            console.log(value1);
            console.log(compare);
            if(value1 = compare){
                console.log(value1 + " is equal to " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };


    this.lessthan=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            value1= parseInt(value[0]);
            console.log(value1);
            console.log(compare);
            if (value1 < compare){
                console.log(value1 + " is less than " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };

    this.greaterthanequalto=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            value1= parseInt(value[0]);
            console.log(value1);
            console.log(compare);
            if (value1 > compare){
                console.log(value1 + " is greater than " + compare);
                result = true;
            }
            else if(value1 = compare){
                console.log(value1 + " is equal to " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };


    this.greaterthan=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            browser.pause(300);
            value1= parseInt(value[0]);
            browser.pause(300);
            console.log(value1);
            console.log(compare);
            if (value1 > compare){
                console.log(value1 + " is greater than " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };

    this.notequalto=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = parseInt(config.searchTextForChannel);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            browser.pause(300);
            value1= parseInt(value[0]);
            browser.pause(300);
            console.log(value1);
            console.log(compare);
            if (value1 != compare){
                console.log(value1 + " is not equal to " + compare);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };

    this.inconditions=function(){
        var listItem = browser.elements(parameter.listItem).getText();
        //console.log(listItem);
        var result = true;
        var compare = [config.searchTextForChannelIn];
        var compare1 = compare[0].split(",");
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Channel results count is " + listItemlength);
        var i = 0;      
        var value1=0;
        while (listItemlength > i) {
            var value = listItem[i].split(" ");
            value1= parseInt(value[0]);
            console.log(value1);
            console.log(compare[i]);
            if (value1 = parseInt(compare1[i])) {
                console.log(value1 + " is in " + compare1[i]);
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
                break;
            }
            i++;
        }
        return result;
    };




};


module.exports=new searchdatapage();