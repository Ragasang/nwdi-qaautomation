let baseMethods = require('../specs/testbase.js');
var scopewidgetpage = function(){

    this.scopeFilter = function (scope) {

        var clearScopeFilter=browser.element("[data-test-id='icon-cancel icon-small-size input-clear']");
        if(clearScopeFilter.isVisible()){
            clearScopeFilter.click();
        }
        var scopeTab = browser.element("[data-test-id='Data-search-box']");
        scopeTab.setValue(scope);
        browser.pause(400);    
    }

};

module.exports=new scopewidgetpage();