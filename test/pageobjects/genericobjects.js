var dragAndDrop = require('html-dnd').codeForSelectors;
let genericObject = function(){

    this.dragAndDropViews=function(draggable,droppable)
    {
        browser.execute(dragAndDrop,draggable,droppable);
        browser.pause(1000);
    }

    this.clickOnDTTab=function(DTTab){
        browser.element(DTTab).click();
        browser.pause(1000);
    }
    
    this.setEventName=function(event_name){
        var clearEventBtn=browser.element("[data-test-id='clear-event-name']");
        if(clearEventBtn.isVisible()){
            clearEventBtn.click();
        }
        var eventTab = browser.element("[data-test-id='Events-search-box']");
        eventTab.click();
        eventTab.setValue(event_name);
        browser.pause(400);
    }

    this.setMetricName=function(metric_name){
        var clearMetricBtn=browser.element("[data-test-id='clear-metric-name']");
        if(clearMetricBtn.isVisible()){
            clearMetricBtn.click();
        }
        var metricsTab = browser.element("[data-test-id='Metrics-search-box']");
        metricsTab.setValue(metric_name);
        browser.pause(400);
    }
    this.tempdragAndDropViews=function(draggable,droppable)
    {
        browser.execute(dragAndDrop,draggable,droppable);
    }
    this.clearsearchkeyword=function(){
        var clearBtn=browser.element('[data-test-id="search-cancel-btn"]');
        if(clearBtn.isVisible()){
        clearBtn.click();
        }
        browser.pause(100);
    }

    this.clickOnDataMenu=function(){
        browser.element("[data-test-id='Data']").click();
    }
    this.clickOnDatasetReplaceButton=function(){
        browser.element("[data-test-id='Datascope-Replace-button']").click();
    }
    this.clickOnReplaceOkWithoutSaving=function(){
        browser.element("[data-test-id='Datascope-ReplaceOK-button']").click();
    }
    this.clickOnReplaceWithSavingCurrentWork=function(){
        browser.element("[data-test-id='Datascope-save-button']").click();
    }
    this.clickOnWSSaveButton=function(){
        browser.element("[data-test-id='ws-savebtn']").click();
    }
    this.clickOnScopeInAV=function(){
        browser.element("[data-test-id='Scope']").click();
    }
    this.clearPreviousValueFromField=function(val){
        browser.element(val).value='';
        browser.keys('Enter');
        browser.pause(400);
    };
    this.clickOnCloseAllViews=function(){
        var closeAllViews = browser.element("[data-test-id='close-all-btn']");
        if (closeAllViews.isVisible()) {
            closeAllViews.click();
            browser.pause(300);
        }
    };
    this.clickOnCancelOnCloseAllViewsPopUp=function(){
        var modalBox = browser.element("//modal-box");
        var mBoxCloseBtn = modalBox.element("[data-test-id='cancel-btn']");
        if (mBoxCloseBtn.isVisible()) {
            mBoxCloseBtn.click();
        }
    };
    this.clickOnCrossOnViewsTabToCloseTheTab=function(){
        browser.element("//views-menu//*[@data-test-id='close-button']").click();
    };
    this.clickOnViewsSaveAsBtn = function () {
        var saveAsBtn = browser.element("//views-menu//*[@data-test-id='save-as-btn']");
        saveAsBtn.click();
        browser.pause(200);
    };
    this.inputWorkSpaceName=function(value){
        var inputElem=browser.element("//*[@data-test-id='ws-name-input']");
        inputElem.click();
        inputElem.keys(value);
        browser.pause(200);
    }
    this.clickOnViewsWorkSpaceSave=function(){
        var elem=browser.element("//*[@data-test-id='ws-savebtn']");
        browser.pause(200);
        //browser.waitUntil(!elem.getAttribute("class").includes("disabled"),5000,"element is disabled",200);
        elem.click();
    }
    this.isOpenDataModalBoxDisplayed=function(){
        var mBox=browser.element("[data-test-id='Open Data-Title-Bar']");
        if(mBox.isVisible()){
            return true;
        }
        else{
            return false;
        }
    }

    this.dragElement=function dragElement( element, direction)
    {
        var   md; // remember mouse down info
        const first  = browser.elements("//l3-pane/div[@class='message-view']/app-split-view/div[@class='split-view']/as-split[@class='as-vertical as-init']/div[@class='as-split-gutter']/div[1]");
        //const second = document.getElementById("second");
        
        element.onmousedown = onMouseDown;
        
        function onMouseDown( e )
        {
        //console.log("mouse down: " + e.clientX);
        // md = {e,
        //       offsetLeft:  element.offsetLeft,
        //       offsetTop:   element.offsetTop,
        //       firstWidth:  '30%',
        //       };
        document.onmousemove = onMouseMove;
        document.onmouseup = () => { 
            //console.log("mouse up");
            document.onmousemove = document.onmouseup = null;
        }
        }
        
        function onMouseMove( e )
        {
        //console.log("mouse move: " + e.clientX);
        var delta = {x: e.clientX - md.e.x,
                 y: e.clientY - md.e.y};
        
        if (direction === "V" ) // Horizontal
        {
            // prevent negative-sized elements
            // delta.x = Math.min(Math.max(delta.x, -md.firstWidth)
            //            );
            
            element.style.top = "1900px";
            first.style.width = "1708px";
            
        }
        }
    }
    
    
   
};
module.exports=new genericObject();