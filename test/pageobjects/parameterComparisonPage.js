var parameterComparisonpage = function(){
    this.selectCheckBox=function(){
        var sCB = browser.element("//datatable-body-cell[@class='datatable-body-cell vertical-aligned sort-active']//span[@class='checkmark-left']");
        sCB.click();
        browser.pause(300);
    };
    this.clickOKBtn=function(){
        var okBtn = browser.element("[dat-test-id='ok-btn']");
        okBtn.click();
        browser.pause(300);
    };
    this.clickCheckBox=function(val){                
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]//label");
        sCB.click();
        browser.pause(300);
    };    
    this.getFValues=function(j, val){        
        var gval = browser.element("//datatable-row-wrapper[" + j + "]//datatable-body-row//div[2]//datatable-body-cell[4]//div//span").getText();
        console.log(gval);
        var findval = gval.includes(val);
        return findval;     
    };
    this.getSValues=function(j, val){        
        var gval = browser.element("//datatable-row-wrapper[" + j + "]//datatable-body-row//div[2]//datatable-body-cell[5]//div//span").getText();
        console.log(gval);
        var findval = gval.includes(val);
        return findval;    
    };
    this.getTValues=function(j, val){        
        var gval = browser.element("//datatable-row-wrapper[" + j + "]//datatable-body-row//div[2]//datatable-body-cell[6]//div//span").getText();
        console.log(gval);
        var findval = gval.includes(val);
        return findval;    
    };
    this.getFtValues=function(j, val){        
        var gval = browser.element("//datatable-row-wrapper[" + j + "]//datatable-body-row//div[2]//datatable-body-cell[7]//div//span").getText();
        console.log(gval);
        var findval = gval.includes(val);
        return findval;     
    };
    this.enterFilterValue=function(value){
        browser.setValue('//app-comparison-view//input[@type="search"]',value,100);
        browser.pause(2000);//wait time needed
    };
    this.getFilterValues=function(val){        
        var gval = browser.element("//datatable-row-wrapper[1]//datatable-body-row//div[2]").getText();
        console.log(gval);
        var findval = gval.includes(val);
        return findval;     
    };
    this.clearFilter=function(){        
        browser.element("//i[@class='icon-cancel input-clear']").click();
        browser.pause(1000);   
    };
    this.clickOnFilter=function(){
        browser.element("//input[@class='input-container main-filter']").click();
        browser.pause(300);
    };
    this.selectAllCheckBox=function(){
        var sCB = browser.element("//app-data-grid[@name='cv-cells-list']//ngx-datatable//datatable-header//datatable-header-cell[1]//label");
        sCB.click();
        browser.pause(300);
    };
    this.clickSelectCellsTab=function(){
        var sCB = browser.element("//app-comparison-view/div/div[1]/div[1]/div[1]/span");
        sCB.click();
        browser.pause(300);
    };
    this.technologyType=function(val){
        var techType = browser.element("//app-data-grid[@name='cv-cells-list']//ngx-datatable//datatable-body//datatable-row-wrapper[" + val + "]/datatable-body-row/div[2]/datatable-body-cell[3]").getText();
        return techType;       
    };
    this.headerItems=function(val){
        var hItemVal = browser.element(val).getText();
        return hItemVal;       
    };
    this.clickSelectBaselineTab=function(){
        var sCB = browser.element("//app-comparison-view/div/div[1]/div[1]/div[2]");
        sCB.click();
        browser.pause(300);
    };
    this.selectRdBtnBaseline=function(val){
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]//datatable-body-cell");
        sCB.click();
    };
    this.clickOKBtnBaseline=function(){
        var okBtn = browser.element("[data-test-id='ok-btn']");
        okBtn.click();
        browser.pause(300);
    };  
    this.getBaselineCellName=function(val){
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]//datatable-body-cell[2]").getText();
        return sCB;
    };
    this.getRdBtnStatus=function(val){
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]//datatable-body-cell[1]//span").getText();
        return sCB;
    };
    this.getRowVal=function(val){
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]").getText();
        return sCB;
    };
    this.unSelectCheckbox=function(val){
        var sCB = browser.element("//modal-box//app-data-grid//datatable-row-wrapper[" + val + "]//label").click();
        browser.pause(300);
    };
    this.getCellName=function(val){
        var sCN = browser.element("//div[@class='cv-modal']//datatable-row-wrapper[" + val + "]//datatable-body-cell[2]//span").getText();
        return sCN;
    };
    this.getFile=function(val){
        var gF = browser.element("//div[@class='cv-modal']//datatable-row-wrapper[" + val + "]//datatable-body-cell[4]//span").getText();
        return gF;
    };
    this.getMainbodyFilterValues=function(val){        
        var mainFval = browser.element("//div[@class='data-container']//ngx-datatable//datatable-row-wrapper[" + val + "]//div[@class='datatable-row-center datatable-row-group']").getText();
        return mainFval;     
    };
    this.clickViewingOptionsMenu=function(){
        var clickVOM = browser.element("//app-comparison-view//div[@class='img-container hover-effect flex-centered settings-menu']");
        if (clickVOM.isVisible()){
            clickVOM.click();
        }
    };
    this.searchInSelectCells=function(value){
        var filter = browser.element("//div[@class='mbox-body']//input[@type='search']");
            filter.click();
            filter.keys(value);
            browser.pause(2000);
    }
    this.getMainHeaderFileNameValue=function(value){
        var filename = browser.element("//div[@class='datatable-row-center']/datatable-header-cell[" + value +"]/div/div[2]/span").getText();
        return filename;
    }
};
module.exports=new parameterComparisonpage();