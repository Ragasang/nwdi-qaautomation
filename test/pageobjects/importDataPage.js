require('../specs/commons/global.js')();
var parameter=require('../specs/parameters');
var importDatapage = function(){
    var importScreen=browser.elements('import-data-files');
    var dataGrid=importScreen.elements('[data-test-id="data-grid-container"]');
    var importProgress=browser.elements('import-data-progress-overview');
    //findAllButton
    this.enterDataSetName=function(value){
        var dataSetName=browser.element('input[placeholder="Enter Dataset Name..."]');
        dataSetName.click();
        dataSetName.keys(value);
        browser.keys('Enter');
        browser.pause(1000);
    };
    this.clearPreviousValueFromField=function(val){
        // var dataSetName=browser.element('input[placeholder="Enter Dataset Name..."]');
        // dataSetName.click();
        browser.element(val).value='';
        browser.keys('Enter');
        browser.pause(1000);
    };
    this.clickSelectFilesFromComputer=function(){
        var selectFiles=browser.element('[data-test-id="select-file-option"]');
        selectFiles.click();
    };
    this.clickImportDataMenu=function(){
        var importData=browser.element("//div[@data-test-id='Import Data']");
        if(!(importData.getAttribute("class")).includes('selected')){
            importData.click();
        }
    };
    this.waitForProgressPage=function(){
        browser.waitForExist("//import-data-progress");
    }
    this.clickImportAddDataMenu=function(){
        var importAddData=browser.element('[data-test-id="Add Data"]');
        if(!(importAddData.getAttribute("class")).includes('selected')){
            importAddData.click();
        }
    };
    this.clickImportBtn=function(){
        browser.pause(300);
        var importBtn=browser.element('//button[@name="wiz-rightBtn"]');
        //console.log(importBtn.getAttribute('class'));
        // if(!importBtn.getAttribute("class").includes("disabled")){
        //     console.log("Enabled");
        importBtn.click();
        //}
    };
    this.clickHistoryMenu=function(){
        browser.pause(300);
        var menu=browser.element('[data-test-id="History"]');
        //console.log(importBtn.getAttribute('class'));
        // if(!importBtn.getAttribute("class").includes("disabled")){
        //     console.log("Enabled");
        menu.click();
        //}
    };
    this.clickDeleteSelectedFile=function(){
        var deleteFile=dataGrid.element('[class="icon-cancel icon-small-size"]');
        deleteFile.click();
        browser.pause(300);
    };
    this.browseFile=function(path){
        var addFilesHeader=browser.elements("//div[@class='button-item margin-offset']");//button-item margin-offset
        addFilesHeader.chooseFile('//input[@type="file"]',path);
    };
    this.getProgressDataSet=function(value){
        var progress=browser.elements("//div[@class='import-progress-overview']");
        var progressCount=progress.value.length;
        console.log(progressCount);
        var i=1;
        // while(progressCount>0){
            var dataProgress=browser.elements("//import-data-progress-overview//div[" +i+ "]");
            var eachElement=dataProgress.elements("//div[@class='import-progress-overview']");
            var eachElement1=browser.elements("//div[@class='import-progress-overview']["+i+"]");
            var dataSetNameElement=browser.elements("//*[contains(text(),'"+value+"')]");
            console.log(dataSetNameElement.getText());
            console.log(eachElement.getText());
            console.log(eachElement1.getText());
            console.log(dataSetNameElement.value.length);
            while(dataSetNameElement.value.length>0){
                dataSetNameElement=browser.elements("//span[contains(text(),'"+value+"')]");
                //console.log("uploading"+i);
                //console.log(dataSetNameElement.value.length);
                i++;
            }
            browser.pause(500);
    };
    this.getRowCount=function(value1,value2){
        var elem=browser.elements(value1);
        return elem.elements(value2).value.length;
    };
    this.getDataSetCount=function(value1,value2,value3){
        var elem=browser.elements(value1);
        var rows=elem.elements(value2);
        var i=1,count=0;
        console.log("datasetcount");
        if(rows.value.length>=0){
            while(i<=rows.value.length){
                console.log(browser.element("//app-data-grid[@name='history-datasets']//datatable-row-wrapper["+i+"]").getText());
                var dataSetValue=browser.element("//app-data-grid[@name='history-datasets']//datatable-row-wrapper["+i+"]//datatable-body-cell[4]").getText();
                console.log(value3);
                if(dataSetValue.includes(value3)){
                    count++;
                    break;
                }
            }
        }
        return count;
    };
    this.getNumberOfDatasetsUploaded=function(value){
        this.clickHistoryMenu();       
        this.enterValueInHistoryFilter(value);
        browser.pause(300);
        var elem=browser.elements(parameter.historyDataGrid);
        var row=elem.element("//datatable-row-wrapper//datatable-body-cell[5]").getText();
        return row;
    };
    this.clickOnDataSetRecord=function(){
        browser.element(parameter.gridRows).click();
    }
    this.getAppendMessage=function(){
        var appendMessage=importScreen.elements("//div[@class='ds-info-message flex-centered']");
        console.log(appendMessage.getText());//button-item margin-offset
        return "abc";
    };
    this.isImportButtonEnabled=function(){
        browser.pause(300);
        var importBtn=browser.element('//button[@name="wiz-rightBtn"]');
        if(!importBtn.getAttribute("class").includes("disabled")){
            console.log("Enabled");
            return true;
        }
        else{
            return false;
        }
    };
    this.enterValueInHistoryFilter=function(value){
        browser.pause(200);
        var elem=browser.element("[data-test-id='history-filter-box']");
        this.clearHistoryFilter();
        elem.click();
        elem.keys(value);
        browser.keys('Enter');
        browser.pause(400);
    };
    this.clickOnOpenBtnFromHistoryOverviewPage=function(){
        browser.element("//div[@class='import-data-nav-container']//*[contains(@class,'open')]").click();
    }
    this.clearHistoryFilter=function(){
        var elem=browser.elements("//*[@data-test-id='history-filter-box']"+parameter.historyFilterClear);
        console.log(elem.value+"count");
        if(elem.value.length>0){
            elem.click();
        }
    }
    this.getDataSetValue=function(){
        var x;
        browser.execute(()=>{
            x=document.querySelector("input[class='files-searchbox ng-valid ng-dirty ng-touched']");
            console.log(x.value);
        });
        return x.value;
    }
    this.verifySelectedFile=function(value){
        var isFilePresent=browser.element("//*[contains(@title,'"+value+"')]");
        return isFilePresent.isExisting();
    }
};
module.exports=new importDatapage();