let parameter=require("../specs/parameters.js")
var timeseries = function(){
    this.enterFieldValue=function(value){
        browser.setValue('input[name="username"]',value,100);
    };
    this.getTitleText=function(){
        return browser.element('span[class="app-logo-title"]').getText();
    };
    this.clickContinue=function(){
        //element(by.buttonText('Continue')).click();
        browser.element("//button[@type='submit']").click();
    };     
    
    this.clickOnTimeSeriesLayerBtn = function () {
       browser.element(parameter.TimeseriesLayerButton).click();
       browser.pause(200);
    }

    this.clickFullScreenIcon=function(){
        browser.element('div[class="widget-max flex-centered"]').click();
    }
    this.deleteAllLayers=function(){
        browser.element("//div[@class='layer-info']//i[@data-test-id='close-btn']").click();
        browser.pause(200);
    }
    this.verifyPlottedEventData=function(value){
        var tabName=browser.elements("//tab[@name='Plotted Data Layers']");
        var expandEvents=tabName.elements("//*[@class='tree-node-level-2 tree-node tree-node-collapsed']");
        if(expandEvents.isExisting()){
            expandEvents.element("//span[@class='toggle-children-wrapper toggle-children-wrapper-collapsed']//span").click();
        }
        var plottedEventData=tabName.elements("//tab[@name='Events']//*[@data-test-id='TimeSeriesPlottedEventData']");
        var i=1;
        var state=false;
        while(plottedEventData.value.length>=i){
            var elem=browser.element("//tab[@name='Events']//*[@class='content-wrapper']["+i+"]//*[@data-test-id='TimeSeriesPlottedEventData']");
            console.log(elem.getText());
            if(elem.getText().includes(value)){
                state=true;
                break;
            }
            i++;
        }
        return state;
    }
    this.verifyPlottedMetricData=function(value){
        var tabName=browser.elements("//tab[@name='Plotted Data Layers']");
        var elem="//tree-node-children//*[@class='layer-info']//*[@data-test-id='TimeSeriesPlottedData']";
        var plottedMetricData=tabName.elements(elem);
        var i=1;
        var state=false;
        while(plottedMetricData.value.length>=i){
            var elem=browser.element(elem+"["+i+"]");
            console.log(elem.getText());
            if(elem.getText().includes(value)){
                state=true;
                break;
            }
            i++;
        }
        return state;
    }
};
module.exports=new timeseries();