require('../specs/commons/global.js')();
let baseMethods = require('../specs/testbase.js');
var path = require('path');
var config = require('../config.js');
let importDataObject = require("../pageobjects/importDataPage");

var browseDataPage = function(){
    
    this.LastFilters=function(value){
        browser.setValue('input[class="input-tp"]', value, 100);        
    }
    this.clickApplyBtn=function(){
        browser.element('button[data-test-id="apply-btn"]').click();
    }
    this.clickLastUpBtn=function(){
        browser.element('[data-test-id="up-btn"]').click();
    }
    this.clickLastDownBtn=function(){
        browser.element('[data-test-id="down-btn"]').click();
    }
    this.LastdropdownDays=function(){
        var DaysVal = browser.element('//select[@data-test-id="select-tp"]/option[1]').click();
    }
    this.LastdropdownWeeks=function(){
        var WeeksVal = browser.element('//select[@data-test-id="select-tp"]/option[2]').click();
    }
    this.TimeFilters=function(){
        var WeeksVal = browser.element('//span[@data-test-id="starttime-radio-btn"]').click();
    }
    this.ClearSTimeBtn=function(){
        var WeeksVal = browser.element('//div[@class="data-filter"][1]//button[@aria-label="Clear Date"]').click();
    }
    this.ClearETimeBtn=function(){
        var WeeksVal = browser.element('//div[@class="data-filter"][2]//button[@aria-label="Clear Date"]').click();
    }      
    this.setTime_1_2=function(time_1, time_2){
        browser.element('//div[@class="control-group"][2]//div[@class="data-filter"][1]/input[@class="input-box"]').click();
        browser.setValue('//div[@class="control-group"][2]//div[@class="data-filter"][1]/input[@class="input-box"]', time_1, 500);    
        browser.pause(300);
        browser.element('//div[@class="control-group"][2]//div[@class="data-filter"][2]/input[@class="input-box"]').click();
        browser.setValue('//div[@class="control-group"][2]//div[@class="data-filter"][2]/input[@class="input-box"]', time_2, 500);
        browser.pause(300);
    }
    this.getDates = function() {
        function pad(n) {
            return n<10 ? '0'+n : n;
        }
        var d = new Date();
        var year = d.getFullYear();
        var month = pad(d.getMonth() + 1);      // "+ 1" becouse the 1st month is 0
        var day = pad(d.getDate());
        var hour = pad(d.getHours());
        var minutes = pad(d.getMinutes());
        var StartDate = month + "/" + pad(day-3) + "/" + year;
        var EndDate = month + "/" + day + "/" + year;
        return Dates = [StartDate, EndDate]
    }
    this.ConvToUNIXdates = function(time) {
        var UnixDate = new Date(time).getTime();
        return UnixDate;
    }
    this.getCreatedGridList= function(xPath) {        
        browser.pause(300);
        var list = browser.elements(xPath);
        browser.pause(300);
        var totList = list.value.length
        console.log(totList);
        browser.pause(300);
        var elem = browser.elements(xPath).getText();
        browser.pause(300);
        console.log(elem);
    }
    this.compareDates = function(sDate, eDate, compDate){
        return (eDate<=compDate || compDate>=sDate)  ? true : false;
    }
    this.filterDataSetName=function(value){
        var dataSetName=browser.element('[data-test-id="browse-data-filter"]');
        browser.setValue('[data-test-id="browse-data-filter"]',value,100);
        browser.keys('Enter');
        browser.pause(1000);       
    };
    this.deleteBtn=function(){
        var Btn = browser.element('[data-test-id="delete-btn"]').click();
    }
    this.confirmDeleteBtn=function(){
        var Btn = browser.element('//button[@class="action-button button-small-size"]');
        Btn.click();
    } 

    this.enter_datsetname=function(value){        
        var textbox = browser.element("[data-test-id='browse-data-filter']");
        textbox.click();
        textbox.keys(value);
        // return nametext;           
    };
    
    this.enter_Filename=function(value){        
        var textbox = browser.element("[data-test-id='browse-data-files-filter']");
        textbox.click();
        textbox.keys(value);
    };
    this.importIconClick=function(value){   
    var importdata = browser.element('[class="size-large menu-navicon icon-import"]');
            importdata.click();
    };
    this.addDataIconClick=function(value){   
        var addData = browser.element('[class="size-large menu-navicon icon-add"]');
        addData.click();
        };
    this.filesCheckmark = function (value) {
        var check = browser.element("//tab[2]//div[@class='datatable-body-cell-label']//label[@class='grid-checkbox pointer-cursor']//span[@class='checkmark-left']");
        check.click();
    };
    this.clickImportAddDataIcon=function(){
        var importAddData=browser.element('[class="size-large menu-navicon icon-add"]');
        if(!(importAddData.getAttribute("class")).includes('enabled')){
            importAddData.click();
        }
    
    };
    this.dropDownClick=function(value){   
        var dropdown = browser.element("[data-test-id='"+value+"dropdown']");
        dropdown.click();
        };
    this.fileUpload=function(value){   
        var fileToUpload = "../testdata/sprintmt/Nokia CPE 19Jul09 162107.1.nmfs";
        var absolutePath = path.resolve(__dirname, fileToUpload);
        importDataObject.browseFile(absolutePath);
        browser.pause(300);
        importDataObject.clickImportBtn();
        browser.pause(300);
        browser.waitForExist(config.importProgress);
        var dataSetName = browser.elements("//span[contains(text(),'" + value + "')]");
        while (dataSetName.value.length > 0) {
            dataSetName = browser.elements("//span[contains(text(), '" + value + "')]");
        }
    };
    this.clearSearchFilter=function(){
        var elem=browser.elements("//app-browse-data//div[@class='input-container']//i");
        console.log(elem.value+"count");
        if(elem.value.length>0){
            elem.click();
        }
    }
    
    
};

module.exports=new browseDataPage();