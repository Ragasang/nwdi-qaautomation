let baseMethods = require('../specs/testbase.js');
var metricsAndEventsdatapage = function(){
    
    this.clickOnFilteredEvent=function()
    {
    var filteredEvent = browser.element("[data-test-id='filteredEvent']");
    filteredEvent.click();
    }
    this.clickOnAddToFavElem = function(value)
    {
    var addFavorites = browser.elements("//span[@class = 'left-group']");
    // var removeFavElem=addFavorites.element("//span[contains(text(),'Remove from Favorites')]");
    // if(removeFavElem.isVisible()){
    //     removeFavElem.click();      
    //     browser.pause(500);
    //     if(value=="metric"){
    //         this.clickFilteredMetric();
    //     }
    //     if(value=="event"){
    //         this.clickOnFilteredEvent();
    //     }
        
    // }
    var addToFavElem=addFavorites.element("//span[contains(text(),'Add to Favorites')]");
    addToFavElem.click();
    }
    this.clickOnRemoveFavElem = function()
    {
    var removeFavorites = browser.elements("//span[@class = 'left-group']");
    var removeFavElem=removeFavorites.element("//span[contains(text(),'Remove from Favorites')]");
    if(removeFavElem.isVisible()){
        removeFavElem.click();
    }
    }
    this.isVisibleRemoveFromFavorites = function()
    {
        var removeFavorites = browser.elements("//span[@class = 'left-group']");
        var removeFavElem=removeFavorites.element("//span[contains(text(),'Remove from Favorites')]");
        return removeFavElem.isVisible()
        }
    this.closeEventWidget = function()
    {
        var close=browser.element("[data-test-id='close-button']");
        close.click();
    }

    this.clickOnToggle = function()
    {
        var toggleButton = browser.element("[data-test-id='switch-box']");
        toggleButton.click();
    };
    this.clickOnExpander=function(){
        var expander = browser.element("//span[@class='toggle-children-wrapper toggle-children-wrapper-collapsed']");
        if(expander.isVisible()){
            expander.click();
        }
    }
    this.clickOnMetricExpander=function(){
        var expander = browser.element("//span[@class='toggle-children-wrapper toggle-children-wrapper-collapsed']");
        if(expander.isVisible()){
            expander.click();
        }
    }

    this.clickFilteredMetric=function(){
        browser.element("[data-test-id='filteredMetric']").click();
    };
    this.clearEventName=function(){
        var elem=browser.element("[data-test-id='clear-event-name']");
        if(elem.isVisible()){
            elem.click();
        }
    };
    this.clearMetricName=function(){
        var elem=browser.element("[data-test-id='clear-metric-name']")
        if(elem.isVisible()){
            elem.click();
        }
    };
    
    this.closeMetricWidget=function(){
        var metricCloseButton=browser.element("[data-test-id='close-button']")
        if(metricCloseButton.isVisible()){
            metricCloseButton.click();
        }
    };
    this.clickFirstEvent=function(){
        browser.element("//div[@class='tree-node-level-2 tree-node tree-node-leaf']").click();
    };
    this.clickFirstMetric=function(){
        browser.element("//div[@class='tree-node-level-2 tree-node tree-node-leaf']").click();
    };
    this.clickSelectionMetricsOKBtn=function(){
        browser.element("[data-test-id='ok-btn']").click();
    };
    this.eventWidgetDock=function(){
        browser.element("[class='icon-16-pin icon-small-size tab-pin']").click();
    };
    this.eventWidgetUnDock=function(){
        browser.element("[class='icon-16-pin icon-small-size tab-pin pinned']").click();
        browser.pause(300);
    };
    this.enterMetricFilterValue=function(value){
        browser.setValue('input[data-test-id="Metrics-search-box"]', value, 100);
        browser.pause(300);
    };
    this.legendTab=function(){
        browser.element("[data-test-id='IconUniqueDataIdLegend']").click();
        browser.pause(300);
    };
    this.selectMetricValue=function(value){
        browser.element("//option[@value = '"+value+"']").click();
        browser.pause(300);
    };
    this.clickOkBtn=function(){
        browser.element("[data-test-id='tv-no-data-button']").click();
    };
    this.clickOkOnErrorMsg=function(){
        browser.element("[data-test-id='ts-no-data-button']").click();
    };
    this.clickOnTimeseriesCloseBtn=function(){
        browser.element("[data-test-id='Time Seriesclose-btn']").click();
    };
    this.clickOnMapErrorMsg=function(){
        browser.element("[class='action-button button-small-size clear-margin']").click();
        browser.pause(100);
    };
};

module.exports=new metricsAndEventsdatapage();