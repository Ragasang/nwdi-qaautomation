exports.config = {
     debug: true,
     execArgv: ['--inspect=127.0.0.1:5859'],
    specs: [
        './specs/*/*/*/*.js',
        './specs/xynergy.smoke.test/*.js',
        './specs/xynergy.qa.test/*/*.js',
    ],
    exclude: [],
    suites: {
        smoke: [
            './specs/xynergy.smoke.test/Searchdata/*.js',
            './specs/xynergy.smoke.test/SequenceDiagrams/*.js',
            './specs/xynergy.smoke.test/TableView/*.js',
            './specs/xynergy.smoke.test/Messages/*.js',
            './specs/xynergy.smoke.test/TimeSeries/*.js',
            './specs/xynergy.smoke.test/MapView/*.js',
            './specs/xynergy.smoke.test/synctest/*.js',
            './specs/xynergy.smoke.test/Workspace_feature/*.js',
        ],
        sanity: [            
            './specs/xynergy.sanity.test/Searchdata/*.js',
            './specs/xynergy.sanity.test/SequenceDiagrams/*.js',
            './specs/xynergy.sanity.test/TableView/*.js',
            './specs/xynergy.sanity.test/Messages/*.js',
            './specs/xynergy.sanity.test/TimeSeries/*.js',
            './specs/xynergy.sanity.test/MapView/*.js',
            './specs/xynergy.sanity.test/synctest/*.js',
        ],
        qa: [
            //'./specs/xynergy.qa.test/*.js',
            './specs/xynergy.qa.test/MetricsAndEvents/metrics_test.js',
        ],
        sample: [
            //'./specs/xynergy.qa.test/BrowseData/*.js',
            './specs/xynergy.qa.test/Workspace_feature/XYN-1160_test.js',
        ],
        desktopsmoke: [            
            './specs/xynergy.smoke.test/Searchdata/*.js',
            './specs/xynergy.smoke.test/SequenceDiagrams/*.js',
            './specs/xynergy.smoke.test/TableView/*.js',
            './specs/xynergy.smoke.test/Messages/*.js',
            './specs/xynergy.smoke.test/TimeSeries/*.js',
            './specs/xynergy.smoke.test/MapView/*.js',
            './specs/xynergy.smoke.test/synctest/*.js',
            './specs/xynergy.smoke.test/Workspace_feature/*.js',
        ],
        desktopsanity: [            
            './specs/xynergy.sanity.test/Diagnostics/*.js',
            './specs/xynergy.sanity.test/ImportData/*.js',
            './specs/xynergy.sanity.test/SequenceDiagrams/*.js',
            './specs/xynergy.sanity.test/TableView/*.js',
            './specs/xynergy.sanity.test/Messages/*.js',
            './specs/xynergy.sanity.test/TimeSeries/*.js',
            './specs/xynergy.sanity.test/MetricsAndEvents/events_test.js',
            './specs/xynergy.sanity.test/MetricsAndEvents/metrics_test.js',
            './specs/xynergy.sanity.test/MapView/*.js',
            './specs/xynergy.sanity.test/synctest/*.js',
            './specs/xynergy.sanity.test/ScopeWidget/*.js',
            './specs/xynergy.sanity.test/Searchdata/*.js',
            './specs/xynergy.sanity.test/ParameterComparison/*.js',
        ],
    },
    reporters: ['allure'],
    reporterOptions: {
        allure: {
            outputDir: 'test-results',
            disableWebdriverStepsReporting: false,
            disableWebdriverScreenshotsReporting: false,
            useCucumberStepReporter: true
        }
    },
    maxInstances: 1,
    capabilities: [{
        maxInstances: 10,
        browserName: 'chrome',
        // chromeOptions: {
        //     args: ["--window-size=1920,1080"]
        // },
    }],
    mocha :[{
        reporter: 'mochawesome',
        reporterOptions: {
          overwrite: true,
          reportTitle: 'My Custom Title',
          showPassed: true,
          reportPath:'./Reports/'
        }
      }],
 
    sync: true,
    logLevel: 'silent',
    coloredLogs: true,
    deprecationWarnings: true,
    bail: 0,
    screenshotPath: './errorShots/',
    waitforTimeout: 900000,
    allScriptsTimeout: 200000,
    connectionRetryTimeout: 100000,
    connectionRetryCount: 3,
    framework: 'mocha',
    reporters: ['dot', 'allure'],
    reporterOptions: {
        outputDir: './wdio-logs/',
        allure: {
            outputDir: '../TestReport'
        }
    },
    mochaOpts: {
        ui: 'bdd',
        timeout: 150000
    },
    //
    // =====
    // Hooks
    // =====
    // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance
    // it and to build services around it. You can either apply a single function or an array of
    // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
    // resolved to continue.
    /**
     * Gets executed once before all workers get launched.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    // onPrepare: function (config, capabilities) {
    // },
    /**
     * Gets executed just before initialising the webdriver session and test framework. It allows you
     * to manipulate configurations depending on the capability or spec.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // beforeSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`. It is the perfect place to define custom commands.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // before: function (capabilities, specs) {
    // },
    /**
     * Runs before a WebdriverIO command gets executed.
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     */
    // beforeCommand: function (commandName, args) {
    // },

    /**
     * Hook that gets executed before the suite starts
     * @param {Object} suite suite details
     */
    // beforeSuite: function (suite) {
    // },
    /**
     * Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
    // beforeTest: function (test) {
    // },
    /**
     * Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
     * beforeEach in Mocha)
     */
    // beforeHook: function () {
    // },
    /**
     * Hook that gets executed _after_ a hook within the suite ends (e.g. runs after calling
     * afterEach in Mocha)
     */
    // afterHook: function () {
    // },
    /**
     * Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) ends.
     * @param {Object} test test details
     */
    // afterTest: function (test) {
    // },
    /**
     * Hook that gets executed after the suite has ended
     * @param {Object} suite suite details
     */
    // afterSuite: function (suite) {
    // },

    /**
     * Runs after a WebdriverIO command gets executed
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     * @param {Number} result 0 - command success, 1 - command error
     * @param {Object} error error object if any
     */
    // afterCommand: function (commandName, args, result, error) {
    // },
    /**
     * Gets executed after all tests are done. You still have access to all global variables from
     * the test.
     * @param {Number} result 0 - test pass, 1 - test fail
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    // after: function (result, capabilities, specs) {
    // },
    /**
     * Gets executed right after terminating the webdriver session.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    // afterSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed after all workers got shut down and the process is about to exit.
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    // onComplete: function(exitCode, config, capabilities) {
    // }
}